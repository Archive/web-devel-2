<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" encoding="iso-8859-1"/>

  <xsl:strip-space elements="*"/>

  <!-- Common functions -->
  <xsl:include href="summary.xsl"/>  

  <!-- Plain text renderer -->
  <xsl:include href="summary-txt.xsl"/>
  
  <!-- Static texts -->
  <xsl:include href="summary-static.en.xsl"/>
  
  <!-- The main header and the TOC is created in the root handler -->
  <xsl:template match="gnome-summary">
    <xsl:text>This is the GNOME Summary for </xsl:text>
    <xsl:value-of select="@startdate"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="@enddate"/>
    
    <!-- TOC header -->
    <xsl:text>
    </xsl:text>
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-toc"/>
    </xsl:call-template>

    <!-- TOC -->
    <xsl:call-template name="toc-list"/>

    <!-- Body -->
    <xsl:apply-templates/>
  </xsl:template>
  
</xsl:stylesheet>

