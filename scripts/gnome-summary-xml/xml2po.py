#!/usr/bin/python -u
import sys
import libxml2
import gettext
import string

def normalizeString(text, ignorewhitespace = 1):
    """Normalizes string to be used as key for gettext lookup.

    Removes all unnecessary whitespace."""
    if not ignorewhitespace:
        return text

    words = text.split()
    result = ''
    for t in words:
        if not t.strip()=='':
            if result:
                result += ' '+t
            else:
                result = t

    return result.replace('"','\\"')


def outputMessage(text, lineno = 0):
    """Adds a string to the list of messages."""
    if (text.strip() != ''):
        t = normalizeString(text)
        if not t in messages:
            messages.append(t)
            if t in linenos.keys():
                linenos[t].append(lineno)
            else:
                linenos[t] = [ lineno ]

def startTagForNode(node):
    if not node:
        return 0

    result = node.name
    params = ''
    if node.properties:
        for p in node.properties:
            if p.type == 'attribute':
                params += ' %s="%s"' % (p.name, p.content)
    return result+params
        
def mySerialize(node):
    """Should have been done all below in nodeToText.

    I screw up. Indeed."""
    if not node:
        return ''

    if not node.get_children():
        if node.type=='comment':
            return ''
        else:
            return node.content
    else:
        if node.type=='comment':
            return ''

        if node.type == 'text' or node.type == 'element':
            cur = node.children
            result = ''
            while cur:
                if cur.type == 'comment':
                    pass
                elif cur.type == 'entity_ref':
                    result += cur.content
                elif cur.type == 'entity_decl':
                    result += '<%s>%s</%s>' % (startTagForNode(cur), cur.content, cur.name)
                else:
                    result += mySerialize(cur)
                cur = cur.next

            return '<%s>%s</%s>' % (startTagForNode(node), result, node.name)
        elif node.type == 'entity_ref':
            return node.content
        else:
            return node.serialize()
        

def nodeToText(node):
    """Makes textual output from a node.

    Makes a serialized textual output from a node, suitable
    for inclusion in gettext message."""
    if node.type == 'entity_ref'  or node.type == 'entity_decl':
        return ''

    if node.children:
        # Add one-by-one children
        result = ''
        t = node.children
        while t:
            result += mySerialize(t)
            t = t.next
    else:
        result = mySerialize(node)

    return result


def isFinalNode(node):
    if node.name in ultimate_tags:
        return 1
    return 0

def ignoreNode(node):
    if node.name in ignored_tags:
        return 1
    if node.type in ('dtd', 'comment'):
        return 1
    return 0
    
def getTranslation(text):
    if (text and text.strip() == ''):
        return text
    text = normalizeString(text)
    file = open(mofile, "rb")
    if file:
        gt = gettext.GNUTranslations(file)
        if gt:
            return gt.gettext(text.replace('\\"','"'))
    return text

def parseNodes(node):
    if ignoreNode(node):
        return 

    if isFinalNode(node) or not node.children:
        if mode == 'merge':
            t = getTranslation(nodeToText(node))
            if node.children:
                tmp = '<%s>%s</%s>' % (startTagForNode(node), t, node.name)
                newnode = libxml2.parseMemory(tmp,len(tmp))
                free = node.children
                while free:
                    next = free.next
                    free.unlinkNode()
                    free = next
                node.addChildList(newnode.children.children)
            else:
                node.setContent(t)
        else:
            outputMessage(nodeToText(node)) # FIXME: Line numbers
    else:
        children = node.children
        while children:
            parseNodes(children)
            children = children.next


# Main program start

#libxml2.lineNumbers(1) # FIXME: line numbers

filename = ''
mofile = ''
ultimate = [ ]
ignored = [ ]

mode = 'pot' # 'pot' or 'merge'

import getopt, fileinput

args = sys.argv[1:]
opts, args = getopt.getopt(args, 'mt:o:f:i:', ['merge', 'translation=', 'output=', 'final-tags=', 'ignored-tags=' ])
for opt, arg in opts:
    if opt in ('-m', '--merge'):
        mode = 'merge'
    elif opt in ('-t', '--translation'):
        mofile = arg
    elif opt in ('-f', '--final-tags'):
        ultimate.append(arg.strip())
    elif opt in ('-i', '--ignored-tags'):
        ignored.append(arg.strip())
    elif opt in ('-o', '--output'):
        output = arg
        # currently ignored, output goes to stdout

if args:
    filename = args.pop()
else:
    filename = '-'

if not len(ultimate):
    ultimate = [ 'final-tags' ]
ultimate_tags = []
for line in fileinput.input(ultimate):
    ultimate_tags.append(line.strip())

#ultimate_tags = ['para']

if not len(ignored):
    ignored = [ 'ignored-tags' ]
ignored_tags = []
for line in fileinput.input(ignored):
    ignored_tags.append(line.strip())

#ignored_tags = [ 'bug-stats', 'cvs-stats' ]

doc = libxml2.parseFile(filename)
if doc.name != filename:
    print "Cannot open file."
    sys.exit(1)

messages = []
linenos = {}

parseNodes(doc)

if mode != 'merge':
    for k in messages:
        # FIXME: Parser mode needs to be changed to make use of line numbers, not sure how easy this is
        #print "#. tst.xml: %d" % (linenos[k][0])
        print "msgid \"%s\"" % (k)
        print "msgstr \"\"\n"
else:
    print doc.serialize('utf-8')
