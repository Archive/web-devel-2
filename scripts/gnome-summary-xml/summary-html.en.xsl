<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Common functions -->
  <xsl:include href="summary.xsl"/>  

  <!-- Plain text renderer -->
  <xsl:include href="summary-html.xsl"/>
  
  <!-- Static texts -->
  <xsl:include href="summary-static.en.xsl"/>
  
  <!-- The main header and the TOC is created in the root handler -->
  <xsl:template match="gnome-summary">
    <div xmlns="http://www.w3.org/1999/xhtml">
    <h1>
      <xsl:text>GNOME Summary - </xsl:text>
      <xsl:value-of select="@startdate"/>
      <xsl:text> - </xsl:text>
      <xsl:value-of select="@enddate"/>
    </h1>
      
    <!-- TOC header -->
    <xsl:text>
    </xsl:text>
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-toc"/>
    </xsl:call-template>

    <!-- TOC -->
    <xsl:call-template name="toc"/>

    <hr />
    
    <!-- Body -->
    <xsl:apply-templates/>
    </div>
  </xsl:template>
  
</xsl:stylesheet>

