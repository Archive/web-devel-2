<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text"/>

  <xsl:strip-space elements="*"/>

  <!-- Common functions -->
  <xsl:include href="summary.xsl"/>  
  <xsl:include href="translations-txt.xsl"/>  

  <!-- A paragraph of text -->
  <xsl:template match="para/text()">
    <xsl:value-of select="normalize-space(string(.))"/>
    <xsl:text> </xsl:text>
  </xsl:template>

  <xsl:template match="para">
    <xsl:apply-templates/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  <xsl:template match="link/text()">
    <xsl:value-of select="normalize-space(string(.))"/>
  </xsl:template>
  
  <!-- A link inside an article text -->
  <xsl:template match="para/link">
    <xsl:choose>
      <xsl:when test="@href">
	<xsl:apply-templates/>
	<xsl:text> (</xsl:text>
	<xsl:value-of select="@href"/>
	<xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- A block-level link -->
  <xsl:template match="text/link">
    <xsl:text>
        </xsl:text>
    <xsl:choose>
      <xsl:when test="@href">
	<xsl:apply-templates/>
	<xsl:text> (</xsl:text>
	<xsl:value-of select="@href"/>
	<xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- A bold horizontal line -->
  <xsl:template name="bold-ruler">
    <xsl:text>
==============================================================
</xsl:text>
  </xsl:template>

  <!-- A thin horizontal line -->
  <xsl:template name="ruler">
    <xsl:text>
--------------------------------------------------------------
</xsl:text>
  </xsl:template>
  
  
  <!-- General template for displaying section headers -->
  <xsl:template name="section-header">
    <xsl:param name="title_text"/>
    <xsl:call-template name="bold-ruler"/>
    <xsl:value-of select="normalize-space($title_text)"/>
    <xsl:call-template name="ruler"/>
    <xsl:text>
</xsl:text>
    </xsl:template>

  <!-- Articles: title + text -->
  <xsl:template match="article">
    <!-- Print article header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	<xsl:number count="article" level="any" format="1. "/><xsl:value-of select="title"/>
      </xsl:with-param>
    </xsl:call-template>
    
    <!-- Print article body -->
    <xsl:apply-templates select="text"/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  <xsl:template match="salute">
    <xsl:text>
</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <!-- Table of Contents entry -->
  <xsl:template name="toc-entry">
    <xsl:param name="index"/>
    <xsl:param name="description"/>
    <xsl:number value="$index" format="1. "/>
    <xsl:value-of select="normalize-space($description)"/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  
  <!-- New and Updated Software -->
  <xsl:template match="apps">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	      <xsl:number value="count(../article|../cvs-stats|../bug-stats|../translation-stats) + 1" format="1. "/>
	<xsl:value-of select="$str-sw-title"/>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:apply-templates/>

    <!-- Section footer -->
    <xsl:text>
</xsl:text>
    <xsl:value-of select="$str-sw-footer"/>
    <xsl:text>
</xsl:text>

  </xsl:template>

  <!-- A software map entry -->
  <xsl:template match="apps/app">
    <xsl:value-of select="normalize-space(name)"/>
    <xsl:text>  - </xsl:text><xsl:value-of select="normalize-space(desc)"/>
    <xsl:if test="version">
      <xsl:text> - </xsl:text><xsl:value-of select="normalize-space(version)"/>
    </xsl:if>
    <xsl:text>
</xsl:text>
  </xsl:template>
    
  <!-- CVS Statistics: fully automated -->
  <xsl:template match="cvs-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	<xsl:number value="count(../article)+1" format="1. "/>
	<xsl:value-of select="$str-cvs-title"/>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:value-of select="$str-cvs-header"/>

    <xsl:apply-templates/>
  </xsl:template>

  <!-- An item (a CVS module or a hacker nick) in the statistics -->
  <xsl:template match="cvs-stats/*/item">
    <xsl:text> </xsl:text>
    <xsl:number value="@commit" format="1 "/>
    <xsl:apply-templates/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  <!-- CVS modules list -->
  <xsl:template match="cvs-stats/modules">
    <xsl:value-of select="$str-cvs-modules"/>
    <xsl:text>
</xsl:text>
    <xsl:for-each select="item">
      <xsl:sort data-type="number" select="@commit" order="descending"/>
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <xsl:call-template name="str-cvs-modules-other"/>
  </xsl:template>

  <!-- CVS hackers list -->
  <xsl:template match="cvs-stats/hackers">
    <xsl:value-of select="$str-cvs-hackers"/>
    <xsl:text>
</xsl:text>
    <xsl:for-each select="item">
      <xsl:sort data-type="number" select="@commit" order="descending"/>
      <xsl:apply-templates select="."/>
    </xsl:for-each>
    <xsl:call-template name="str-cvs-hackers-other"/>
  </xsl:template>
  
  <!--  Bug Statistics: fully automated (Article Header) -->
  <xsl:template match="bug-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	<xsl:number value="count(../article|../cvs-stats) + 1" format="1. "/>
	<xsl:value-of select="$str-bugs-title"/>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:value-of select="$str-bugs-header"/>
    <xsl:text>
</xsl:text>

    <xsl:apply-templates/>
  </xsl:template>

  <!-- List of modules with the most bugs (Summary) -->
  <xsl:template match="bug-stats/bug-modules">
    <xsl:text>
</xsl:text>
	  <xsl:text>Currently open: </xsl:text>
	  <xsl:number value="@open" format="1"/> 
	  <xsl:text> (In the last week: New: </xsl:text>
	  <xsl:number value="@opened" format="1"/> 
	  <xsl:text>, Resolved: </xsl:text>
	  <xsl:number value="@closed" format="1"/>
	  <xsl:text>, Difference: </xsl:text>
	  <xsl:choose> <xsl:when test="@opened > @closed"> <xsl:text>+</xsl:text></xsl:when> </xsl:choose>
	  <xsl:value-of select="@opened - @closed" format="1"/>
	  <xsl:text>)
</xsl:text>
    <xsl:text>
</xsl:text>
    <xsl:value-of select="$str-bugs-modules"/>
    <xsl:text>
</xsl:text>
    <xsl:text>
  </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <!-- List of modules with the most bugs (Per Module) -->
<xsl:template match="bug-stats/bug-modules/bug-item">
	<xsl:apply-templates/>
	<xsl:text>: </xsl:text>
	<xsl:number value="@open" format="1 "/>
	<xsl:text>(In the last week: New: </xsl:text>
	<xsl:number value="@opened" format="1"/>
	<xsl:text>, Resolved: </xsl:text>
	<xsl:number value="@closed" format="1"/>
	<xsl:text>, Difference: </xsl:text>
	<xsl:choose> <xsl:when test="@opened > @closed"> <xsl:text>+</xsl:text></xsl:when> </xsl:choose>
	<xsl:value-of select="@opened - @closed" format="1"/>
	<xsl:text>)
  </xsl:text>
  </xsl:template>

  <!-- List of bughunters who closed the most bugs -->
  <xsl:template match="bug-stats/bug-hunters">
    <xsl:text>
</xsl:text>
    <xsl:value-of select="$str-bugs-hunters"/>
    <xsl:text>
  </xsl:text>
    <xsl:text>
  </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

<xsl:template match="bug-stats/bug-hunters/bug-item">
	<xsl:apply-templates/>
	<xsl:text>: </xsl:text> 
	<xsl:number value="@closed" format="1 "/>
	<xsl:text>bugs closed.</xsl:text>
	<xsl:text>
  </xsl:text>
</xsl:template>


</xsl:stylesheet>

