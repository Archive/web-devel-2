#!/bin/sh

XSLTPROC=${XLSTPROC:-xsltproc}
TEXT_XSL=summary-txt.en.xsl
HTML_XSL=summary-html.en.xsl

XMLFILE=$1
BASENAME=$(basename $XMLFILE .xml)
TEXT_OUT=$BASENAME.txt
HTML_OUT=$BASENAME.html

[ -f $XMLFILE ] || { echo "Input file $XMLFILE not found"; exit; }

echo "Creating $TEXT_OUT"
$XSLTPROC $TEXT_XSL $XMLFILE | fold -s > $TEXT_OUT

echo "Creating $HTML_OUT"
$XSLTPROC $HTML_XSL $XMLFILE | sed -e '1,2d' > $HTML_OUT
