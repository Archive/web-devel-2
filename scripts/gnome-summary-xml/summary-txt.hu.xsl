<?<?xml version="${xml_version}" encoding="UTF-8"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  <?xml version="${xml_version}" encoding="UTF-8"?>

  <xsl:output method="text"
    encoding="iso-8859-2"/>

  <xsl:strip-space elements="*"/>

  <!-- Common functions -->
  <xsl:include href="summary.xsl"/>  

  <!-- Plain text renderer -->
  <xsl:include href="summary-txt.xsl"/>
  
  <!-- Static texts -->
  <xsl:include href="summary-static.hu.xsl"/>
  
  <!-- The main header and the TOC is created in the root handler -->
  <xsl:template match="gnome-summary">
    <xsl:text>GNOME Hírek </xsl:text>
    <xsl:value-of select="@startdate"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="@enddate"/>
    
    <!-- TOC header -->
    <xsl:text>
    </xsl:text>
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-toc"/>
    </xsl:call-template>

    <!-- TOC -->
    <xsl:call-template name="toc-list"/>

    <!-- Body -->
    <xsl:apply-templates/>

    <!-- Footer -->
    <xsl:call-template name="bold-ruler"/>
    <xsl:text>
Eredeti (C) 2001 GNOME Summary Team &lt;gnome-summary@gnome.org&gt;
Magyar változat (C) 2001 Érdi Gergő &lt;cactus@cactus.rulez.org&gt;

http://cactus.rulez.org/projects/gnome/summary/

</xsl:text>
      
  </xsl:template>
  
</xsl:stylesheet>

