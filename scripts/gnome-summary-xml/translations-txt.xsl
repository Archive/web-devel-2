<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--  Translation Statistics: fully automated -->
  <xsl:template match="translation-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	      <xsl:number value="count(../article|../cvs-stats|../bug-stats) + 1" format="1. "/>
	<xsl:value-of select="$str-translation-title"/>
      </xsl:with-param>
      <xsl:with-param name="index" select="count(../article|../cvs-stats|../bug-stats)+1"/>
    </xsl:call-template>

    <xsl:value-of select="$str-translation-start" /><xsl:value-of select="/gnome-summary/@enddate" />
    <xsl:text>, </xsl:text><xsl:value-of select="$str-translation-changes" /><xsl:value-of select="/gnome-summary/@startdate" />
    <xsl:text>.
</xsl:text>

    <xsl:for-each select="language">
      <!--<xsl:sort data-type="number" select="@translated" order="descending"/>-->
      <xsl:apply-templates select="."/>
    </xsl:for-each>

    <xsl:text>

</xsl:text>
    <xsl:value-of select="$str-translation-end" />

    <xsl:text>

</xsl:text>
  </xsl:template>

  <!-- Per language template -->
  <xsl:template match="translation-stats/language">
	    <xsl:text>
</xsl:text>
	    <xsl:text>   </xsl:text><xsl:value-of select="@rank"/><xsl:text>	</xsl:text>
	    <xsl:value-of select="."/>
	    <xsl:choose>
	      <xsl:when test="string-length(.)>15"><xsl:text>	</xsl:text></xsl:when>
	      <xsl:when test="string-length(.)>7"><xsl:text>		</xsl:text></xsl:when>
	      <xsl:otherwise><xsl:text>			</xsl:text></xsl:otherwise>
	    </xsl:choose>
	    <xsl:choose>
	      <xsl:when test="string-length(@translated)=4"><xsl:text>  </xsl:text></xsl:when>
	      <xsl:when test="string-length(@translated)=5"><xsl:text> </xsl:text></xsl:when>
	    </xsl:choose>
	    <xsl:value-of select="@translated"/><xsl:text>%	</xsl:text>
	    <xsl:choose>
		    <xsl:when test="@changed > 0"> 
		      <xsl:text>   up </xsl:text><xsl:value-of select="@changed"/><xsl:text>%</xsl:text>
		    </xsl:when> 
		    <xsl:when test="@changed &lt; 0"> 
		      <xsl:text> down </xsl:text><xsl:value-of select="-@changed"/><xsl:text>%</xsl:text>
		    </xsl:when> 
	            <xsl:otherwise> <xsl:text> no change</xsl:text></xsl:otherwise> 
	    </xsl:choose>
  
  </xsl:template>

</xsl:stylesheet>

