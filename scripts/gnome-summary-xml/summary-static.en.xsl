<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:variable name="str-toc">
    <xsl:text>Table of Contents</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-sw-title">
    <xsl:text>New and Updated Software</xsl:text>
  </xsl:variable>

  <xsl:template name="foo">
    <xsl:param name="text"/>
    
    <xsl:text>(</xsl:text>
    <xsl:value-of select="$href"/>
    <xsl:text>)</xsl:text>
  </xsl:template>
  
  <xsl:variable name="str-sw-footer">
    <xsl:text>For more information on these packages visit the </xsl:text>
    <xsl:text>GNOME Software map: </xsl:text>
    <xsl:text>http://www.gnome.org/softwaremap/latest.php</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-title">
    <xsl:text>Hacker Activity</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-translation-title">
    <xsl:text>Translation status</xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-rank">
    <xsl:text>Rank</xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-language">
    <xsl:text>Language</xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-status">
    <xsl:text>Status</xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-changed">
    <xsl:text>Changed</xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-start">
    <xsl:text>This is translation status for core Gnome 2.6 for </xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-changes">
    <xsl:text>with changes since </xsl:text>
  </xsl:variable>
  <xsl:variable name="str-translation-end">
    <xsl:text>Languages are ranked by the percentage of translated strings for developer 
platform and desktop.  Rank for each language is determined as the number 
of languages that have a better percentage, increased by one.  If languages 
share the same rank, then only the first in the list contains the rank field.
</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-header">
    <xsl:text>Thanks for Paul Warren for these lists.

</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-modules">
    <xsl:text>Most active modules:</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-hackers">
    <xsl:text>Most active hackers:</xsl:text>
  </xsl:variable>

  <!-- [x active modules ommitted] -->
  <xsl:template name="str-cvs-modules-other">
    <xsl:text>[</xsl:text>
    <xsl:number value="@other" format="1"/>
    <xsl:text> active modules omitted]

</xsl:text>
  </xsl:template>

  <!-- [x active hackers ommitted] -->
  <xsl:template name="str-cvs-hackers-other">
    <xsl:text>[</xsl:text>
    <xsl:number value="@other" format="1"/>
    <xsl:text> active hackers omitted]

</xsl:text>
  </xsl:template>
  
  <xsl:variable name="str-bugs-title">
    <xsl:text>Gnome Bug Hunting Activity</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-bugs-header">
	  <xsl:text>This information is from http://bugzilla.gnome.org, which hosts bug and feature reports for most of the Gnome modules. If you would like to join the bug hunt, subscribe to the gnome-bugsquad mailing list.</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-bugs-modules">
	  <xsl:text>Modules with the most open bugs (excluding enhancement requests): </xsl:text>
  </xsl:variable>

  <xsl:variable name="str-bugs-hunters">
	  <xsl:text>Gnome Bugzilla users who resolved or closed the most bugs: </xsl:text>
  </xsl:variable>

</xsl:stylesheet>
