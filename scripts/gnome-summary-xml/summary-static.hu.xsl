<?xml version="1.0" encoding="UTF-8"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:variable name="str-toc">
    <xsl:text>Tartalomjegyzék</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-sw-title">
    <xsl:text>Szoftverújdonságok</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-sw-footer">
    <xsl:text>További információkat találhatsz ezekről és más</xsl:text>
    <xsl:text>progamokról a GNOME Software Map-ben: </xsl:text>
    <xsl:text>http://www.gnome.org/softwaremap/latest.php</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-title">
    <xsl:text>Hackelési mutatók</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-header">
    <xsl:text>Köszönjük Paul Warren-nek a listák elkészítését.

</xsl:text>
  </xsl:variable>

  
  <xsl:variable name="str-cvs-modules">
    <xsl:text>Legaktívabb modulok:</xsl:text>
  </xsl:variable>

  <xsl:variable name="str-cvs-hackers">
    <xsl:text>Legaktívabb fejlesztők:</xsl:text>
  </xsl:variable>

  <!-- [x active modules ommitted] -->
  <xsl:template name="str-cvs-modules-other">
    <xsl:text>
[valamint további </xsl:text>
    <xsl:number value="@other" format="1"/>
    <xsl:text> aktív modul]

</xsl:text>
  </xsl:template>

  <!-- [x active hackers ommitted] -->
  <xsl:template name="str-cvs-hackers-other">
    <xsl:text>
[valamint további </xsl:text>
    <xsl:number value="@other" format="1"/>
    <xsl:text> aktív fejlesztő]

</xsl:text>
  </xsl:template>
  
</xsl:stylesheet>

