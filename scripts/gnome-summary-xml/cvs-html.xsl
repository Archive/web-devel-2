<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- CVS Statistics -->
  <xsl:template match="cvs-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-cvs-title"/>
      <xsl:with-param name="index" select="count(../article)+1"/>
    </xsl:call-template>

    <p xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$str-cvs-header"/>
    </p>
    
    <table xmlns="http://www.w3.org/1999/xhtml" width="100%">
      <tr>
	<xsl:apply-templates/>
      </tr>
    </table>
  </xsl:template>

  <!-- A CVS module in the statistics -->
  <xsl:template match="cvs-stats/modules/item">
    <tr xmlns="http://www.w3.org/1999/xhtml">
      <td align="right">
	<xsl:number value="@commit" format="1"/>
      </td>
      <td align="left">
	<a href="http://cvs.gnome.org/lxr/source/{string(.)}">
	  <xsl:apply-templates/>
	</a>
      </td>
    </tr>
  </xsl:template>

  <!-- A CVS hacker in the statistics -->
  <xsl:template match="cvs-stats/hackers/item">
    <tr xmlns="http://www.w3.org/1999/xhtml">
      <td align="right">
	<xsl:number value="@commit" format="1"/>
      </td>
      <td align="left">
	<xsl:apply-templates/>
      </td>
    </tr>
  </xsl:template>

  <!-- CVS modules list -->
  <xsl:template match="cvs-stats/modules">
    <td xmlns="http://www.w3.org/1999/xhtml" align="left">
      <xsl:value-of select="$str-cvs-modules"/>

      <table>
	<xsl:for-each select="item">
	  <xsl:sort data-type="number" select="@commit" order="descending"/>
	  <xsl:apply-templates select="."/>
	</xsl:for-each>
      </table>
      <xsl:call-template name="str-cvs-modules-other"/>
    </td>
  </xsl:template>

  <!-- CVS hackers list -->
  <xsl:template match="cvs-stats/hackers">
    <td xmlns="http://www.w3.org/1999/xhtml" align="left">
      <xsl:value-of select="$str-cvs-hackers"/>
      <table>
	<xsl:for-each select="item">
	  <xsl:sort data-type="number" select="@commit" order="descending"/>
	  <xsl:apply-templates select="."/>
	</xsl:for-each>
      </table>
      <xsl:call-template name="str-cvs-hackers-other"/>
    </td>
  </xsl:template>
  
</xsl:stylesheet>

