<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--  Translation Statistics: fully automated -->
  <xsl:template match="translation-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-translation-title" />
      <xsl:with-param name="index" select="count(../article|../cvs-stats|../bug-stats)+1"/>
    </xsl:call-template>
    
    <p xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$str-translation-start" /><xsl:value-of select="/gnome-summary/@enddate" />
    <xsl:text>, </xsl:text><xsl:value-of select="$str-translation-changes" /><xsl:value-of select="/gnome-summary/@startdate" />
    <xsl:text>.
</xsl:text></p>

    <table xmlns="http://www.w3.org/1999/xhtml">
    <tr style="background: #e0e0f0;">
      <th style="text-align: left; padding-left: 0px; padding-right: 0px;"><xsl:value-of select="$str-translation-rank"/></th>
      <th style="text-align: left; padding-left: 1em; padding-right: 1em;"><xsl:value-of select="$str-translation-language"/></th>
      <th style="text-align: left; padding-left: 1em; padding-right: 1em;"><xsl:value-of select="$str-translation-status"/></th>
      <th style="text-align: right; padding-left: 1em; padding-right: 1em;"><xsl:value-of select="$str-translation-changed"/></th>
    </tr>
    <!--<xsl:apply-templates/>-->

    <xsl:for-each select="language">
      <!--<xsl:sort data-type="number" select="@translated" order="descending"/>-->
      <xsl:apply-templates select="."/>
    </xsl:for-each>

    </table>
    <p xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$str-translation-end" /></p>
  </xsl:template>

  <!-- Per language template -->
  <xsl:template match="translation-stats/language">
	  <tr xmlns="http://www.w3.org/1999/xhtml">
	    <td style="text-align: right;"><xsl:value-of select="@rank"/></td>
	    <td style="padding-left: 1em; padding-right: 1em;"><xsl:apply-templates/></td>
	    <td style="text-align: right; padding-left: 1em; padding-right: 1em;"><xsl:value-of select="@translated"/><xsl:text>%</xsl:text></td>
	    <td style="text-align: right; padding-left: 1em; padding-right: 1em;">
	      <xsl:choose>
		    <xsl:when test="@changed > 0"> 
		      <xsl:text>up </xsl:text><xsl:value-of select="@changed"/><xsl:text>%</xsl:text>
		    </xsl:when> 
		    <xsl:when test="@changed &lt; 0"> 
		      <xsl:text>down </xsl:text><xsl:value-of select="-@changed"/><xsl:text>%</xsl:text>
		    </xsl:when> 
	            <xsl:otherwise> <xsl:text>no change</xsl:text></xsl:otherwise> 
	      </xsl:choose>
	      
 	    </td>
	  </tr>
  </xsl:template>

</xsl:stylesheet>

