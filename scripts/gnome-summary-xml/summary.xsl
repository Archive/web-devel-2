<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Table of Contents -->
  <xsl:template name="toc-list">

    <!-- Articles -->
    <xsl:for-each select="article">
      <xsl:call-template name="toc-entry">
	<xsl:with-param name="index" select="position()"/>
	<xsl:with-param name="description" select="title"/>
      </xsl:call-template>
    </xsl:for-each>

    <!-- Special: CVS stats -->
    <xsl:if test="cvs-stats">
      <xsl:call-template name="toc-entry">
	<xsl:with-param name="index" select="count(article) + 1"/>
	<xsl:with-param name="description" select="$str-cvs-title"/>
      </xsl:call-template>
    </xsl:if>
        
    <!-- Special: Bug stats -->
    <xsl:if test="bug-stats">
      <xsl:call-template name="toc-entry">
	<xsl:with-param name="index" select="count(article|cvs-stats) + 1"/>
	<xsl:with-param name="description" select="$str-bugs-title"/>
      </xsl:call-template>
    </xsl:if>

    <!-- Special: Translation stats -->
    <xsl:if test="translation-stats">
      <xsl:call-template name="toc-entry">
	<xsl:with-param name="index" select="count(article|cvs-stats|bug-stats) + 1"/>
	<xsl:with-param name="description" select="$str-translation-title"/>
      </xsl:call-template>
    </xsl:if>

    <!-- Special: Updated software -->
    <xsl:if test="apps">
      <xsl:call-template name="toc-entry">
	<xsl:with-param name="index">
	  <xsl:value-of select="count(article|cvs-stats|bug-stats|translation-stats) + 1"/>
	</xsl:with-param>
	<xsl:with-param name="description" select="$str-sw-title"/>
      </xsl:call-template>
    </xsl:if>
    
  </xsl:template>
  
</xsl:stylesheet>

