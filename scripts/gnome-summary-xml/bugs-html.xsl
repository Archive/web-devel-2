<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--  Bug Statistics: fully automated -->
  <xsl:template match="bug-stats">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text">
	<xsl:number value="count(../article|../cvs-stats) + 1" format="1. "/>
	<xsl:value-of select="$str-bugs-title"/>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:value-of select="$str-bugs-header"/>
    <xsl:text>
</xsl:text>

    <xsl:apply-templates/>
  </xsl:template>

  <!-- List of modules with the most bugs (Summary) -->
  <xsl:template match="bug-stats/bug-modules">
	  <p xmlns="http://www.w3.org/1999/xhtml"><xsl:text>Currently open: </xsl:text>
	  <xsl:number value="@open" format="1"/>
	  <xsl:text> (In the last week: New: </xsl:text>
	  <xsl:number value="@opened" format="1"/>
	  <xsl:text>, Resolved: </xsl:text>
	  <xsl:number value="@closed" format="1"/>
	  <xsl:text>, Difference: </xsl:text>
	  <xsl:choose> <xsl:when test="@opened > @closed"> <xsl:text>+</xsl:text></xsl:when> </xsl:choose>
	  <xsl:value-of select="@opened - @closed" format="1"/>
	  <xsl:text>)</xsl:text></p>
    <xsl:text>
</xsl:text>
<p xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$str-bugs-modules"/></p>
    <xsl:text>
</xsl:text>
    <xsl:text>
    </xsl:text>
    <table xmlns="http://www.w3.org/1999/xhtml" border="1" cellspacing="5">
	<tr>
    	    	<th><xsl:text> Module </xsl:text></th>
	    	<th><xsl:text> Open Bugs </xsl:text></th>
	    	<th><xsl:text> New/Opened in last week </xsl:text></th>
	    	<th><xsl:text> Resolved in last week </xsl:text></th>
	    	<th><xsl:text> Difference </xsl:text></th>
    	</tr>
    <xsl:apply-templates/>
    </table>
  </xsl:template>

  <!-- List of modules with the most bugs (Per Module) -->
<xsl:template match="bug-stats/bug-modules/bug-item">
	<tr xmlns="http://www.w3.org/1999/xhtml">
		<td>
			<xsl:apply-templates/>
			<xsl:text>:</xsl:text>
		</td>
		<td>
			<xsl:number value="@open" format="1"/>
		</td>
		<td>
			<xsl:number value="@opened" format="1"/>
		</td>
		<td>
			<xsl:number value="@closed" format="1"/>
		</td>
		<td>
			<xsl:choose> <xsl:when test="@opened > @closed"> <xsl:text>+</xsl:text></xsl:when> </xsl:choose>
	  		<xsl:value-of select="@opened - @closed" format="1"/>
		</td>
	</tr>
  </xsl:template>

  <!-- List of bughunters who closed the most bugs -->
  <xsl:template match="bug-stats/bug-hunters">
    <xsl:text>
</xsl:text>
<p xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$str-bugs-hunters"/></p>
    <xsl:text>
  </xsl:text>
    <xsl:text>
  </xsl:text>
    <table xmlns="http://www.w3.org/1999/xhtml" border="1" cellspacing="5">
	<tr>
		<th><xsl:text> Bug Hunter </xsl:text></th>
		<th><xsl:text> Bugs Resolved/Closed </xsl:text></th>
    	</tr>
    <xsl:apply-templates/>
    </table>
  </xsl:template>

<xsl:template match="bug-stats/bug-hunters/bug-item">
	<tr xmlns="http://www.w3.org/1999/xhtml" align="left">
		<td><a href="mailto:{string(.)}"><xsl:apply-templates/></a>
		<xsl:text>:</xsl:text></td>
		<td><xsl:number value="@closed" format="1"/></td>
	</tr>
  </xsl:template>


</xsl:stylesheet>

