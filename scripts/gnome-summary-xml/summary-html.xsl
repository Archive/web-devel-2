<?xml version="1.0"?> <!-- -*- xml -*- -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:strip-space elements="*"/>
  <xsl:output method="xml"
    indent="yes"
    encoding="iso-8859-1"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <!-- Common functions -->
  <xsl:include href="summary.xsl"/>  
  <xsl:include href="cvs-html.xsl"/>
  <xsl:include href="bugs-html.xsl"/>
  <xsl:include href="translations-html.xsl"/>
  
  <!-- A paragraph of text -->
  <xsl:template match="para">
    <p xmlns="http://www.w3.org/1999/xhtml">
     <xsl:apply-templates/>
    </p>
  </xsl:template>

  <!-- An inline link -->
  <xsl:template match="para/link">
    <xsl:choose>
      <xsl:when test="@href">
	<a xmlns="http://www.w3.org/1999/xhtml" href="{@href}"><xsl:apply-templates/></a>
      </xsl:when>
      <xsl:otherwise>
	<a xmlns="http://www.w3.org/1999/xhtml" href="{.}"><xsl:apply-templates/></a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- A link inside an article text -->
  <xsl:template match="text/link">
    <p xmlns="http://www.w3.org/1999/xhtml">
      <xsl:choose>
	<xsl:when test="@href">
	  <a href="{@href}"><xsl:apply-templates/></a>
	</xsl:when>
	<xsl:otherwise>
	  <a href="{.}"><xsl:apply-templates/></a>
	</xsl:otherwise>
      </xsl:choose>
    </p>
  </xsl:template>

  <!-- General template for displaying section headers -->
  <xsl:template name="section-header">
    <xsl:param name="title_text"/>
    <xsl:param name="index"/>

    <h2 xmlns="http://www.w3.org/1999/xhtml">
      <xsl:if test="$index">
	<a name="{$index}"></a>
	<xsl:number value="$index" format="1. "/>
      </xsl:if>
      <xsl:value-of select="$title_text"/>
    </h2>
    </xsl:template>

  <!-- Table of Contents entry -->
  <xsl:template name="toc-entry">
    <xsl:param name="index"/>
    <xsl:param name="description"/>

    <li xmlns="http://www.w3.org/1999/xhtml">
      <a href="#{$index}"><xsl:value-of select="$description"/></a>
    </li>
  </xsl:template>

  <xsl:template name="toc">
    <ol xmlns="http://www.w3.org/1999/xhtml">
      <xsl:call-template name="toc-list"/>
    </ol>
  </xsl:template>

  <xsl:template match="salute">
    <hr xmlns="http://www.w3.org/1999/xhtml" />
    <xsl:apply-templates/>
  </xsl:template>
  
  <!-- Articles: title + text -->
  <xsl:template match="article">
    <!-- Print article header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="title"/>
      <xsl:with-param name="index"><xsl:number count="article" level="any"/></xsl:with-param>
    </xsl:call-template>
    
    <!-- Print article body -->
    <xsl:apply-templates select="text"/>
    <xsl:text>
</xsl:text>
  </xsl:template>

  <!-- New and Updated Software -->
  <xsl:template match="apps">
    <!-- Section header -->
    <xsl:call-template name="section-header">
      <xsl:with-param name="title_text" select="$str-sw-title"/>
      <xsl:with-param name="index" select="count(../article|../cvs-stats|../bug-stats|../translation-stats)+1"/>
    </xsl:call-template>

    <ul>
      <xsl:apply-templates/>
    </ul>
      
    <!-- Section footer -->
    <xsl:value-of select="$str-sw-footer"/>
  </xsl:template>

  <!-- A software map entry -->
  <xsl:template match="apps/app">
    <li xmlns="http://www.w3.org/1999/xhtml">
      <strong>
	<a href="http://www.gnome.org/softwaremap/projects/{unixname}">
	  <xsl:value-of select="name"/>
	</a>
      </strong>
      <xsl:text>: </xsl:text>
      <xsl:value-of select="desc"/>
      <xsl:if test="version">
	<xsl:text> - </xsl:text><xsl:value-of select="normalize-space(version)"/>
      </xsl:if>
    </li>
  </xsl:template>
    
</xsl:stylesheet>

