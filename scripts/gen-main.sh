#!/bin/sh

if [ ! -f tree.in ]; then
	echo "Error: You must run this in a directory with a tree.in"
	exit 1
fi

TITLE="`grep '^Title;' tree.in | awk -F';' '{print $2}'`"
SECTIONS="`awk -F';' '!/^Title;/ {print $2}' tree.in`"

echo '<!--#set var="last_modified" value="$Date$" -->'
echo "<h2>$TITLE</h2>"
echo "<blockquote><dl>"
for I in $SECTIONS; do
	SECTTITLE="`grep ";$I$" tree.in | awk -F';' '{print $1}'`"

	echo "<dt><strong><a href=\"$I\">$SECTTITLE</a></strong></dt>"
	echo -e '<dd>\n</dd>\n'
	# Find out if this is the last section
if [ "`echo $SECTIONS | rev | awk '{print $1}'`" != "`echo $I | rev`" ]; then 
	echo "<p>"
fi
done
echo "</dl></blockquote>"

