<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta name="generator" content="HTML Tidy, see www.w3.org">
    <title>Application Programming</title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.53">
    <link rel="HOME" title="Introduction to the GConf library "
    href="index.html">
    <link rel="PREVIOUS" title="Implementation Overview" href=
    "impl.html">
    <link rel="NEXT" title="Coding Example" href="example.html">
  </head>

  <body class="SECT1" bgcolor="#FFFFFF" text="#000000" link=
  "#0000FF" vlink="#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" cellpadding="0" cellspacing=
      "0">
        <tr>
          <th colspan="3" align="center">Introduction to the GConf
          library</th>
        </tr>

        <tr>
          <td width="10%" align="left" valign="bottom"><a href=
          "impl.html">&lt;&lt;&lt; Previous</a></td>

          <td width="80%" align="center" valign="bottom">
          </td>

          <td width="10%" align="right" valign="bottom"><a href=
          "example.html">Next &gt;&gt;&gt;</a></td>
        </tr>
      </table>
      <hr align="LEFT" width="100%">
    </div>

    <div class="SECT1">
      <h1 class="SECT1"><a name="API">Application
      Programming</a></h1>

      <p>GConf makes it as simple as possible for application
      programmers to store and retrieve their configuration data.
      However, the general structure of configuration data storage
      and retrieval is somewhat different from the one you might
      use with configuration files.</p>

      <div class="SECT2">
        <h2 class="SECT2"><a name="MVC">Model-View-Controller
        Architecture</a></h2>

        <p>Applications using GConf effectively should structure
        their preferences and configuration code according to the
        famous <i class="FIRSTTERM">Model-View-Controller</i> (MVC)
        design pattern. In this pattern, your application contains
        three separate objects:</p>

        <ol type="1">
          <li>
            <p>The <i class="FIRSTTERM">model</i> traditionally
            models a real-world object; but more generally, it is
            the data you are planning to present to the user and
            allow the user to edit. In the GConf case, the model is
            the configuration database (in the big picture) and a
            particular configuration key (from a smaller-scale
            point of view). For example, the configuration key <tt
            class="FILENAME">/desktop/gnome/menus/show-icons</tt>
            can be thought of as a model; it stores a boolean value
            indicating whether to show icons next to menu items in
            GNOME menus.</p>
          </li>

          <li>
            <p>The <i class="FIRSTTERM">view</i> somehow displays
            or represents your model. It receives some sort of
            notification when the model changes, and updates itself
            accordingly. For example, the GNOME menu item widgets
            listen for changes to the <tt class="FILENAME">
            /desktop/gnome/menus/show-icons</tt> configuration key,
            and show or hide icons accordingly. An important point
            about views is that there can be more than one of them;
            you can have many menu items, without changing the
            model.</p>
          </li>

          <li>
            <p>The <i class="FIRSTTERM">controller</i> modifies the
            model. In the menu icons example, a GNOME control panel
            might be the controller; users use it to set the <tt
            class="FILENAME">/desktop/gnome/menus/show-icons</tt>
            key to true or false.</p>
          </li>
        </ol>
        <br>
        <br>

        <p>An MVC architecture has a number of advantages. Most
        notably, it encourages code reuse and modularity; you can
        have any number of views, and different types of views,
        without changing the model or the controller; you can have
        any number of controllers, and different types of
        controller, without changing the model or the view. The
        main controller may be the GNOME control panel, but if the
        key is changed by some other application, that will also
        work properly. The primary view may be the GNOME menu item
        code, but if some different menu item code wanted to
        monitor and honor this setting, it could certainly do
        so.</p>

        <p>GConf implements an exciting enhancement to the basic
        MVC pattern: the model is process-transparent. That is, the
        view and controller have no knowledge of which process
        contains the model; if you set a configuration key, then
        all views in all interested processes will be notified of
        the change. In a world of component technology, this is
        extremely useful.</p>

        <p>All buzz-acronyms aside, all MVC means for you as an
        application programmer is that every section of code only
        has to be concerned with <i class="EMPHASIS">directly
        related</i> configuration issues. For example, the menu
        item code only monitors the keys that affect how menu items
        are displayed. It is not concerned with any other keys, or
        with setting or saving that setting.</p>

        <p>In contrast, with a simple configuration file, typically
        you have a global data structure which stores all your
        preferences, and you load and save it as a single unit. To
        add a setting, you have to modify the global structure.
        Whenever you load settings, you have to add code to the
        global preferences code that knows about all the interested
        parts of the program and notifies them (via some ad-hoc
        mechanism). It's kind of unpleasant and leads to poor
        modularity in a single process; but once multiple processes
        are involved, coming up with a suitable ad-hoc mechanism
        for change notification becomes a really significant
        problem.</p>
      </div>

      <div class="SECT2">
        <h2 class="SECT2"><a name="DATATYPES">Major Data
        Types</a></h2>

        <div class="SECT3">
          <h3 class="SECT3"><a name="GCONFENGINE"><span class=
          "STRUCTNAME">GConfEngine</span></a></h3>

          <p>A <span class="STRUCTNAME">GConfEngine</span>
          represents a configuration database (normally a
          connection to <tt class="APPLICATION">gconfd</tt>). You
          can store values in a <span class="STRUCTNAME">
          GConfEngine</span>, and retrieve values from one. You can
          also ask a <span class="STRUCTNAME">GConfEngine</span> to
          monitor a key or directory for changes, and invoke an
          application-supplied callback if changes occur. Each time
          you ask a <span class="STRUCTNAME">GConfEngine</span> to
          notify you of changes, it registers a request for
          notification with <tt class="APPLICATION">gconfd</tt>.
          Thus, notification requests are relatively expensive.
          With <span class="STRUCTNAME">GConfClient</span>, they
          are relatively cheap.</p>
        </div>

        <div class="SECT3">
          <h3 class="SECT3"><a name="AEN130"><span class=
          "STRUCTNAME">GConfClient</span></a></h3>

          <p><span class="STRUCTNAME">GConfClient</span> is located
          in a separate library from GConf itself, because to use
          it you must link with GTK+. <span class="STRUCTNAME">
          GConfClient</span> derives from <span class="STRUCTNAME">
          GtkObject</span> and is a wrapper for the <span class=
          "STRUCTNAME">GConfEngine</span> type. It adds a few nice
          features:</p>

          <ul>
            <li>
              <p>It can keep a client-side cache of values.
              Sometimes this is just memory overhead, so you'll
              want to turn it off. However, if you access the same
              keys frequently it can be a performance win.</p>
            </li>

            <li>
              <p>It can do client-side notification request
              dispatching. This means that if you want to monitor
              the keys <tt class="LITERAL">/foo/bar</tt> and <tt
              class="LITERAL">/foo/baz</tt>, <span class=
              "STRUCTNAME">GConfClient</span> registers a single
              notification request with <tt class="APPLICATION">
              gconfd</tt>; when the notification comes from <tt
              class="APPLICATION">gconfd</tt>, <span class=
              "STRUCTNAME">GConfClient</span> decides which key has
              changed and dispatches to the proper application
              callback. With <span class="STRUCTNAME">
              GConfEngine</span>, each callback involves
              registering a request with <tt class="APPLICATION">
              gconfd</tt>.</p>
            </li>

            <li>
              <p>It uses the GTK signal system. Instead of using
              GConf's custom callback API for notification, you can
              simply connect to a <tt class="LITERAL">
              "value_changed"</tt> signal.</p>
            </li>

            <li>
              <p>Finally, <span class="STRUCTNAME">
              GConfClient</span> provides default error handlers.
              This allows you to ignore errors, and let <span
              class="STRUCTNAME">GConfClient</span> present them to
              the user via an error dialog.</p>
            </li>
          </ul>
          <br>
          <br>
        </div>

        <div class="SECT3">
          <h3 class="SECT3"><a name="AEN158"><span class=
          "STRUCTNAME">GConfValue</span></a></h3>

          <p>When GConf needs to pass around a dynamically-typed
          value retrieved from or to be stored in the database, it
          uses a type-tagged union called <span class="STRUCTNAME">
          GConfValue</span>. For example, the <tt class="FUNCTION">
          gconf_get()</tt> function takes a <span class=
          "STRUCTNAME">GConfEngine</span> and a key name as
          arguments and returns a <span class="STRUCTNAME">
          GConfValue</span>. There are also convenience wrappers
          such as <tt class="FUNCTION">gconf_get_string()</tt> and
          <tt class="FUNCTION">gconf_get_int()</tt> that allow you
          to avoid the pesky <span class="STRUCTNAME">
          GConfValue</span> union.</p>
        </div>

        <div class="SECT3">
          <h3 class="SECT3"><a name="AEN169"><span class=
          "STRUCTNAME">GConfChangeSet</span></a></h3>

          <p>Modifications to the GConf database can be grouped
          into <i class="FIRSTTERM">change sets</i>, or collections
          of changes. A change is a new value for a key, or a
          directive to unset a key's current value. For now, the
          <span class="STRUCTNAME">GConfChangeSet</span> is
          primarily a convenience mechanism for managing the set of
          changes a user has made via a preferences dialog.
          However, in the future committing a change set may be an
          atomic operation (that is, GConf could be extended to
          support transactions).</p>
        </div>

        <div class="SECT3">
          <h3 class="SECT3"><a name="AEN175"><span class=
          "STRUCTNAME">GConfError</span></a></h3>

          <p>If you explicitly handle errors, rather than allowing
          the <span class="STRUCTNAME">GConfClient</span> default
          error handler to report them to the user, you receive
          information about an error in an object called <span
          class="STRUCTNAME">GConfError</span>. This contains a
          verbose error description, and an error code number.</p>
        </div>
      </div>

      <div class="SECT2">
        <h2 class="SECT2"><a name="INTEGRATION">GNOME
        Integration</a></h2>

        <p>Version 2.0 of GNOME automatically integrates with
        GConf, making it simpler to use the library. It does the
        following:</p>

        <ul>
          <li>
            <p>Keeps a global <span class="STRUCTNAME">
            GConfClient</span> that's easy to access with a <tt
            class="FUNCTION">gnome_get_gconf_client()</tt>
            function.</p>
          </li>

          <li>
            <p>Automatically uses GNOME dialogs for default error
            handling.</p>
          </li>

          <li>
            <p>Provides convenience functions to extract and insert
            a <span class="STRUCTNAME">GConfValue</span> from
            various kinds of widget. For example, there is a
            function to get the string inside a <span class=
            "STRUCTNAME">GtkEntry</span> as a <span class=
            "STRUCTNAME">GConfValue</span>.</p>
          </li>
        </ul>
        <br>
        <br>
      </div>
    </div>

    <div class="NAVFOOTER">
      <hr align="LEFT" width="100%">

      <table width="100%" border="0" cellpadding="0" cellspacing=
      "0">
        <tr>
          <td width="33%" align="left" valign="top"><a href=
          "impl.html">&lt;&lt;&lt; Previous</a></td>

          <td width="34%" align="center" valign="top"><a href=
          "index.html">Home</a></td>

          <td width="33%" align="right" valign="top"><a href=
          "example.html">Next &gt;&gt;&gt;</a></td>
        </tr>

        <tr>
          <td width="33%" align="left" valign="top">Implementation
          Overview</td>

          <td width="34%" align="center" valign="top">&#xA0;</td>

          <td width="33%" align="right" valign="top">Coding
          Example</td>
        </tr>
      </table>
    </div>
  </body>
</html>

