<HTML
><HEAD
><TITLE
>The GNOME Canvas</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.40"><LINK
REL="NEXT"
TITLE="The Canvas Architecture"
HREF="x70.html"></HEAD
><BODY
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="ARTICLE"
><DIV
CLASS="TITLEPAGE"
><H1
CLASS="TITLE"
><A
NAME="AEN2"
>The GNOME Canvas</A
></H1
><H3
CLASS="AUTHOR"
>by Federico Mena Quintero</H3
><DIV
CLASS="AFFILIATION"
><DIV
CLASS="ADDRESS"
>	    <TT
CLASS="EMAIL"
>&lt;federico@nuclecu.unam.mx&gt;</TT
>
	  </DIV
></DIV
><P
CLASS="COPYRIGHT"
>Copyright © 1999 by <SPAN
CLASS="HOLDER"
>The Free Software Foundation</SPAN
></P
><table
width="70%"
border="0"
cellspacing="0"
cellpadding="4"
><tr
><td
><B
>Abstract </B
><DIV
><DIV
CLASS="ABSTRACT"
><P
></P
><P
>	The GNOME canvas is an engine for structured graphics that
	offers a rich imaging model, high performance rendering, and a
	powerful, high—level API.  It offers a choice of two rendering
	back-ends, one based on Xlib for extremely fast display, and
	another based on Libart, a sophisticated, antialiased,
	alpha-compositing engine.  Applications have a choice between
	the Xlib imaging model or a superset of the PostScript imaging
	model, depending on the level of graphic sophistication
	required.
      </P
><P
>	This white paper presents the architecture of the GNOME canvas
	and Libart, and describes the situations in which it is useful
	to use these technologies.
      </P
><P
></P
></DIV
></DIV
></td
></tr
></table
><HR></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="AEN17"
>Introduction</A
></H1
><P
>      The GNOME canvas is a high-level engine for creating structured
      graphics.  This means the programmer can insert graphical
      items like lines, rectangles, and text into the canvas, and
      refer to them later for further manipulation.  The programmer
      does not need to worry about repainting these items or
      generating events for them; the canvas automatically takes care
      of these operations.
    </P
><P
>      The canvas provides several predefined item types, including
      lines, rectangles, and text.  However, the canvas is designed to
      serve as a general-purpose display engine.  Applications can
      define their own custom item types that integrate with the rest
      of the canvas framework.  This lets them have a flicker-free
      display engine with proper event management and propagation.
    </P
><P
>      The canvas supports two rendering models.  One is based directly
      on Xlib (used via GDK), which provides an extremely fast and
      lean display that runs well over networks.  The second rendering
      model is based on Libart, a sophisticated library for
      manipulating and rendering vector paths using antialiasing and
      alpha compositing.  Libart provides a superset of the PostScript
      imaging model, allowing for extremely high-quality displays.
    </P
><P
>      Simple applications can use the predefined canvas item types to
      create interactive graphics displays.  For example, the
      <TT
CLASS="APPLICATION"
>GNOME Calendar</TT
> program uses the
      canvas to display and manipulate monthly calendars.  It only
      needs simple graphical items like rectangles and text to display
      its information.  Please look at <A
HREF="canvas.html#GNOMECAL"
>Figure 1</A
> for
      an example of the use of the canvas in the <TT
CLASS="APPLICATION"
>GNOME
      Calendar</TT
>.
    </P
><DIV
CLASS="FIGURE"
><A
NAME="GNOMECAL"
></A
><P
><B
>Figure 1. Use of the canvas in the <TT
CLASS="APPLICATION"
>GNOME Calendar</TT
></B
></P
><P
><IMG
SRC="gnomecal.gif"></P
><DIV
CLASS="CALLOUTLIST"
><DL
COMPACT="COMPACT"
><DT
><A
HREF="canvas.html#MONTH-VIEW"
><B
>(1)</B
></A
></DT
><DD
>		The whole month view is a single big canvas.  Stock
		canvas items like rectangles and text are used to
		display an easily-customizable calendar.
	      </DD
><DT
><A
HREF="canvas.html#MONTH-ITEM"
><B
>(2)</B
></A
></DT
><DD
>		The little monthly calendars are custom canvas groups
		based on the stock item types.  The calendar program
		implements different behavior for each place in which
		the monthly calendar item is used.
	      </DD
></DL
></DIV
></DIV
><P
>      Applications with more sophisticated requirements can define
      their custom canvas item types.  The
      <TT
CLASS="APPLICATION"
>Gnumeric</TT
> spreadsheet defines a large
      ‘Sheet’ item that takes care of painting the sheet's
      grid and the cell contents.  It also defines an item to handle
      the complex cursor object in the spreadsheet, which must handle
      the current selection, the current cell, and the border items to
      drag and extend selections.  Please look at <A
HREF="canvas.html#GNUMERIC"
>Figure 2</A
> for an example of the use of the canvas in
      the <TT
CLASS="APPLICATION"
>Gnumeric</TT
> spreadsheet.
    </P
><DIV
CLASS="FIGURE"
><A
NAME="GNUMERIC"
></A
><P
><B
>Figure 2. Use of the canvas in the <TT
CLASS="APPLICATION"
>Gnumeric</TT
> spreadsheet</B
></P
><P
><IMG
SRC="gnumeric.gif"></P
><DIV
CLASS="CALLOUTLIST"
><DL
COMPACT="COMPACT"
><DT
><A
HREF="canvas.html#SHEET"
><B
>(1)</B
></A
></DT
><DD
>		<TT
CLASS="APPLICATION"
>Gnumeric</TT
> uses a custom
		"Sheet" item to display the spreadsheet contents.
		This item is responsible for drawing grid lines and
		cell contents.
	      </DD
><DT
><A
HREF="canvas.html#CURSOR"
><B
>(2)</B
></A
></DT
><DD
>		The cursor is another custom item.  In <A
HREF="canvas.html#GNUMERIC"
>Figure 2</A
> it is shown as a 4×2-cell
		selection with the current cell being G13.  The cursor
		item handles all aspects of a spreadsheet's cursor,
		including the current cell, the selection range, and
		the clickable areas that let the user drag and fill
		cells automatically.
	      </DD
><DT
><A
HREF="canvas.html#HEADERS"
><B
>(3)</B
></A
></DT
><DD
>		The column and row headers are two modes of a single
		custom item.  This item draws the button-like headers
		and highlights them as appropriate when cells are
		selected.
	      </DD
><DT
><A
HREF="canvas.html#OVERLAY"
><B
>(4)</B
></A
></DT
><DD
>		<TT
CLASS="APPLICATION"
>Gnumeric</TT
> uses the stock
		canvas items (like lines, rectangles, and ellipses)
		for the graphic elements that the user may want to
		overlay on the spreadsheet.  Here the user has
		inserted an arrow, and the canvas takes care of
		repainting everything automatically.
	      </DD
></DL
></DIV
></DIV
><P
>      This white paper describes the architecture of the GNOME canvas
      and Libart.  It gives examples on why application writers may
      want to use these technologies.
    </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>&#xA0;</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&#xA0;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="x70.html"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>&#xA0;</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&#xA0;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>The Canvas Architecture</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>