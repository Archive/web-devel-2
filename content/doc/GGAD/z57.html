<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Widget Concepts
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GTK+ Basics" href="cha-gtk.html">
    <link rel="PREVIOUS" title="Containers And Widget Layout" href= 
    "sec-containers.html">
    <link rel="NEXT" title="The Main Loop" href=
    "sec-mainloop.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-containers.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-mainloop.html"><font color="#0000ff" size= 
            "2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="Z57">Widget Concepts</a>
      </h1>
      <p>
        This section discusses concepts that apply to all widgets,
        including memory management and certain special states
        widgets can be in. It's a "conceptual" section; however,
        the concepts are very important to practical topics covered
        later in the book.
      </p>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="WIDGETLIFECYCLE">Widget Life Cycle</a>
        </h2>
        <p>
          Widget resource and memory management is mostly
          automatic. However, there are a couple of "gotchas" to
          keep in mind if you're doing more complicated things.
        </p>
        <div class="FIGURE">
          <a name="FL-WIDGETDESTROY"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-WIDGETDESTROY.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_destroy</tt></code>(GtkWidget*
              <tt class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 21. Widget Destruction</b>
          </p>
        </div>
        <p>
          A widget can be destroyed at any time by calling <tt
          class="FUNCTION">gtk_widget_destroy()</tt> (shown in <a
          href="z57.html#FL-WIDGETDESTROY">Figure 21</a>);
          destroying a widget frees any associated memory and other
          resources. If the widget is inside a container, it is
          automatically removed from the container before it's
          destroyed. It's worth noting that <tt class="FUNCTION">
          gtk_widget_destroy()</tt> is simply another name for <tt
          class="FUNCTION">gtk_object_destroy()</tt>; <span class= 
          "STRUCTNAME">GtkObject</span>s have "virtual destructors"
          so <tt class="FUNCTION">gtk_object_destroy()</tt> will
          always do the right thing.
        </p>
        <p>
          Internally, a reference count is maintained for all
          widgets (actually, all <span class="STRUCTNAME">
          GtkObject</span>s). Objects begin their life with a
          reference count of 1, even though they have not yet been
          referenced. At this stage the object is said to be <i
          class="FIRSTTERM">floating</i> and is flagged as such. It
          is possible to remove the object's initial reference;
          this is called <i class="FIRSTTERM">sinking</i> the
          floating object and will destroy the object if the
          floating reference was the only one.
        </p>
        <p>
          Containers first reference and then sink any floating
          widgets that are added to them. By sinking a widget, a
          container "takes ownership" of it for resource management
          purposes. Thus, the reference count of the widget remains
          1, but the object is no longer flagged as floating. When
          a widget is removed from a container---or the container
          is destroyed---the reference count is decremented to 0.
          When an object's reference count reaches 0, it is
          destroyed.
        </p>
        <p>
          In practice, this means that you only have to destroy
          toplevel widgets; any widgets that are inside a container
          will be destroyed along with the container.
        </p>
        <p>
          There's a danger here, however. Sometimes you want to
          remove a widget from a container; perhaps some element of
          your interface is optional or only appears under certain
          circumstances. When you remove the widget (using <tt
          class="FUNCTION">gtk_container_remove()</tt>), it will be
          unreferenced, its reference count will drop to 0, and it
          will be destroyed. To avoid this situation, you should
          add a reference to the widget before you remove it. <a
          href="z57.html#FL-REFCOUNTS">Figure 22</a> lists the
          functions to manipulate reference counts.
        </p>
        <div class="FIGURE">
          <a name="FL-REFCOUNTS"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-REFCOUNTS.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkobject.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_object_ref</tt></code>(GtkObject* <tt
              class="PARAMETER"><i>object</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_object_unref</tt></code>(GtkObject*
              <tt class="PARAMETER"><i>object</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_object_sink</tt></code>(GtkObject* <tt
              class="PARAMETER"><i>object</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 22. Reference Counting</b>
          </p>
        </div>
        <p>
          <tt class="FUNCTION">gtk_object_ref()</tt> and <tt class= 
          "FUNCTION">gtk_object_unref()</tt> have widget-specific
          variants (<tt class="FUNCTION">gtk_widget_ref()</tt>,
          etc.) but the object and widget versions are completely
          synonymous. The widget-specific versions are leftovers
          from earlier versions of GTK+.
        </p>
        <p>
          So to safely remove a widget from a container, you might
          do this:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
  gtk_widget_ref(widget);
  gtk_container_remove(container, widget);
</pre>
            </td>
          </tr>
        </table>
        <p>
          The widget now has one reference, held by your code. At
          some point you'll need to release the reference,
          destroying the widget. (It would make sense to do so
          after re-adding the widget to some other container, for
          example.)
        </p>
        <p>
          It's worth pointing out that removing widgets from
          containers is uncommon; in general it's faster to simply
          hide the widget with <tt class="FUNCTION">
          gtk_widget_hide()</tt>, then <tt class="FUNCTION">
          gtk_widget_show()</tt> it at some later time.
        </p>
        <p>
          <tt class="FUNCTION">gtk_object_sink()</tt> is used
          almost exclusively in widget implementations, when you
          expect to be the primary "owner" of an object. If an
          object is not "floating", <tt class="FUNCTION">
          gtk_object_sink()</tt> has no effect. To claim ownership
          of a widget, do this:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
  gtk_widget_ref(widget);
  gtk_object_sink(GTK_OBJECT(widget));
</pre>
            </td>
          </tr>
        </table>
        <p>
          This code adds one reference to the widget; if the widget
          was "floating," it also subtracts one reference. If the
          widget was not floating, <tt class="FUNCTION">
          gtk_widget_sink()</tt> has no effect.
        </p>
        <p>
          It's important to understand the details because in some
          cases they can be important. But most of the time, you
          can get by with a few simple rules:
        </p>
        <ul>
          <li>
            <p>
              You must destroy any toplevel widgets when you are
              done with them, but child widgets are destroyed
              automatically.
            </p>
          </li>
          <li>
            <p>
              If you want to remove a widget from a container
              without destroying it, you must first add a reference
              to the widget.
            </p>
          </li>
          <li>
            <p>
              If you add a reference to a widget, you are
              responsible for unreferencing the widget again when
              you're done with it.
            </p>
          </li>
        </ul>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="SEC-REALIZINGSHOWING">Realizing, Mapping, and
          Showing</a>
        </h2>
        <p>
          Fully understanding GTK+ requires some minimal
          understanding of the X Window System. This book assumes
          you have a user-level understanding---you know what an X
          server is, that X is network transparent, what a window
          manager does, and so on. A few more details are needed to
          write programs, however.
        </p>
        <p>
          One detail is particularly important: the X Window System
          maintains a tree of <i class="FIRSTTERM">windows</i>.
          "Window" in this sense refers to an X window, not a <tt
          class="CLASSNAME">GtkWindow</tt>---<tt class=
          "CLASSNAME">GtkWindow</tt> is a GTK+-specific concept, a
          widget that corresponds to an application's toplevel X
          window. An X window is not the user-visible concept
          "window" represented by <tt class="CLASSNAME">
          GtkWindow</tt>; rather, it's an abstraction used by the X
          server to partition the screen. The "background"
          displayed by your X server is the <i class="FIRSTTERM">
          root window</i>; the root window has no parent.
          Application windows are typically near-children of the
          root window; most window managers create a child of the
          root window to hold the window's titlebar and other
          decorations, and place the application window inside.
          Window managers have total control over application
          windows---they can reposition them, reparent them, and
          iconify them at will. Application windows can in turn
          contain subwindows, which are controlled by the
          application. Note that GTK+ uses the GDK library, rather
          than using X directly; in GDK, there is a thin X window
          wrapper called <span class="STRUCTNAME">GdkWindow</span>.
          Don't confuse <span class="STRUCTNAME">GdkWindow</span>
          and <tt class="CLASSNAME">GtkWindow</tt>.
        </p>
        <p>
          An X window, or a <span class="STRUCTNAME">
          GdkWindow</span>, gives the X server hints about the
          structure of the graphics being displayed. Since X is
          network transparent, this helps reduce network traffic.
          The X server knows how to show windows on the screen;
          hide them; move them around (keeping children in position
          relative to their parents); capture events such as mouse
          movements on a per-window basis; and so on. A <span
          class="STRUCTNAME">GdkWindow</span> is also the
          fundamental unit for drawing graphics---you can't draw to
          "the screen" as a whole, you must draw on a <span class= 
          "STRUCTNAME">GdkWindow</span>.
        </p>
        <p>
          Most GTK+ widgets have a corresponding <span class= 
          "STRUCTNAME">GdkWindow</span>. There are exceptions, such
          as <tt class="CLASSNAME">GtkLabel</tt>; these are
          referred to as "no window widgets," and are relatively
          lightweight. Widgets with no associated <span class= 
          "STRUCTNAME">GdkWindow</span> draw into their parent's
          <span class="STRUCTNAME">GdkWindow</span>. Some
          operations, such as capturing events, require a <span
          class="STRUCTNAME">GdkWindow</span>; thus they are
          impossible on no-window widgets.
        </p>
        <p>
          Widgets pass through a number of states related to their
          <span class="STRUCTNAME">GdkWindow</span>:
        </p>
        <ul>
          <li>
            <p>
              A widget is said to be <i class="FIRSTTERM">
              realized</i> if its corresponding <span class= 
              "STRUCTNAME">GdkWindow</span> has been created.
              Widgets are realized via <tt class="FUNCTION">
              gtk_widget_realize()</tt>, and unrealized via <tt
              class="FUNCTION">gtk_widget_unrealize()</tt>. Since
              an X window must have a parent, if a widget is
              realized its parent must also be. 
            </p>
          </li>
          <li>
            <p>
              A widget is <i class="FIRSTTERM">mapped</i> if <tt
              class="FUNCTION">gdk_window_show()</tt> has been
              called on its <span class="STRUCTNAME">
              GdkWindow</span>. This means the server has been
              asked to display the window on the screen; obviously
              the <span class="STRUCTNAME">GdkWindow</span> must
              exist, implying that the widget is realized.
            </p>
          </li>
          <li>
            <p>
              A widget is <i class="FIRSTTERM">visible</i> if it
              will automatically be mapped when its parent is
              mapped. This means that <tt class="FUNCTION">
              gtk_widget_show()</tt> has been called on the widget.
              A widget can be rendered invisible by calling <tt
              class="FUNCTION">gtk_widget_hide()</tt>; this will
              either unschedule the pending map, or unmap the
              widget (hide its <span class="STRUCTNAME">
              GdkWindow</span>). Since toplevel widgets have no
              parent, they are mapped as soon as they are
              shown.
            </p>
          </li>
        </ul>
        <p>
          In typical user code, you only need to call <tt class= 
          "FUNCTION">gtk_widget_show()</tt>; this implies realizing
          and mapping the widget as soon as its parent is realized
          and mapped. It's important to understand that <tt class= 
          "FUNCTION">gtk_widget_show()</tt> has no immediate
          effect, it merely schedules the widget to be shown. This
          means you don't have to worry about showing widgets in
          any particular order; it also means that you can't
          immediately access the <span class="STRUCTNAME">
          GdkWindow</span> of a widget. Sometimes you need to
          access the <span class="STRUCTNAME">GdkWindow</span>; in
          those cases you'll want to manually call <tt class= 
          "FUNCTION">gtk_widget_realize()</tt> to create it. <tt
          class="FUNCTION">gtk_widget_realize()</tt> will also
          realize a widget's parents, if appropriate. It's uncommon
          to need <tt class="FUNCTION">gtk_widget_realize()</tt>;
          if you find that you do, perhaps you are approaching the
          problem incorrectly.
        </p>
        <p>
          Destroying a widget automatically reverses the entire
          sequence of events, recursively unrealizing the widget's
          children and the widget itself.
        </p>
        <p>
          <a href="z57.html#FL-SHOWREALIZE">Figure 23</a>
          summarizes the functions discussed in this section.
        </p>
        <div class="FIGURE">
          <a name="FL-SHOWREALIZE"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-SHOWREALIZE.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_realize</tt></code>(GtkWidget*
              <tt class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gtk_widget_unrealize</tt></code>(GtkWidget* <tt
              class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_map</tt></code>(GtkWidget* <tt
              class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_unmap</tt></code>(GtkWidget*
              <tt class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_show</tt></code>(GtkWidget* <tt
              class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gtk_widget_hide</tt></code>(GtkWidget* <tt
              class="PARAMETER"><i>widget</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 23. Showing/Realizing Widgets</b>
          </p>
        </div>
        <p>
          <a href="z57.html#ML-STATES">Figure 24</a> summarizes
          macros for querying the states discussed in this section.
        </p>
        <div class="FIGURE">
          <a name="ML-STATES"></a>
          <div class="FUNCSYNOPSIS">
            <a name="ML-STATES.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF"><tt class="FUNCTION">
              GTK_WIDGET_NO_WINDOW</tt></code>(<tt class=
              "PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF"><tt class="FUNCTION">
              GTK_WIDGET_REALIZED</tt></code>(<tt class=
              "PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF"><tt class="FUNCTION">
              GTK_WIDGET_MAPPED</tt></code>(<tt class=
              "PARAMETER"><i>widget</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF"><tt class="FUNCTION">
              GTK_WIDGET_VISIBLE</tt></code>(<tt class=
              "PARAMETER"><i>widget</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 24. Widget Predicates</b>
          </p>
        </div>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z58">Other Widget Concepts</a>
        </h2>
        <p>
          This section describes a few other concepts associated
          with the <tt class="CLASSNAME">GtkWidget</tt> base class,
          including <i class="FIRSTTERM">sensitivity</i>, <i class= 
          "FIRSTTERM">focus</i>, and <i class="FIRSTTERM">widget
          states</i>.
        </p>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-SENSITIVITY">Sensitivity</a>
          </h3>
          <p>
            Widgets can be <i class="FIRSTTERM">sensitive</i> or <i
            class="FIRSTTERM">insensitive</i>; insensitive widgets
            do not respond to input. (On other platforms, this is
            termed "ghosted" or "inactive").
          </p>
          <p>
            <tt class="FUNCTION">gtk_widget_set_sensitive()</tt>
            (<a href="z57.html#FL-SETSENSITIVE">Figure 25</a>)
            changes a widget's sensitivity.
          </p>
          <div class="FIGURE">
            <a name="FL-SETSENSITIVE"></a>
            <div class="FUNCSYNOPSIS">
              <a name="FL-SETSENSITIVE.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gtk_widget_set_sensitive</tt></code>(GtkWidget* <tt
                class="PARAMETER"><i>widget</i></tt>, gboolean <tt
                class="PARAMETER"><i>setting</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 25. Changing Sensitivity</b>
            </p>
          </div>
          <p>
            By default sensitivity is set to <span class=
            "STRUCTNAME">TRUE</span>. A widget is only "really"
            sensitive if all its parents are sensitive; that is,
            you can make an entire container full of widgets
            (in)sensitive by setting the sensitivity of the
            container. The "real" sensitivity of a widget,
            including its parent's state, can be tested with the
            <tt class="FUNCTION">GTK_WIDGET_IS_SENSITIVE()</tt>
            macro. The sensitivity of the widget itself, which only
            matters if the widget's parent is sensitive, can be
            queried using <tt class="FUNCTION">
            GTK_WIDGET_SENSITIVE()</tt>. These are in <a href= 
            "z57.html#ML-ISSENSITIVE">Figure 26</a>.
          </p>
          <div class="FIGURE">
            <a name="ML-ISSENSITIVE"></a>
            <div class="FUNCSYNOPSIS">
              <a name="ML-ISSENSITIVE.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF"><tt class="FUNCTION">
                GTK_WIDGET_IS_SENSITIVE</tt></code>(<tt class= 
                "PARAMETER"><i>widget</i></tt>);</code>
              </p>
              <p>
                <code><code class="FUNCDEF"><tt class="FUNCTION">
                GTK_WIDGET_SENSITIVE</tt></code>(<tt class= 
                "PARAMETER"><i>widget</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 26. Sensitivity</b>
            </p>
          </div>
        </div>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-FOCUSWIDGET">Focus</a>
          </h3>
          <p>
            Within each toplevel window, one widget at a time may
            have the <i class="FIRSTTERM">keyboard focus</i>. Any
            key events received by the toplevel window are
            forwarded to the focused widget. This is important
            because typing something on the keyboard should have
            only one effect---changing only one text entry field,
            for example.
          </p>
          <p>
            Most widgets will give some visual indication that they
            have the current focus. Using the default GTK+ theme,
            the focused widget is typically surrounded by a thin
            black frame. The user can move the focus between
            widgets, using the arrow keys or the Tab key. Focus can
            also move to a widget if the user clicks it.
          </p>
          <p>
            The concept of focus is important for keyboard
            navigation. For example, pressing Enter or the space
            bar "activates" many widgets if they have the focus;
            you can move between buttons with the Tab key, and
            press one with Space, for example.
          </p>
        </div>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-GRABS">Grabs</a>
          </h3>
          <p>
            Widgets can <i class="FIRSTTERM">grab</i> the pointer
            and keyboard away from other widgets. This essentially
            means that the widget becomes "modal"; input goes only
            to that widget, and the focus can't be changed to
            another widget. A typical reason to grab input is to
            create a modal dialog; if a window has the grab,
            interaction with other windows is blocked. Note that
            there is another, GDK-level "grab"; a GDK keyboard or
            pointer grab occurs on an X-server-wide basis---that
            is, other applications are unable to receive keyboard
            or mouse events. A widget grab is a GTK+ concept; it
            only grabs events away from other widgets in the same
            application.
          </p>
        </div>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="Z59">Default</a>
          </h3>
          <p>
            Each window may have at most one <i class="FIRSTTERM">
            default</i> widget. For example, dialogs typically have
            a default button which is activated when the user
            presses the Enter key.
          </p>
        </div>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-WIDGETSTATES">Widget States</a>
          </h3>
          <p>
            Widgets have <i class="FIRSTTERM">states</i> which
            determine their appearance:
          </p>
          <ul>
            <li>
              <p>
                <i class="FIRSTTERM">Normal</i>: just like it
                sounds.
              </p>
            </li>
            <li>
              <p>
                <i class="FIRSTTERM">Active</i>: a button is
                currently pressed in, or a check box is currently
                checked, for example.
              </p>
            </li>
            <li>
              <p>
                <i class="FIRSTTERM">Prelight</i>: the mouse is
                over the widget (and typically clicking would have
                some effect). Buttons "highlight" when you move
                over them, for example.
              </p>
            </li>
            <li>
              <p>
                <i class="FIRSTTERM">Selected</i>: the widget is in
                a list or other set of alternatives, and is the
                currently selected option.
              </p>
            </li>
            <li>
              <p>
                <i class="FIRSTTERM">Insensitive</i>: the widget is
                "ghosted," inactive, or unresponsive; it will not
                respond to input.
              </p>
            </li>
          </ul>
          <p>
            The exact meaning and visual representation of a given
            state depends on the particular widget and the current
            theme. You can access the state of the widget <tt
            class="FUNCTION">GTK_WIDGET_STATE()</tt> (<a href= 
            "z57.html#ML-WIDGETSTATE">Figure 27</a>). This macro
            returns one of the constants <span class="STRUCTNAME">
            GTK_STATE_NORMAL</span>, <span class="STRUCTNAME">
            GTK_STATE_ACTIVE</span>, <span class="STRUCTNAME">
            GTK_STATE_PRELIGHT</span>, <span class="STRUCTNAME">
            GTK_STATE_SELECTED</span>, or <span class="STRUCTNAME">
            GTK_STATE_INSENSITIVE</span>.
          </p>
          <div class="FIGURE">
            <a name="ML-WIDGETSTATE"></a>
            <div class="FUNCSYNOPSIS">
              <a name="ML-WIDGETSTATE.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkwidget.h&gt;
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF"><tt class="FUNCTION">
                GTK_WIDGET_STATE</tt></code>(<tt class=
                "PARAMETER"><i>widget</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 27. State Accessor</b>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-containers.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-mainloop.html"><font color="#0000ff" size= 
            "2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Containers And Widget
            Layout</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>The Main
            Loop</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

