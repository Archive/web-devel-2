<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      User Communication: Dialogs
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="Building a Gnome Application" href= 
    "build-app.html">
    <link rel="PREVIOUS" title="Finishing Touches" href="z94.html">
    <link rel="NEXT" title="Modal Dialogs" href= 
    "sec-modaldialogs.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="z94.html"><font color="#0000ff" size="2"><b>
            &lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-modaldialogs.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="CHAPTER">
      <h1>
        <a name="CHA-DIALOGS">User Communication: Dialogs</a>
      </h1>
      <div class="TOC">
        <dl>
          <dt>
            <b>Table of Contents</b>
          </dt>
          <dt>
            <a href="cha-dialogs.html#Z97">The <tt class=
            "CLASSNAME">GnomeDialog</tt> Widget</a>
          </dt>
          <dt>
            <a href="sec-modaldialogs.html">Modal Dialogs</a>
          </dt>
          <dt>
            <a href="z101.html">A Dialog Example</a>
          </dt>
          <dt>
            <a href="cha-dialogs-special.html">Special Dialog
            Types</a>
          </dt>
          <dt>
            <a href="sec-dialogs-convenience.html">Convenience
            Routines</a>
          </dt>
        </dl>
      </div>
      <p>
        Dialogs are a continuous annoyance in plain GTK+; every
        time you want to tell the user anything, you have to create
        a window, create some buttons, create a label, pack the
        buttons and label into the window, set up callbacks,
        remember to capture <span class="SYMBOL">
        "delete_event"</span>, and so on. It's a pain. Gnome saves
        you from this pain, with an easy-to-use general-purpose
        dialog widget, and several subclasses of it that implement
        common dialog types. Gnome also has easy functions for
        using modal dialogs.
      </p>
      <div class="SECT1">
        <h1 class="SECT1">
          <a name="Z97">The <tt class="CLASSNAME">GnomeDialog</tt>
          Widget</a>
        </h1>
        <p>
          Since dialogs in plain GTK+ are painstakingly constructed
          from scratch, there are at least as many ways to write a
          dialog as there are programmers. The programmer must
          decide where to place the dialog on the screen, how much
          padding to have, whether to put a separator above the
          buttons, what container to put the buttons in, what the
          keyboard shortcuts are, and so on. The premise of <tt
          class="CLASSNAME">GnomeDialog</tt> is that the programmer
          should not have to care about these things; if they're
          variable at all, the user should configure them the way
          they want. From the programmer's perspective, dialogs
          "just work."
        </p>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="Z98">Creating a Dialog</a>
          </h2>
          <p>
            A <tt class="CLASSNAME">GnomeDialog</tt> is easy to
            create. Here's a summary of the basic steps, more
            detail follows:
          </p>
          <ol type="1">
            <li>
              <p>
                Read <a href="cha-dialogs-special.html">the section
                called <i>Special Dialog Types</i></a> and decide
                whether one of the special dialog subclasses is
                appropriate. If so, skip the below steps and create
                that subclass instead.
              </p>
            </li>
            <li>
              <p>
                Create the widget with <tt class="FUNCTION">
                gnome_dialog_new()</tt>. Pass this function the
                title of the dialog (displayed by the window
                manager) and the name of each button you'd like to
                have.
              </p>
            </li>
            <li>
              <p>
                Populate <span class="STRUCTNAME">
                GNOME_DIALOG(dialog)-&gt;vbox</span> with the
                contents of your dialog.
              </p>
            </li>
            <li>
              <p>
                Plan how your dialog will work. You can connect to
                the <span class="SYMBOL">"close"</span> or <span
                class="SYMBOL">"clicked"</span> signals, as
                appropriate. You can have the dialog hide or
                destroy itself when closed. You can also have the
                dialog automatically close when clicked, or handle
                this yourself. There are a number of ways the user
                can interact with a dialog, so it's important to be
                sure the combination of settings you choose will
                work no matter what the user does.
              </p>
            </li>
          </ol>
          <p>
            To create a dialog, use <tt class="FUNCTION">
            gnome_dialog_new()</tt>, shown in <a href= 
            "cha-dialogs.html#FL-DIALOGNEW">Figure 1</a>. The
            argument list is a <span class="STRUCTNAME">
            NULL</span>-terminated list of buttons to insert in the
            dialog. For example, you might say:
          </p>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="PROGRAMLISTING">
GtkWidget* dialog;
dialog = gnome_dialog_new(_("My Dialog Title"),
                          _("OK"),
                          _("Cancel"),
                          NULL);

      
</pre>
              </td>
            </tr>
          </table>
          <p>
            This creates a dialog titled "My Dialog Title" with an
            OK and a Cancel button; the strings are marked for
            translation with the <tt class="FUNCTION">_()</tt>
            macro. The OK button will be the leftmost button in the
            dialog.
          </p>
          <div class="FIGURE">
            <a name="FL-DIALOGNEW"></a>
            <div class="FUNCSYNOPSIS">
              <a name="FL-DIALOGNEW.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-dialog.h&gt;
      
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF">GtkWidget* <tt class= 
                "FUNCTION">gnome_dialog_new</tt></code>(const
                gchar* <tt class="PARAMETER"><i>title</i></tt>, <tt
                class="PARAMETER"><i>...</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 1. <tt class="CLASSNAME">GnomeDialog</tt>
              Constructor</b>
            </p>
          </div>
          <p>
            The <tt class="CLASSNAME">GnomeDialog</tt> API numbers
            the buttons you add starting with 0; you use these
            numbers to refer to the buttons later, since you don't
            have a pointer to the automatically-created button
            widgets. In this case, the OK button is button 0, and
            the Cancel button is button 1. (Note that this is
            standard Gnome practice---OK or Yes goes first, then
            Cancel or No. In fact <tt class="FILENAME">
            libgnomeui/gnome-uidefs.h</tt> contains the macros <tt
            class="FUNCTION">GNOME_YES</tt>, <tt class="FUNCTION">
            GNOME_OK</tt>, <tt class="FUNCTION">GNOME_NO</tt>, and
            <tt class="FUNCTION">GNOME_CANCEL</tt> which represent
            the dialog button numbers for these items in a
            two-button dialog.)
          </p>
          <p>
            The above example, which specifies buttons called "OK"
            and "Cancel," is not quite correct for production code.
            Gnome provides a set of "stock buttons" for common
            button names. These ensure everyone uses "OK" instead
            of "Ok" or "OK!"; they allow translators to translate
            common strings only once; and they often insert icons
            in the buttons, making them more attractive and
            recognizable to users. You should always use stock
            buttons if possible.
          </p>
          <p>
            You can use stock buttons in <tt class="FUNCTION">
            gnome_dialog_new()</tt>. Simply substitute the stock
            button macros for the button names:
          </p>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="PROGRAMLISTING">
dialog = gnome_dialog_new(_("My Dialog Title"),
                          GNOME_STOCK_BUTTON_OK,
                          GNOME_STOCK_BUTTON_CANCEL,
                          NULL);

      
</pre>
              </td>
            </tr>
          </table>
          <p>
            Gnome includes many stock buttons, stock menu items,
            and stock pixmaps---it's a good idea to check these out
            so you don't reinvent the wheel. There's a complete
            list in <tt class="FILENAME">
            libgnomeui/gnome-stock.h</tt>.
          </p>
        </div>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="Z99">Filling in the Dialog</a>
          </h2>
          <p>
            After creating a dialog, you'll want to put something
            inside. If you just want a label inside, probably you
            should use <tt class="CLASSNAME">GnomeMessageBox</tt>
            or one of the convenience routines (such as <tt class= 
            "FUNCTION">gnome_ok_dialog()</tt>) instead of
            constructing the dialog manually. Otherwise, filling a
            dialog is very simple:
          </p>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="PROGRAMLISTING">
GtkWidget* button;
/* ... create dialog as shown earlier ... */
button = gtk_button_new_with_label(_("Push Me"));
gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)-&gt;vbox)),
                   button, 
                   TRUE, 
                   TRUE,
                   0);
      
</pre>
              </td>
            </tr>
          </table>
          <p>
            Of course you can pack the contents of <span class= 
            "STRUCTNAME">dialog-&gt;vbox</span> using the packing
            options of your choice. The above code is just an
            example.
          </p>
          <p>
            <a href="cha-dialogs.html#FIG-GNOMEDIALOG">Figure 2</a>
            shows a dialog from the Gnumeric spreadsheet, with its
            components labelled.
          </p>
          <div class="FIGURE">
            <a name="FIG-GNOMEDIALOG"></a>
            <p>
              <img src="figures/gnumeric-dialog.png">
            </p>
            <p>
              <b>Figure 2. A <tt class="CLASSNAME">GnomeDialog</tt>
              from the Gnumeric spreadsheet</b>
            </p>
          </div>
        </div>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="Z100">Handling <tt class="CLASSNAME">
            GnomeDialog</tt> Signals</a>
          </h2>
          <p>
            Now for the tricky part. You have to prepare yourself
            to handle anything the user might do to your dialog.
            Here's a brief list of possibilities; it's worth going
            over the list whenever you create a dialog:
          </p>
          <ul>
            <li>
              <p>
                Closing the dialog by pressing the Escape key
              </p>
            </li>
            <li>
              <p>
                Closing the dialog by clicking the window manager's
                close decoration
              </p>
            </li>
            <li>
              <p>
                Clicking one of the dialog's buttons
              </p>
            </li>
            <li>
              <p>
                Interacting with the contents of the dialog
              </p>
            </li>
            <li>
              <p>
                If the dialog is not modal, interacting with other
                parts of the application
              </p>
            </li>
          </ul>
          <p>
            <tt class="CLASSNAME">GnomeDialog</tt> emits two
            signals in addition to those it inherits from parent
            classes. If the user clicks one of the dialog's
            buttons, a <span class="SYMBOL">"clicked"</span> signal
            is emitted. (This is <i class="EMPHASIS">not</i> the
            <span class="SYMBOL">"clicked"</span> signal from <tt
            class="CLASSNAME">GtkButton</tt>; it's a different
            signal, emitted by <tt class="CLASSNAME">
            GnomeDialog</tt>.) A <tt class="CLASSNAME">
            GnomeDialog</tt><span class="SYMBOL">"clicked"</span>
            handler should have three arguments: the dialog
            emitting the signal, the number of the button clicked,
            and your callback data.
          </p>
          <p>
            <tt class="CLASSNAME">GnomeDialog</tt> also has a <span
            class="SYMBOL">"close"</span> signal. It is emitted
            when <tt class="FUNCTION">gnome_dialog_close()</tt> is
            called; all the built-in event handlers (e.g. for the
            Escape shortcut) call this function to close the
            dialog. <tt class="CLASSNAME">GnomeDialog</tt>'s
            default handler for <span class="SYMBOL">"close"</span>
            has two possible behaviors: it can call either <tt
            class="FUNCTION">gtk_widget_hide()</tt> or <tt class= 
            "FUNCTION">gtk_widget_destroy()</tt> on the dialog. The
            behavior is configurable by calling <tt class=
            "FUNCTION">gnome_dialog_close_hides()</tt>, shown in <a
            href="cha-dialogs.html#FL-CLOSINGDIALOG">Figure 3</a>.
          </p>
          <div class="FIGURE">
            <a name="FL-CLOSINGDIALOG"></a>
            <div class="FUNCSYNOPSIS">
              <a name="FL-CLOSINGDIALOG.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-dialog.h&gt;
      
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_close_hides</tt></code>(GnomeDialog*
                <tt class="PARAMETER"><i>dialog</i></tt>, gboolean
                <tt class="PARAMETER"><i>setting</i></tt>);</code>
              </p>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_set_close</tt></code>(GnomeDialog* <tt
                class="PARAMETER"><i>dialog</i></tt>, gboolean <tt
                class="PARAMETER"><i>setting</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 3. Closing <tt class="CLASSNAME">
              GnomeDialog</tt></b>
            </p>
          </div>
          <p>
            By default, <span class="SYMBOL">"close"</span>
            destroys the dialog. This is what you usually want;
            however, if a dialog is noticeably time-consuming to
            create, you might want to merely hide and re-show it
            between uses, without ever destroying it. You might
            also want to hide the dialog from the user, extract the
            state of any widgets inside it, and then destroy it
            with <tt class="FUNCTION">gtk_widget_destroy()</tt>.
            The decision depends on the structure of your code.
            However, in general it is simpler and less error-prone
            to let the dialog be destroyed when clicked. You can
            connect to the <span class="SYMBOL">"clicked"</span>
            signal if you need to query the state of widgets in the
            dialog.
          </p>
          <p>
            If you connect a handler to <span class="SYMBOL">
            "close"</span>, that handler should return a boolean
            value. If it returns <span class="STRUCTNAME">
            TRUE</span>, the hide or destroy will not take place.
            You can use this to keep the user from closing the
            dialog, for example if they have not filled in all the
            fields of a form.
          </p>
          <p>
            The <span class="SYMBOL">"close"</span> signal is
            designed to collect several possible user actions into
            a single handler: it should be emitted when the user
            presses Escape or the window manager's window close
            button is clicked. It's often convenient to emit close
            when the dialog's buttons are clicked as well. You can
            ask <tt class="CLASSNAME">GnomeDialog</tt> to emit
            close whenever a button is clicked with <tt class= 
            "FUNCTION">gnome_dialog_set_close()</tt> (<a href= 
            "cha-dialogs.html#FL-CLOSINGDIALOG">Figure 3</a>); if
            its <span class="STRUCTNAME">setting</span> argument is
            <span class="STRUCTNAME">TRUE</span>, the dialog will
            emit <span class="SYMBOL">"close"</span> in addition to
            <span class="SYMBOL">"clicked"</span> if any of its
            buttons are clicked. By default, this setting is <span
            class="STRUCTNAME">FALSE</span> for <tt class=
            "CLASSNAME">GnomeDialog</tt>, but for many of the
            special dialog types the default is <span class= 
            "STRUCTNAME">TRUE</span> (the inconsistency is an
            unfortunate misfeature).
          </p>
          <p>
            Note that the <span class="SYMBOL">"close"</span>
            signal is emitted when the dialog receives <span class= 
            "SYMBOL">"delete_event"</span>; this means you only
            have to write one signal handler to deal with all
            dialog closings. There is no need to handle <span
            class="SYMBOL">"delete_event"</span> as a separate
            case.
          </p>
        </div>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="SEC-DIALOGS-TOUCHES">Finishing Touches</a>
          </h2>
          <p>
            The difference between a good dialog and a great dialog
            is in the details. <tt class="CLASSNAME">
            GnomeDialog</tt> comes with a number of features to
            make that final polish easy. <a href= 
            "cha-dialogs.html#FL-DIALOGPOLISH">Figure 4</a> sums
            them up.
          </p>
          <div class="FIGURE">
            <a name="FL-DIALOGPOLISH"></a>
            <div class="FUNCSYNOPSIS">
              <a name="FL-DIALOGPOLISH.SYNOPSIS"></a>
              <table border="0" bgcolor="#E0E0E0" width="100%">
                <tr>
                  <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-dialog.h&gt;
      
</pre>
                  </td>
                </tr>
              </table>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_set_parent</tt></code>(GnomeDialog*
                <tt class="PARAMETER"><i>dialog</i></tt>,
                GtkWindow* <tt class="PARAMETER"><i>
                parent</i></tt>);</code>
              </p>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_set_default</tt></code>(GnomeDialog*
                <tt class="PARAMETER"><i>dialog</i></tt>, gint <tt
                class="PARAMETER"><i>button</i></tt>);</code>
              </p>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_editable_enters</tt></code>(GnomeDialog*
                <tt class="PARAMETER"><i>dialog</i></tt>,
                GtkEditable* <tt class="PARAMETER"><i>
                editable</i></tt>);</code>
              </p>
              <p>
                <code><code class="FUNCDEF">void <tt class=
                "FUNCTION">
                gnome_dialog_set_sensitive</tt></code>(GnomeDialog*
                <tt class="PARAMETER"><i>dialog</i></tt>, gint <tt
                class="PARAMETER"><i>button</i></tt>, gboolean <tt
                class="PARAMETER"><i>setting</i></tt>);</code>
              </p>
            </div>
            <p>
              <b>Figure 4. <tt class="CLASSNAME">GnomeDialog</tt>
              Polish</b>
            </p>
          </div>
          <p>
            Dialogs have a logical <i class="FIRSTTERM">parent</i>,
            usually the main application window. You can tell the
            library about this parent-child relationship; this lets
            Gnome honor certain user preferences, and in turn
            indicates the relationship to the window manager. Most
            window managers will minimize child dialogs when the
            parent window is minimized, and keep child dialogs on
            top of their parent.
          </p>
          <p>
            It's important to use <tt class="FUNCTION">
            gnome_dialog_set_parent()</tt> with <i class=
            "FIRSTTERM">transient</i> dialogs only. A transient
            dialog is one that appears and is dismissed relatively
            quickly. (<tt class="CLASSNAME">GnomeDialog</tt> is
            really meant for transient dialogs.) Some "dialogs" are
            just small windows, such as the tool palette in the
            Gimp. These persistent ("floating") dialogs should be
            minimizable without minimizing the parent, and they
            should not be forced to stay above the parent window.
          </p>
          <p>
            Your dialog should have a sensible <i class=
            "FIRSTTERM">default button</i>---this is the button
            activated when the user presses the Enter key. <tt
            class="FUNCTION">gnome_dialog_set_default()</tt>
            specifies the default button. It's a judgment call
            which button should be the default. Often the best
            choice is the least-destructive action (i.e., "Cancel"
            rather than "OK"), but if neither is destructive, user
            convenience might guide your decision.
          </p>
          <p>
            Typically, operations such as deleting data or quitting
            an application have "Cancel" or "No" as the default;
            dialogs that ask the user to enter text or other
            information typically have "OK" as the default.
            Remember that many window managers will focus windows
            when they pop up, so keystrokes users intend to go to
            their current application might go to your dialog
            instead. If your dialog has "delete all my files" as
            the default button, you will get hate mail.
          </p>
          <p>
            Editable widgets emit the <span class="SYMBOL">
            "activate"</span> signal when Enter is pressed.
            Typically users expect Enter to activate the default
            dialog button, but if you have an editable widget such
            as <tt class="CLASSNAME">GtkEntry</tt> in your dialog,
            it will capture any Enter presses, and keep the
            dialog's buttons from responding to them. <tt class= 
            "FUNCTION">gnome_dialog_editable_enters()</tt>
            activates the dialog's default button when the <tt
            class="CLASSNAME">GtkEditable</tt> is activated,
            solving the problem.
          </p>
          <p>
            <tt class="FUNCTION">gnome_dialog_set_sensitive()</tt>
            calls <tt class="FUNCTION">
            gtk_widget_set_sensitive()</tt> on <span class= 
            "STRUCTNAME">button</span>. If clicking a button makes
            no sense at a given time it should be desensitized.
          </p>
          <p>
            Finally, you should make sure you do not create
            multiple instances of a dialog. Many applications allow
            you to pop up multiple Preferences or About dialogs;
            users will not trigger this bug very often, but it is a
            nice touch to avoid the problem. The following code
            deals with it in a simple way (note that the details of
            creating and showing the dialog have been omitted).
          </p>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="PROGRAMLISTING">
void 
do_dialog()
{
  static GtkWidget* dialog = NULL;

  if (dialog != NULL) 
    {
      /* This code tries to de-iconify and raise the dialog. 
       * It assumes the dialog is realized; if you can't 
       * ensure that, check that dialog-&gt;window != NULL.
       */

      gdk_window_show(dialog-&gt;window);
      gdk_window_raise(dialog-&gt;window);
    }
  else
    {
      dialog = gnome_dialog_new();      /* Arguments elided. */

      gtk_signal_connect(GTK_OBJECT(dialog),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &amp;dialog);

      /* Show the dialog, connect callbacks, etc. here */                   
    }
}
      
</pre>
              </td>
            </tr>
          </table>
          <p>
            <tt class="FUNCTION">gtk_widget_destroyed()</tt> is
            defined in <tt class="FILENAME">gtk/gtkwidget.h</tt>,
            and simply assigns <span class="STRUCTNAME">NULL</span>
            to its second argument. The code resets the <span
            class="STRUCTNAME">dialog</span> variable each time the
            user closes the dialog, and raises/deiconifies the
            dialog if the user tries to open it while another one
            is active. Note that the window manager has some say in
            whether the raise/deiconify is successful, so it is not
            guaranteed to happen.
          </p>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="z94.html"><font color="#0000ff" size="2"><b>
            &lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-modaldialogs.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Finishing
            Touches</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Modal
            Dialogs</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

