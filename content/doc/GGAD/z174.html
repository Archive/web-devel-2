<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Basic Canvas Architecture
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GnomeCanvas" href="cha-canvas.html">
    <link rel="PREVIOUS" title="GnomeCanvas" href=
    "cha-canvas.html">
    <link rel="NEXT" title="Using the Canvas" href="z177.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-canvas.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z177.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="Z174">Basic Canvas Architecture</a>
      </h1>
      <p>
        This section introduces the architecture of the canvas,
        including the arrangement of items into hierarchical
        groups, and the many coordinate systems involved in using
        the canvas.
      </p>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z175"><span class="STRUCTNAME">
          GnomeCanvasGroup</span></a>
        </h2>
        <p>
          Canvas items are arranged in a tree structure. You can
          group items together, to be moved as a unit; canvas
          architect Federico Mena Quintero likes to use a circuit
          diagram editor as an example. You might group together
          the shapes representing each logic gate, so you could
          manipulate the logic gate as a unit. You could also
          collect several logic gates into a single component; that
          is, groups can contain subgroups. Within each group, the
          canvas maintains a stacking order; objects higher in the
          stacking order obscure objects lower in the stacking
          order.
        </p>
        <p>
          To implement this, the canvas comes with a special kind
          of canvas item called <span class="STRUCTNAME">
          GnomeCanvasGroup</span>. As the name suggests, a canvas
          group groups a number canvas items together so you can
          manipulate the child items as a single item. A <span
          class="STRUCTNAME">GnomeCanvasGroup</span> is invisible;
          to render itself, it simply recurses its children,
          rendering each of them in turn. When you create a new
          <span class="STRUCTNAME">GnomeCanvas</span>, a default
          group called the "root" is created for you. All canvas
          items are added somewhere below the root group. The
          canvas widget only deals with the root canvas item
          directly; all other canvas items are managed by their
          parent group.
        </p>
        <p>
          An accessor function is provided to access the root
          canvas group, shown in <a href="z174.html#FL-CANVASROOT">
          Figure 1</a>.
        </p>
        <div class="FIGURE">
          <a name="FL-CANVASROOT"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-CANVASROOT.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-canvas.h&gt;
      
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">GnomeCanvasGroup* <tt
              class="FUNCTION">
              gnome_canvas_root</tt></code>(GnomeCanvas* <tt class= 
              "PARAMETER"><i>canvas</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 1. Root Group Accessor</b>
          </p>
        </div>
        <p>
          Items must always be part of a group; there is no such
          thing as an "orphan" canvas item. When you create an
          item, you must specify its canvas group. It is also
          possible to reparent items after creation. However, items
          are permanently bound to the <tt class="CLASSNAME">
          GnomeCanvas</tt> they were created on; it is not
          permitted to reparent an item to a group on a different
          canvas.
        </p>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z176">Coordinates</a>
        </h2>
        <p>
          Many of the features of the canvas are implemented via
          translations between different coordinate systems. Canvas
          items can be moved, rotated, or scaled via <i class= 
          "FIRSTTERM">affine transformations</i>, described in more
          detail below. (Short version: an affine transformation is
          a way to convert from one coordinate system to another.)
          Here are the important coordinate systems which come up
          when using the canvas and writing custom canvas items:
        </p>
        <ul>
          <li>
            <p>
              <i class="FIRSTTERM">World coordinates</i> are an
              absolute coordinate system; i.e., the same world
              coordinate refers to the same place on the canvas in
              all cases. World coordinates are conceptually
              infinite and are represented by a <span class= 
              "STRUCTNAME">double</span>. World coordinates are the
              real, toplevel, untransformed, canonical coordinate
              system. Consistent with the X Window System and GDK,
              Y coordinates increase as they move <i class=
              "EMPHASIS">downward</i>, so lower Y coordinates are
              toward the top of the canvas.
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">Item coordinates</i> are the
              coordinates used by a particular canvas item. Item
              coordinates exist because each canvas item has an
              affine transformation associated with it. In the case
              of <span class="STRUCTNAME">GnomeCanvasGroup</span>,
              this transformation is applied to the group's
              children. To convert from world coordinates to item
              coordinates for some particular item, you apply the
              transform for each canvas group in the item's
              ancestry, starting with the root canvas group; then
              you apply the item's own transform. (Don't worry,
              Gnome comes with a function to do this for you.) Like
              world coordinates, item coordinates are conceptually
              infinite.
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">Canvas coordinates</i> are pixel
              coordinates. While item and world coordinates are
              floating-point numbers, canvas pixel coordinates are
              integers. To use the canvas, you must specify a
              "scroll region," which is the rectangle in world
              coordinate space you want the user to be able to see.
              Canvas pixel coordinates are relative to this
              rectangle. Canvas pixel coordinates also take into
              account a scaling factor representing the number of
              pixels per world coordinate unit. To convert from
              world coordinates to canvas coordinates, the canvas
              subtracts the X and Y coordinates of the scroll
              region, multiplies by the scaling factor, and then
              rounds to an integer. Thus, <tt class="APPLICATION">
              (0,0)</tt> in canvas coordinates will be the top left
              corner of the scroll region.
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">Buffer coordinates</i> are
              canvas coordinates modified by some offset. Item
              implementations use these during rendering. The
              canvas passes the item implementation a buffer (which
              is either a <span class="STRUCTNAME">
              GdkDrawable</span> or an RGB buffer, depending on the
              canvas mode). The canvas tells the item
              implementation which region of the screen the buffer
              represents---the buffer region is defined by an X
              offset, Y offset, width and height. The X and Y
              offsets are in canvas coordinates, and are equivalent
              to <tt class="APPLICATION">(0,0)</tt> in buffer
              coordinates. To convert from canvas coordinates to
              buffer coordinates, simply subtract the offset.
              Buffer coordinates are only valid from <tt class= 
              "APPLICATION">(0,0)</tt> to the maximum width and
              height of the buffer.
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">Window coordinates</i> are
              rarely used. The canvas eventually copies each
              temporary buffer to a <span class="STRUCTNAME">
              GdkWindow</span> (to be precise,it copies them to
              <span class="STRUCTNAME">
              GTK_LAYOUT(canvas)-&gt;bin_window</span>). Window
              coordinates are relative to this <span class= 
              "STRUCTNAME">GdkWindow</span>. In some rare cases you
              might want to draw to the window directly rather than
              using a canvas item, or you might want to respond to
              an event on the window (such as a drag-and-drop).
              Then you need to convert from window coordinates to
              one of the other coordinate systems.
            </p>
          </li>
        </ul>
        <p>
          When using preexisting canvas items, you will mostly be
          interested in world and item coordinates. When writing
          your own items, you will also need to use canvas and
          buffer coordinates.
        </p>
        <p>
          There are two ways to convert between the various
          coordinate systems; one way is to obtain and use affines
          directly---this is described in the next section. The
          easy way is to use one of the convenience functions
          provided for the purpose, shown in <a href= 
          "z174.html#FL-COORDCONVERT">Figure 2</a>. Conversion
          between canvas and item coordinates requires you to
          convert to world coordinates first as an intermediate
          step. There is no function to convert to or from buffer
          coordinates, because this is a simple matter of
          subtracting the buffer offsets from the canvas
          coordinates (canvas to buffer), or adding the buffer
          offsets to the buffer coordinates (buffer to canvas).
        </p>
        <div class="FIGURE">
          <a name="FL-COORDCONVERT"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-COORDCONVERT.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-canvas.h&gt;
      
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gnome_canvas_w2c</tt></code>(GnomeCanvas*
              <tt class="PARAMETER"><i>canvas</i></tt>, double <tt
              class="PARAMETER"><i>wx</i></tt>, double <tt class= 
              "PARAMETER"><i>wy</i></tt>, int* <tt class=
              "PARAMETER"><i>cx</i></tt>, int* <tt class=
              "PARAMETER"><i>cy</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_w2c_d</tt></code>(GnomeCanvas* <tt
              class="PARAMETER"><i>canvas</i></tt>, double <tt
              class="PARAMETER"><i>wx</i></tt>, double <tt class= 
              "PARAMETER"><i>wy</i></tt>, double* <tt class= 
              "PARAMETER"><i>cx</i></tt>, double* <tt class= 
              "PARAMETER"><i>cy</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gnome_canvas_c2w</tt></code>(GnomeCanvas*
              <tt class="PARAMETER"><i>canvas</i></tt>, int <tt
              class="PARAMETER"><i>cx</i></tt>, int <tt class= 
              "PARAMETER"><i>cy</i></tt>, double* <tt class= 
              "PARAMETER"><i>wx</i></tt>, double* <tt class= 
              "PARAMETER"><i>wy</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_item_w2i</tt></code>(GnomeCanvasItem*
              <tt class="PARAMETER"><i>item</i></tt>, double* <tt
              class="PARAMETER"><i>x</i></tt>, double* <tt class= 
              "PARAMETER"><i>y</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_item_i2w</tt></code>(GnomeCanvasItem*
              <tt class="PARAMETER"><i>item</i></tt>, double* <tt
              class="PARAMETER"><i>x</i></tt>, double* <tt class= 
              "PARAMETER"><i>y</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_window_to_world</tt></code>(GnomeCanvas*
              <tt class="PARAMETER"><i>canvas</i></tt>, double <tt
              class="PARAMETER"><i>winx</i></tt>, double <tt class= 
              "PARAMETER"><i>winy</i></tt>, double* <tt class= 
              "PARAMETER"><i>worldx</i></tt>, double* <tt class= 
              "PARAMETER"><i>worldy</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_world_to_window</tt></code>(GnomeCanvas*
              <tt class="PARAMETER"><i>canvas</i></tt>, double <tt
              class="PARAMETER"><i>worldx</i></tt>, double <tt
              class="PARAMETER"><i>worldy</i></tt>, double* <tt
              class="PARAMETER"><i>winx</i></tt>, double* <tt
              class="PARAMETER"><i>winy</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 2. Coordinate Conversions</b>
          </p>
        </div>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="SEC-AFFINES">Affine Transformations</a>
        </h2>
        <p>
          An <i class="FIRSTTERM">affine</i> is a transformation
          matrix made up of six real numbers that can be <i class= 
          "FIRSTTERM">applied</i> to an ordered pair. Depending on
          the contents of the affine, the point it is applied to
          can be:
        </p>
        <ul>
          <li>
            <p>
              <i class="FIRSTTERM">translated</i>---shifted by an
              arbitrary distance in either dimension;
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">rotated</i> some number of
              degrees;
            </p>
          </li>
          <li>
            <p>
              <i class="FIRSTTERM">scaled</i> by some factor.
            </p>
          </li>
        </ul>
        <p>
          Conceptually, an affine defines a relationship between
          points on a plane. For any point (A,B), the affine
          defines a single corresponding transformed point; the
          mapping is one-to-one, so given the transformed point you
          can determine the original point.
        </p>
        <p>
          Affines have interesting properties that make them useful
          in computer graphics. Most importantly, they can be <i
          class="FIRSTTERM">composed</i>, <i class="FIRSTTERM">
          concatenated</i>, or <i class="FIRSTTERM">multiplied</i>
          (the three terms are synonymous). You can compose any
          number of affines to create a single affine; applying the
          single affine has the same effect as applying each of the
          original affines in order. Note that the order of
          composition is important! Unlike multiplication, affine
          composition is not commutative (which is a reason to
          avoid the term "multiply" in this context).
        </p>
        <p>
          <tt class="APPLICATION">libart_lgpl</tt> contains a
          module for affine manipulation. It represents affines as
          an array of six doubles. Its affine functions are shown
          in <a href="z174.html#FL-AFFINES">Figure 3</a>.
        </p>
        <div class="FIGURE">
          <a name="FL-AFFINES"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-AFFINES.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libart_lgpl/art_affine.h&gt;
      
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_point</tt></code>(ArtPoint* <tt
              class="PARAMETER"><i>dst</i></tt>, const ArtPoint*
              <tt class="PARAMETER"><i>src</i></tt>, const double
              <tt class="PARAMETER"><i>affine[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_invert</tt></code>(double <tt
              class="PARAMETER"><i>dst_affine[6]</i></tt>, const
              double <tt class="PARAMETER"><i>
              src_affine[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_multiply</tt></code>(double <tt
              class="PARAMETER"><i>dst[6]</i></tt>, const double
              <tt class="PARAMETER"><i>src1[6]</i></tt>, const
              double <tt class="PARAMETER"><i>
              src2[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_identity</tt></code>(double <tt
              class="PARAMETER"><i>dst[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_scale</tt></code>(double <tt
              class="PARAMETER"><i>dst[6]</i></tt>, double <tt
              class="PARAMETER"><i>sx</i></tt>, double <tt class= 
              "PARAMETER"><i>sy</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_rotate</tt></code>(double <tt
              class="PARAMETER"><i>dst[6]</i></tt>, double <tt
              class="PARAMETER"><i>theta</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">art_affine_translate</tt></code>(double
              <tt class="PARAMETER"><i>dst[6]</i></tt>, double <tt
              class="PARAMETER"><i>tx</i></tt>, double <tt class= 
              "PARAMETER"><i>ty</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">int <tt class="FUNCTION">
              art_affine_rectilinear</tt></code>(const double <tt
              class="PARAMETER"><i>src[6]</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 3. Affine Manipulation</b>
          </p>
        </div>
        <p>
          <tt class="FUNCTION">art_affine_point()</tt> applies an
          affine to a point. The affine is applied to the second
          argument (<span class="STRUCTNAME">src</span>) and the
          result is copied into the first argument (<span class= 
          "STRUCTNAME">dst</span>). An <span class="STRUCTNAME">
          ArtPoint</span> is simply:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _ArtPoint ArtPoint;

struct _ArtPoint {
  double x, y;
};

      
</pre>
            </td>
          </tr>
        </table>
        <p>
          Affines can be <i class="FIRSTTERM">inverted</i>. If an
          affine converts points in coordinate system A into points
          in coordinate system B, its inverse converts points in
          coordinate system B into points in coordinate system A.
          <tt class="FUNCTION">art_affine_invert()</tt> fills its
          first argument with the inverse of its second.
        </p>
        <p>
          <tt class="FUNCTION">art_affine_multiply()</tt> composes
          two affines as described earlier in this section, placing
          the result in its first argument.
        </p>
        <p>
          Four functions are provided to create affines with
          particular properties.
        </p>
        <ul>
          <li>
            <p>
              <tt class="FUNCTION">art_affine_identity()</tt>
              creates the identity affine. Applying the identity
              affine to a point has no effect.
            </p>
          </li>
          <li>
            <p>
              <tt class="FUNCTION">art_affine_rotate()</tt> gives
              an affine that rotates points by <span class= 
              "STRUCTNAME">theta</span> degrees.
            </p>
          </li>
          <li>
            <p>
              <tt class="FUNCTION">art_affine_translate()</tt>
              gives an affine that translates points <span class= 
              "STRUCTNAME">tx</span> in the X dimension and <span
              class="STRUCTNAME">ty</span> in the Y dimension.
            </p>
          </li>
          <li>
            <p>
              <tt class="FUNCTION">art_affine_scale()</tt> gives an
              affine which scales the plane by the given factors (a
              factor of 1.0 does no scaling, less than 1.0 shrinks,
              greater than 1.0 expands).
            </p>
          </li>
        </ul>
        <p>
          <tt class="FUNCTION">art_affine_rectilinear()</tt>
          returns <span class="STRUCTNAME">TRUE</span> if the
          affine rotates rectangles aligned to the axes in such a
          way that they remain aligned to the axes. That is, it
          returns <span class="STRUCTNAME">TRUE</span> if the
          rotation is 0, 90, 180, or 270 degrees.
        </p>
        <p>
          You can ask the canvas widget to compute affines which
          convert between its various coordinate systems. These
          functions are shown in <a href= 
          "z174.html#FL-CANVAS-AFFINES">Figure 4</a>; each of them
          fills an array you pass in with the affine being
          requested.
        </p>
        <div class="FIGURE">
          <a name="FL-CANVAS-AFFINES"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-CANVAS-AFFINES.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
       #include &lt;libgnomeui/gnome-canvas.h&gt;
      
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_item_i2w_affine</tt></code>(GnomeCanvasItem*
              <tt class="PARAMETER"><i>item</i></tt>, double <tt
              class="PARAMETER"><i>affine[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_item_i2c_affine</tt></code>(GnomeCanvasItem*
              <tt class="PARAMETER"><i>item</i></tt>, double <tt
              class="PARAMETER"><i>affine[6]</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gnome_canvas_w2c_affine</tt></code>(GnomeCanvas* <tt
              class="PARAMETER"><i>canvas</i></tt>, double <tt
              class="PARAMETER"><i>affine[6]</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 4. Canvas Affines</b>
          </p>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-canvas.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z177.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b><tt class=
            "CLASSNAME">GnomeCanvas</tt></b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Using the
            Canvas</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

