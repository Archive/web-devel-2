<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      GtkArg and the Type System
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="The GTK+ Object and Type System" href= 
    "cha-objects.html">
    <link rel="PREVIOUS" title="Initializing a New Class" href= 
    "sec-classinit.html">
    <link rel="NEXT" title="Object Arguments" href= 
    "hc-objectargs.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-classinit.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="hc-objectargs.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-GTKARG"><span class="STRUCTNAME">GtkArg</span>
        and the Type System</a>
      </h1>
      <p>
        Before delving further into <span class="STRUCTNAME">
        GtkObject</span>, you will need more details on GTK+'s type
        system. The type system is used in many contexts:
      </p>
      <ul>
        <li>
          <p>
            It allows signals and callbacks with any signature to
            be dynamically registered and dynamically queried.
            Function argument lists can be constructed at
            runtime.
          </p>
        </li>
        <li>
          <p>
            It allows object attributes (values that you can "get"
            or "set") to be dynamically queried and
            manipulated.
          </p>
        </li>
        <li>
          <p>
            It exports information about enumerations and bitfields
            (lists of permitted values, and human-readable
            names).
          </p>
        </li>
        <li>
          <p>
            It is possible to identify types at runtime and
            traverse the object class hierarchy.
          </p>
        </li>
      </ul>
      <p>
        Because of its type system, GTK+ is particularly easy to
        manipulate from dynamically-typed, interactive languages.
        There are bindings available for nearly all popular
        languages, and the bindings can be lightweight (since GTK+
        already includes much of the needed functionality, and
        types can be handled generically so the amount of glue code
        is reduced). You can find a complete list of functions for
        querying and using GTK+'s type system in <tt class=
        "FILENAME">gtk/gtktypeutils.h</tt>; most of these are not
        useful in applications. Only the functions of general
        interest are described in this book.
      </p>
      <p>
        GTK+ has a number of so-called <i class="FIRSTTERM">
        fundamental</i> types which are automatically registered
        during <tt class="FUNCTION">gtk_init()</tt> (or <tt class= 
        "FUNCTION">gnome_init()</tt>). The fundamental types
        include all the primitive C types, some GTK+ types such as
        <span class="STRUCTNAME">GTK_TYPE_SIGNAL</span>, and <span
        class="STRUCTNAME">GTK_TYPE_OBJECT</span>. Fundamental
        types are essentially the "base classes" understood by the
        GTK+ type system; for example, the fundamental type of any
        enumeration is <span class="STRUCTNAME">
        GTK_TYPE_ENUM</span>, and the fundamental type of any <span
        class="STRUCTNAME">GtkObject</span> subclass is <span
        class="STRUCTNAME">GTK_TYPE_OBJECT</span>. Fundamental
        types are supposed to cover all the "special cases" in the
        GTK+ type system; all types ultimately derive from some
        fundamental type. A type's fundamental type is extracted
        from a <span class="STRUCTNAME">GtkType</span> with the <tt
        class="FUNCTION">GTK_FUNDAMENTAL_TYPE()</tt> macro. The
        fundamental types are shown in <a href= 
        "sec-gtkarg.html#TABLE-FUNDTYPES">Table 1</a>.
      </p>
      <p>
        There is a second category of <span class="STRUCTNAME">
        GtkType</span> values: <i class="FIRSTTERM">builtin</i>
        types are registered by GTK+ and <tt class="APPLICATION">
        libgnomeui</tt> during library initialization and are thus
        always available. Builtin types include enumerations,
        flags, and some structs (<span class=
        "STRUCTNAME">GdkWindow</span>, or <span class="STRUCTNAME">
        GdkImlibImage</span>, for example). Builtin types are
        distinct from fundamental types because the GTK+ object
        system does not have to understand them; for the purposes
        of getting and setting argument values, they can be treated
        as fundamental types. They are somewhat arbitrarily
        distinguished from user-registered enumeration or flag
        types. (The difference between builtin types and user types
        is the time of registration.)
      </p>
      <p>
        Builtin types are all accessible via macros that come with
        GTK+ and Gnome. These begin with <span class="STRUCTNAME">
        GTK_TYPE_</span>, as in: <span class="STRUCTNAME">
        GTK_TYPE_WINDOW</span>, <span class="STRUCTNAME">
        GTK_TYPE_GDK_WINDOW</span>, <span class="STRUCTNAME">
        GTK_TYPE_RELIEF_STYLE</span>, <span class="STRUCTNAME">
        GTK_TYPE_GNOME_DIALOG</span>. As you can see, the name of
        the type macro is derived from the name of the <span class= 
        "STRUCTNAME">GtkObject</span>, struct, or enumeration; if
        the object name begins with "Gtk," the "Gtk" is dropped.
        The above examples map to the <tt class="CLASSNAME">
        GtkWindow</tt> widget, <span class="STRUCTNAME">
        GdkWindow</span> struct, <span class="STRUCTNAME">
        GtkReliefStyle</span> enumeration, and <span class= 
        "STRUCTNAME">GnomeDialog</span> widget, respectively.
      </p>
      <p>
        The final major category of <span class="STRUCTNAME">
        GtkType</span> values consists of the registered <span
        class="STRUCTNAME">GtkObject</span> types. These are
        registered the first time the <span class="STRUCTNAME">
        _get_type()</span> routine for each object is called.
      </p>
      <div class="TABLE">
        <a name="TABLE-FUNDTYPES"></a>
        <p>
          <b>Table 1. The GTK+ Fundamental Types</b>
        </p>
        <table border="1" bgcolor="#E0E0E0" cellspacing="0"
        cellpadding="4" class="CALSTABLE">
          <tr>
            <th align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GtkType</span> Constant
            </th>
            <th align="LEFT" valign="TOP">
              Corresponding C Type
            </th>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_INVALID</span>
            </td>
            <td align="LEFT" valign="TOP">
              None
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_NONE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">void</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_CHAR</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gchar</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_UCHAR</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">guchar</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_BOOL</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gboolean</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_INT</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gint</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_UINT</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">guint</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_LONG</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">glong</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_ULONG</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gulong</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_FLOAT</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gfloat</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_DOUBLE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gdouble</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_STRING</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gchar*</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_ENUM</span>
            </td>
            <td align="LEFT" valign="TOP">
              Any enumeration
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_FLAGS</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">guint</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_BOXED</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gpointer</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_POINTER</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gpointer</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_SIGNAL</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GtkSignalFunc</span>, <span
              class="STRUCTNAME">gpointer</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_ARGS</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gint</span>, <span class= 
              "STRUCTNAME">GtkArg*</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_CALLBACK</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GtkCallbackMarshal</span>,
              <span class="STRUCTNAME">gpointer</span>, <span
              class="STRUCTNAME">GtkDestroyNotify</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_C_CALLBACK</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GtkFunction</span>, <span
              class="STRUCTNAME">gpointer</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_FOREIGN</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">gpointer</span>, <span
              class="STRUCTNAME">GtkDestroyNotify</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GTK_TYPE_OBJECT</span>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GtkObject*</span>
            </td>
          </tr>
        </table>
      </div>
      <p>
        Some of the fundamental types require further explanation.
        In brief:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_INVALID</span>: used
            to signal errors.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_NONE</span>: used to
            indicate a <span class="STRUCTNAME">void</span> return
            value when specifying the signature of a signal.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_BOXED</span>:
            Subtypes of <span class="STRUCTNAME">
            GTK_TYPE_BOXED</span> are used to mark the type of a
            generic pointer; language bindings will special case
            these types. Most GDK types, such as <span class= 
            "STRUCTNAME">GdkWindow</span>, are registered as boxed
            types.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_SIGNAL</span>:
            special-cased in <span class="STRUCTNAME">
            GtkObject</span>; it allows users to connect signal
            handlers with <tt class="FUNCTION">
            gtk_object_set()</tt>. It should not be useful in
            application code. 
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_ARGS</span>: type of
            an array of <span class="STRUCTNAME">GtkArg</span>
            (when used with <tt class="FUNCTION">
            gtk_object_set()</tt>, an integer array length followed
            by the array itself are expected as arguments).
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_CALLBACK</span>:
            interpreted language bindings can use this to pass
            signal callbacks around.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_C_CALLBACK</span>:
            this is used for other kinds of callbacks, i.e.
            callbacks that are not attached to signals (such as the
            argument to a <span class="STRUCTNAME">
            _foreach()</span> function).
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GTK_TYPE_FOREIGN</span>:
            unused in current GTK+ code. Represents a pointer plus
            a function used to destroy the pointed-to resource;
            intended to represent object data (see <a href= 
            "sec-objectdata.html">the section called <i>Attaching
            Data to Objects</i></a>), for example.
          </p>
        </li>
      </ul>
      <p>
        A fundamental type describes not only describe the data
        layout but also how memory is managed. For values passed in
        as arguments, the called function is not allowed to retain
        the pointer beyond the duration of the call. For returned
        values, the caller assumes ownership of the memory. <span
        class="STRUCTNAME">GTK_TYPE_BOXED</span>, <span class= 
        "STRUCTNAME">GTK_TYPE_ARGS</span>, and <span class= 
        "STRUCTNAME">GTK_TYPE_STRING</span> obey this rule.
      </p>
      <p>
        Note that you should almost always use the most informative
        type available. Notably, <span class="STRUCTNAME">
        GTK_TYPE_POINTER</span> should only be used for generic
        pointers (<span class="STRUCTNAME">gpointer</span>);
        whenever possible, prefer a "subclass" of <span class= 
        "STRUCTNAME">GTK_TYPE_BOXED</span> such as <span class= 
        "STRUCTNAME">GTK_TYPE_GDK_WINDOW</span> or <span class= 
        "STRUCTNAME">GTK_TYPE_GDK_EVENT</span>. Similarly, it is
        better to use a specific enumeration type, rather than
        <span class="STRUCTNAME">GTK_TYPE_ENUM</span>. <span class= 
        "STRUCTNAME">GTK_TYPE_CALLBACK</span> is normally preferred
        to <span class="STRUCTNAME">GTK_TYPE_C_CALLBACK</span> or
        <span class="STRUCTNAME">GTK_TYPE_SIGNAL</span>, because
        <span class="STRUCTNAME">GTK_TYPE_CALLBACK</span> includes
        information about how to marshal the function and destroy
        the callback data.
      </p>
      <p>
        GTK+ has a consistent interface for passing typed values
        around; to do this, it needs a data structure which stores
        a type tag and a value. <span class="STRUCTNAME">
        GtkArg</span> fills the bill. Here is its definition, from
        <tt class="FILENAME">gtk/gtktypeutils.h</tt>:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
typedef struct _GtkArg GtkArg;

struct _GtkArg
{
  GtkType type;
  gchar *name;

  union {
    gchar char_data;
    guchar uchar_data;
    gboolean bool_data;
    gint int_data;
    guint uint_data;
    glong long_data;
    gulong ulong_data;
    gfloat float_data;
    gdouble double_data;
    gchar *string_data;
    gpointer pointer_data;
    GtkObject *object_data;
    
    struct {
      GtkSignalFunc f;
      gpointer d;
    } signal_data;
    struct {
      gint n_args;
      GtkArg *args;
    } args_data;
    struct {
      GtkCallbackMarshal marshal;
      gpointer data;
      GtkDestroyNotify notify;
    } callback_data;
    struct {
      GtkFunction func;
      gpointer func_data;
    } c_callback_data;
    struct {
      gpointer data;
      GtkDestroyNotify notify;
    } foreign_data;
  } d;
};
</pre>
          </td>
        </tr>
      </table>
      <p>
        The <span class="STRUCTNAME">type</span> field contains the
        value's <span class="STRUCTNAME">GtkType</span>, as you
        might expect. The <span class="STRUCTNAME">name</span>
        field is an object argument name---more on arguments in a
        moment. The final union stores a value of the appropriate
        type; there is one union member for each fundamental type.
        This value field should be accessed using a special set of
        macros provided for the purpose, listed in <a href= 
        "sec-gtkarg.html#ML-ARGVALUE">Figure 2</a>; each macro
        corresponds to a fundamental type. These macros are defined
        so that you can use the <span class="STRUCTNAME">
        &amp;</span> operator on them; e.g. <span class=
        "STRUCTNAME">&amp;GTK_VALUE_CHAR(arg)</span>.
      </p>
      <p>
        To print a <span class="STRUCTNAME">GtkArg</span>'s value,
        you might write code like this:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
   GtkArg arg;

   /* ... */

   switch (GTK_FUNDAMENTAL_TYPE (arg.type))
     {
     case GTK_TYPE_INT:
       printf("arg: %d\n", GTK_VALUE_INT(arg));
       break;
     /* ... case for each type ... */
     }
</pre>
          </td>
        </tr>
      </table>
      <div class="FIGURE">
        <a name="ML-ARGVALUE"></a>
        <div class="FUNCSYNOPSIS">
          <a name="ML-ARGVALUE.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtktypeutils.h&gt;
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_CHAR</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_UCHAR</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_BOOL</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_INT</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_UINT</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_LONG</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_ULONG</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_FLOAT</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_DOUBLE</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_STRING</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_ENUM</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_FLAGS</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_BOXED</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_POINTER</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_OBJECT</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_SIGNAL</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_ARGS</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_CALLBACK</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_C_CALLBACK</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            GTK_VALUE_FOREIGN</tt></code>(<tt class=
            "PARAMETER"><i>arg</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 2. Macros for Accessing <span class=
          "STRUCTNAME">GtkArg</span> Values</b>
        </p>
      </div>
      <p>
        Some uses of <span class="STRUCTNAME">GtkArg</span> require
        you to assign a value to it. The <span class="STRUCTNAME">
        GTK_VALUE_</span> macros are not appropriate here; instead,
        a parallel set of macros exist which return a pointer to an
        assignable location. These are called <span class= 
        "STRUCTNAME">GTK_RETLOC_CHAR()</span>, <span class= 
        "STRUCTNAME">GTK_RETLOC_UCHAR()</span>, and so on.
      </p>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-classinit.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="hc-objectargs.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Initializing a New
            Class</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Object
            Arguments</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

