<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      GdkWindow
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GDK Basics" href="cha-gdk.html">
    <link rel="PREVIOUS" title="GDK Basics" href="cha-gdk.html">
    <link rel="NEXT" title="Visuals and Colormaps" href= 
    "sec-gdkvisual.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-gdk.html"><font color="#0000ff" size="2">
            <b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkvisual.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-GDKWINDOW"><span class="STRUCTNAME">
        GdkWindow</span></a>
      </h1>
      <p>
        <span class="STRUCTNAME">GdkWindow</span> is a wrapper
        around Xlib's <span class="STRUCTNAME">Window</span>
        object. (It was discussed briefly in <a href= 
        "z57.html#SEC-REALIZINGSHOWING">the section called <i>
        Realizing, Mapping, and Showing</i> in the chapter called
        <i>GTK+ Basics</i></a>.) A <span class="STRUCTNAME">
        GdkWindow</span> represents a region on the screen. It can
        be shown or hidden (called <i class="FIRSTTERM">mapping</i>
        and <i class="FIRSTTERM">unmapping</i> the window in Xlib).
        You can capture events received by a <span class=
        "STRUCTNAME">GdkWindow</span>, draw graphics inside it, and
        move or resize it. <span class="STRUCTNAME">
        GdkWindow</span>s are arranged in a tree structure; that
        is, each window can have child windows. Child windows are
        positioned relative to their parent window, and move when
        the parent moves. Child windows don't draw outside of their
        parent's bounds (i.e. they are clipped by the parent
        window).
      </p>
      <p>
        The tree of <span class="STRUCTNAME">GdkWindow</span>s is
        not specific to each application; there is a global tree of
        windows controlled by the X server and the window manager.
        The <i class="FIRSTTERM">root window</i> has no parent; all
        windows derive from it. All or part of it is visible as
        your desktop background. Each window can be owned by a
        different UNIX process; some windows will be created by the
        window manager, some will come from user applications.
      </p>
      <p>
        <span class="STRUCTNAME">GdkWindow</span> and <tt class= 
        "CLASSNAME">GtkWindow</tt> are very different things; <tt
        class="CLASSNAME">GtkWindow</tt> is a GTK+ widget used to
        represent toplevel windows (toplevel windows are the
        highest application-controlled windows in the window
        hierarchy). Window managers typically create <i class= 
        "FIRSTTERM">decorations</i> for toplevel windows;
        decorations include title bars, close buttons, and the
        like.
      </p>
      <p>
        It's important to understand that an X window is primarily
        an object on the X server. X clients receive a unique
        integer ID for each window, and refer to windows by ID.
        Thus, all window operations take place on the server; all
        functions that deal with X windows go across the network.
      </p>
      <p>
        <span class="STRUCTNAME">GdkWindow</span> is a wrapper
        around the integer ID returned by X. It does keep local
        copies of some information (such as the window's
        dimensions), so some GDK operations are more efficient than
        the corresponding Xlib operations. Still, <span class= 
        "STRUCTNAME">GdkWindow</span> is essentially a handle for a
        server-side object. Many GDK objects are similar; fonts,
        pixmaps, cursors, and so on are also handles for
        server-side objects.
      </p>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z113"><span class="STRUCTNAME">GdkWindow</span>
          and <span class="STRUCTNAME">GtkWidget</span></a>
        </h2>
        <p>
          Many <span class="STRUCTNAME">GtkWidget</span> subclasses
          have an associated <span class="STRUCTNAME">
          GdkWindow</span>. In theory, GTK+ applications could
          create only toplevel windows, and have all widgets draw
          into them. However, it would make little sense to do so;
          <span class="STRUCTNAME">GdkWindow</span> allows the X
          Window System to automatically handle many details. For
          example, events received from GDK are marked with the
          window they occurred in; GTK+ can rapidly determine which
          widget each event corresponds to.
        </p>
        <p>
          There are some widgets with no associated <span class= 
          "STRUCTNAME">GdkWindow</span>; these are called "no
          window" widgets, an allusion to the <span class= 
          "STRUCTNAME">GTK_NO_WINDOW</span> flag that marks them.
          (You can test this flag with the macro <tt class=
          "FUNCTION">GTK_WIDGET_NO_WINDOW()</tt>.) Widgets without
          a window render themselves into their parent container's
          <span class="STRUCTNAME">GdkWindow</span>. Windowless
          widgets are relatively small and lightweight; <tt class= 
          "CLASSNAME">GtkLabel</tt> is the most common example.
          Because events are always received on a <span class= 
          "STRUCTNAME">GdkWindow</span>, windowless widgets do not
          receive events. (The <tt class="CLASSNAME">
          GtkEventBox</tt> container can be used if you need to
          capture events on a windowless widget.)
        </p>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="SEC-GDKWINATTR"><span class="STRUCTNAME">
          GdkWindow</span> Attributes</a>
        </h2>
        <p>
          <tt class="FUNCTION">gdk_window_new()</tt> (shown in <a
          href="sec-gdkwindow.html#FL-GDKWINNEW">Figure 1</a>)
          allows you to specify all of a window's attributes when
          you create it; many of them can be changed later as well.
          To specify a block of attributes, you pass in a <span
          class="STRUCTNAME">GdkWindowAttr</span> object; its
          contents will give you an idea what attributes a <span
          class="STRUCTNAME">GdkWindow</span> can have:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GdkWindowAttr GdkWindowAttr;

struct _GdkWindowAttr
{
  gchar *title;
  gint event_mask;
  gint16 x, y;
  gint16 width;
  gint16 height;
  GdkWindowClass wclass;
  GdkVisual *visual;
  GdkColormap *colormap;
  GdkWindowType window_type;
  GdkCursor *cursor;
  gchar *wmclass_name;
  gchar *wmclass_class;
  gboolean override_redirect;
};
</pre>
            </td>
          </tr>
        </table>
        <p>
          Because some of the fields in <span class="STRUCTNAME">
          GdkWindowAttr</span> are optional, <tt class="FUNCTION">
          gdk_window_new()</tt> is used with an <span class= 
          "STRUCTNAME">attributes_mask</span> to specify which
          optional fields contain valid information (bit flags are
          available representing each optional field). GDK will
          only examine the optional fields given in the mask, so
          you can let the default values remain for fields you
          aren't interested in. <a href= 
          "sec-gdkwindow.html#TAB-GDKWINDOWATTR">Table 1</a>
          summarizes them briefly; fields with no <span class= 
          "STRUCTNAME">attributes_mask</span> flag are required and
          have no default value.
        </p>
        <p>
          <tt class="FUNCTION">gdk_window_new()</tt> is typically
          used in widget implementations to create the widget's
          <span class="STRUCTNAME">GdkWindow</span>; you will
          rarely use it in any other context. <tt class="FUNCTION">
          gdk_window_destroy()</tt> destroys a window when you are
          done with it. Windows are also reference counted; be sure
          to read <a href="sec-gdkresourcemgmt.html">the section
          called <i>GDK Resource Management</i></a> for more
          details on this.
        </p>
        <div class="FIGURE">
          <a name="FL-GDKWINNEW"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-GDKWINNEW.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gdk/gdk.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">GdkWindow* <tt class= 
              "FUNCTION">gdk_window_new</tt></code>(GdkWindow* <tt
              class="PARAMETER"><i>parent</i></tt>, GdkWindowAttr*
              <tt class="PARAMETER"><i>attributes</i></tt>, gint
              <tt class="PARAMETER"><i>
              attributes_mask</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">gdk_window_destroy</tt></code>(GdkWindow*
              <tt class="PARAMETER"><i>window</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 1. <span class="STRUCTNAME">
            GdkWindow</span></b>
          </p>
        </div>
        <p>
          A <span class="STRUCTNAME">GdkWindow</span>'s title is
          only really important for toplevel windows; most window
          managers will place it in the titlebar. Usually you
          should not specify it when creating a <span class= 
          "STRUCTNAME">GdkWindow</span>, however; instead, let your
          widget's users call <tt class="FUNCTION">
          gtk_window_set_title()</tt>.
        </p>
        <p>
          The window's event mask determines which events will be
          received on this window; <a href="sec-gdkevent.html">the
          section called <i>Events</i></a> goes into more detail
          about events.
        </p>
        <p>
          The X and Y coordinates for a window are specified in
          pixels, relative to the parent window's origin. The
          origin of each window is its top left ("northwest")
          corner. Notice that a 16-bit signed integer is used; X
          windows have a maximum size of 32768 pixels. Negative
          values are allowed, but the window will be clipped by its
          parent window (only the portion inside the parent window
          will be visible).
        </p>
        <p>
          The width and height of a window are given in pixels, and
          are also 16-bit signed integers.
        </p>
        <p>
          A window's <span class="STRUCTNAME">GdkWindowClass</span>
          can have one of two values:
        </p>
        <ul>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_INPUT_OUTPUT</span> is a
              normal window.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_INPUT_ONLY</span> is a
              window that has a position and receives events but
              has no visual representation. Its children must also
              be input only. You can set the cursor and other
              attributes for an input only window, but there is no
              way to draw to it (it's invisible). Input-only
              windows are occasionally useful to capture events or
              change the cursor in a region that overlaps two
              normal windows.
            </p>
          </li>
        </ul>
        <p>
          A <i class="FIRSTTERM">visual</i> describes the
          color-handling characteristics of a display; a <i class= 
          "FIRSTTERM">colormap</i> contains the colors you can use
          to draw. <a href="sec-gdkvisual.html">the section called
          <i>Visuals and Colormaps</i></a> gives details on visuals
          and colormaps.
        </p>
        <p>
          Windows can be one of several different types, specified
          by the <span class="STRUCTNAME">GdkWindowType</span>
          enumeration:
        </p>
        <ul>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_ROOT</span> is
              the type of the GDK wrapper for the root window,
              created at initialization time.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_TOPLEVEL</span>
              is a toplevel window; the <span class="STRUCTNAME">
              parent</span> argument to <tt class="FUNCTION">
              gdk_window_new()</tt> should be <span class= 
              "STRUCTNAME">NULL</span> in this case. GDK
              automatically uses the root window as the window's
              parent.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_CHILD</span> is a
              subwindow within a toplevel window. 
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_DIALOG</span> is
              essentially the same as a toplevel window; its parent
              should be <span class="STRUCTNAME">NULL</span> and
              GDK will substitute the root window. A hint will be
              set to tell the window manager the window is a
              dialog; some window managers take this into
              account.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_TEMP</span> is
              used for popup menus and the like; it's a window that
              will exist only briefly. It's a toplevel window, so
              its parent should be <span class="STRUCTNAME">
              NULL</span>. These windows are always override
              redirect and their cursor is always the same as their
              parent's. So they will ignore those elements of the
              attributes struct.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_PIXMAP</span> is
              not a window at all; <span class="STRUCTNAME">
              GdkPixmap</span> and <span class="STRUCTNAME">
              GdkWindow</span> are almost always treated the same
              way, so GDK uses the same struct to represent them.
              They can both be considered kinds of <span class= 
              "STRUCTNAME">GdkDrawable</span>. See <a href= 
              "sec-gdkdrawable.html">the section called <i>
              Drawables and Pixmaps</i></a>.
            </p>
          </li>
          <li>
            <p>
              <span class="STRUCTNAME">GDK_WINDOW_FOREIGN</span>
              identifies a wrapper around a window not created by
              GDK.
            </p>
          </li>
        </ul>
        <p>
          Only <span class="STRUCTNAME">GDK_WINDOW_TOPLEVEL</span>,
          <span class="STRUCTNAME">GDK_WINDOW_CHILD</span>, <span
          class="STRUCTNAME">GDK_WINDOW_TEMP</span>, and <span
          class="STRUCTNAME">GDK_WINDOW_DIALOG</span> are valid for
          <tt class="FUNCTION">gdk_window_new()</tt>. Library users
          may not create a <span class="STRUCTNAME">
          GDK_WINDOW_ROOT</span>. Pixmaps (<span class=
          "STRUCTNAME">GDK_WINDOW_PIXMAP</span>) are created with
          <tt class="FUNCTION">gdk_pixmap_new()</tt>. Foreign
          windows (<span class=
          "STRUCTNAME">GDK_WINDOW_FOREIGN</span>) are X windows
          created outside of GDK and wrapped using <tt class= 
          "FUNCTION">gdk_window_foreign_new()</tt> (declared in <tt
          class="FILENAME">gdk/gdkx.h</tt>, since you will only
          need this function if you are using Xlib directly).
        </p>
        <p>
          The <span class="STRUCTNAME">cursor</span> field
          specifies the mouse pointer (cursor) to use in this
          window; see <a href="sec-gdkcursor.html">the section
          called <i>The Mouse Pointer</i></a> for information about
          cursors.
        </p>
        <p>
          The "class hint" is described in <a href= 
          "z94.html#SEC-CLASSHINT">the section called <i>Setting
          Window Class Hints</i> in the chapter called <i>The Main
          Window: <tt class="CLASSNAME">GnomeApp</tt></i></a>. When
          writing widgets, you will not usually set the class hint.
          It is only relevant for toplevel windows; GTK+ provides
          <tt class="FUNCTION">gtk_window_set_wmclass()</tt> so
          application authors can set it to something sensible.
        </p>
        <p>
          The last field in <span class="STRUCTNAME">
          GdkWindowAttr</span> determines whether the window is
          "override redirect." Normally, window managers intercept
          all requests to show, hide, move, or resize a toplevel
          window. They can then redirect or cancel these requests,
          to force windows to behave according to the window
          manager's layout policy. You can override this behavior
          by setting <span class="STRUCTNAME">
          override_redirect</span> to <span class="STRUCTNAME">
          TRUE</span>. Since window managers can not move windows
          with this flag set, they will normally not put a title
          bar or other decorations on them. Note that all <span
          class="STRUCTNAME">GDK_WINDOW_TEMP</span> windows use
          <span class="STRUCTNAME">TRUE</span> for this field;
          recall that <span class="STRUCTNAME">
          GDK_WINDOW_TEMP</span> is often used for popup menus,
          which are not controlled by the window manager.
        </p>
        <p>
          Typically you should not change the <span class= 
          "STRUCTNAME">override_redirect</span> field; the default
          is almost always right, if you specify the correct <span
          class="STRUCTNAME">GdkWindowType</span>. However, there
          are some exceptions; the Gnome panel application sets
          this field, for example.
        </p>
        <div class="TABLE">
          <a name="TAB-GDKWINDOWATTR"></a>
          <p>
            <b>Table 1. <span class="STRUCTNAME">
            GdkWindowAttr</span> Fields</b>
          </p>
          <table border="1" bgcolor="#E0E0E0" cellspacing="0"
          cellpadding="4" class="CALSTABLE">
            <tr>
              <th align="LEFT" valign="TOP">
                Field
              </th>
              <th align="LEFT" valign="TOP">
                Type
              </th>
              <th align="LEFT" valign="TOP">
                Flag
              </th>
              <th align="LEFT" valign="TOP">
                Default Value
              </th>
              <th align="LEFT" valign="TOP">
                Purpose
              </th>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">title</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gchar*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_TITLE</span>
              </td>
              <td align="LEFT" valign="TOP">
                Program Name
              </td>
              <td align="LEFT" valign="TOP">
                The window's title
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">event_mask</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gint</span>
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                Events to receive on this window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">x</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gint16</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_X</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">0</span>
              </td>
              <td align="LEFT" valign="TOP">
                X position relative to parent window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">y</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gint16</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_Y</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">0</span>
              </td>
              <td align="LEFT" valign="TOP">
                Y position relative to parent window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">width</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gint16</span>
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                Width of window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">height</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gint16</span>
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                Height of window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">wclass</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GdkWindowClass</span>
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_INPUT_ONLY</span> vs.
                <span class="STRUCTNAME">GDK_INPUT_OUTPUT</span>
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">visual</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GdkVisual*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_VISUAL</span>
              </td>
              <td align="LEFT" valign="TOP">
                X's "default visual"
              </td>
              <td align="LEFT" valign="TOP">
                Visual for this window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">colormap</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GdkColormap*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_COLORMAP</span>
              </td>
              <td align="LEFT" valign="TOP">
                X's "default colormap" [PD]footnote!
              </td>
              <td align="LEFT" valign="TOP">
                Colormap for this window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">window_type</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GdkWindowType</span>
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                none
              </td>
              <td align="LEFT" valign="TOP">
                Window type (see text)
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">cursor</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GdkCursor*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_CURSOR</span>
              </td>
              <td align="LEFT" valign="TOP">
                Parent window's cursor
              </td>
              <td align="LEFT" valign="TOP">
                Mouse pointer for this window
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">wmclass_name</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gchar*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_WMCLASS</span>
              </td>
              <td align="LEFT" valign="TOP">
                none (doesn't set hint)
              </td>
              <td align="LEFT" valign="TOP">
                Set the "name" part of the class hint (see text)
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">wmclass_class</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gchar*</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_WMCLASS</span>
              </td>
              <td align="LEFT" valign="TOP">
                none (doesn't set hint)
              </td>
              <td align="LEFT" valign="TOP">
                Set the "class" part of the class hint (see text)
              </td>
            </tr>
            <tr>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">override_redirect</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">gboolean</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">GDK_WA_NOREDIR</span>
              </td>
              <td align="LEFT" valign="TOP">
                <span class="STRUCTNAME">FALSE</span> [PD]
                footnote!
              </td>
              <td align="LEFT" valign="TOP">
                Make the window "override redirect" (see text)
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-gdk.html"><font color="#0000ff" size="2">
            <b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkvisual.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>GDK Basics</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Visuals and
            Colormaps</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

