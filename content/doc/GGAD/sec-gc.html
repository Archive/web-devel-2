<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Graphics Contexts
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GDK Basics" href="cha-gdk.html">
    <link rel="PREVIOUS" title="Fonts" href="sec-gdkfont.html">
    <link rel="NEXT" title="Drawing" href="z132.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkfont.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z132.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-GC">Graphics Contexts</a>
      </h1>
      <p>
        A <i class="FIRSTTERM">graphics context</i>, or GC, is
        simply a set of parameters to be used when drawing (such as
        color, clip mask, font, and so on). It is a server-side
        resource, just as pixmaps and windows are. GCs reduce the
        number of arguments to the GDK drawing functions, and also
        reduce the number of parameters passed from client to
        server with each drawing request.
      </p>
      <p>
        A graphics context can be created with a <span class= 
        "STRUCTNAME">GdkGCValues</span> struct, analagous to <span
        class="STRUCTNAME">GdkWindowAttr</span>; the struct
        contains all the interesting features of a graphics
        context, and you pass <tt class="FUNCTION">
        gdk_gc_new_with_values()</tt> flags indicating which fields
        are valid. The other fields retain their default value. You
        can also create an all-default GC with <tt class=
        "FUNCTION">gdk_gc_new()</tt> (this is usually easier).
        Functions are provided to change GC settings after the GC
        is created as well---but remember that each change requires
        a message to the X server. These functions are summarized
        in <a href="sec-gc.html#FL-GDKGC">Figure 16</a>. The
        attributes of a GC, and the flags used as the final
        argument to <tt class="FUNCTION">
        gdk_gc_new_with_values()</tt>, are summarized in <a href= 
        "sec-gc.html#TAB-GDKGC">Table 6</a>.
      </p>
      <p>
        All GCs are not interchangeable; they are tied to a
        particular depth and visual. The GC's depth and visual must
        match the depth and visual of the drawable you are drawing
        to. A GC's depth and visual are taken from the <span class= 
        "STRUCTNAME">GdkWindow*</span> argument to <tt class= 
        "FUNCTION">gdk_gc_new()</tt>, so the easiest way to handle
        this issue is to create the GC with the window you plan to
        draw on.
      </p>
      <p>
        <span class="STRUCTNAME">GdkGCValues</span> is a nice
        summary of a GC's attributes:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
typedef struct _GdkGCValues GdkGCValues;

struct _GdkGCValues
{
  GdkColor          foreground;
  GdkColor          background;
  GdkFont          *font;
  GdkFunction       function;
  GdkFill           fill;
  GdkPixmap        *tile;
  GdkPixmap        *stipple;
  GdkPixmap        *clip_mask;
  GdkSubwindowMode  subwindow_mode;
  gint              ts_x_origin;
  gint              ts_y_origin;
  gint              clip_x_origin;
  gint              clip_y_origin;
  gint              graphics_exposures;
  gint              line_width;
  GdkLineStyle      line_style;
  GdkCapStyle       cap_style;
  GdkJoinStyle      join_style;
};
</pre>
          </td>
        </tr>
      </table>
      <p>
        The <span class="STRUCTNAME">foreground</span> color is the
        "pen color" used to draw lines, circles, and other shapes.
        The purpose of the <span class="STRUCTNAME">
        background</span> color depends on the particular drawing
        operation. These colors must be allocated in the current
        colormap with <tt class="FUNCTION">gdk_color_alloc()</tt>.
      </p>
      <p>
        The <span class="STRUCTNAME">font</span> field is unused:
        in Xlib, it specifies the font to use when drawing text. In
        GDK, it used to have the same purpose; but now the GDK
        routines for drawing text all require a <span class= 
        "STRUCTNAME">GdkFont*</span> argument instead. An Xlib
        graphics context can only store plain fonts, but a <span
        class="STRUCTNAME">GdkFont</span> can also represent a
        fontset (used to render some foreign languages). GDK should
        probably store a font field in its <span class=
        "STRUCTNAME">GdkGC</span> instead of requiring a font
        argument to the text-drawing functions, but it doesn't.
      </p>
      <p>
        The <span class="STRUCTNAME">function</span> field
        specifies how each pixel being drawn is combined with the
        pixel that already exists in the drawable. There are many
        possible values, but only two are ever used:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_COPY</span> is the
            default. It ignores the existing pixel (just writes the
            new pixel over it).
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_XOR</span> combines the
            old and new pixels in an invertable way. That is, if
            you perform exactly the same <span class="STRUCTNAME">
            GDK_XOR</span> operation twice, the first draw is
            undone by the second. <span class="STRUCTNAME">
            GDK_XOR</span> is often used for "rubberbanding," since
            it makes it easy to restore the original contents of
            the drawable once rubberbanding is complete.
          </p>
        </li>
      </ul>
      <p>
        The <span class="STRUCTNAME">fill</span> field determines
        how the <span class="STRUCTNAME">tile</span> and <span
        class="STRUCTNAME">stipple</span> fields are used. A <span
        class="STRUCTNAME">tile</span> is a pixmap with the same
        depth as the destination drawable; it is copied over and
        over into the destination drawable---the origin of the
        first tile is (<span class="STRUCTNAME">ts_x_origin</span>,
        <span class="STRUCTNAME">ts_y_origin</span>). A <span
        class="STRUCTNAME">stipple</span> is a bitmap (pixmap with
        depth 1); stipples are also tiled starting at (<span class= 
        "STRUCTNAME">ts_x_origin</span>, <span class="STRUCTNAME">
        ts_y_origin</span>). Possible <span class="STRUCTNAME">
        fill</span> values are:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_SOLID</span> means to
            ignore the <span class="STRUCTNAME">tile</span> and
            <span class="STRUCTNAME">stipple</span>. Shapes are
            drawn in the foreground and background colors.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_TILED</span> means that
            shapes are drawn with the tile, instead of the
            foreground and background colors. Imagine a tiled
            surface underneath your drawable; drawing in <span
            class="STRUCTNAME">GDK_TILED</span> mode will scratch
            away the contents of the drawable, revealing the tiled
            surface underneath.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_STIPPLED</span> is like
            <span class="STRUCTNAME">GDK_SOLID</span> with a
            bitmask defined by the stipple. That is, bits not set
            in the stipple are not drawn.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_OPAQUE_STIPPLED</span>
            draws bits set in the stipple with the foreground
            color, and bits not set in the stipple with the
            background color.
          </p>
        </li>
      </ul>
      <p>
        Some X servers do not implement the more obscure function
        and fill modes very efficiently. Don't be surprised if
        using them noticeably slows down drawing.
      </p>
      <p>
        The optional <span class="STRUCTNAME">clip_mask</span> is a
        bitmap; only bits set in this bitmap will be drawn. The
        mapping from the clip mask to the drawable is determined by
        <span class="STRUCTNAME">clip_x_origin</span> and <span
        class="STRUCTNAME">clip_y_origin</span>; these define the
        drawable coordinates corresponding to (0,0) in the clip
        mask. It is also possible to set a clip rectangle (the most
        common and useful form of clipping) or a clip region (a
        region is an arbitrary area on the screen, typically a
        polygon or list of rectangles). To set a clip rectangle,
        use <tt class="FUNCTION">gdk_gc_set_clip_rectangle()</tt>:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
  GdkRectangle clip_rect;
  clip_rect.x = 10;
  clip_rect.y = 20;
  clip_rect.width = 200;
  clip_rect.height = 100;
  gdk_gc_set_clip_rectangle(gc, &amp;clip_rect);
</pre>
          </td>
        </tr>
      </table>
      <p>
        To turn off clipping, set the clip rectangle, clip region,
        or clip mask to <span class="STRUCTNAME">NULL</span>.
      </p>
      <p>
        The <span class="STRUCTNAME">subwindow_mode</span> of a GC
        only matters if the drawable is a window. The default
        setting is <span class="STRUCTNAME">
        GDK_CLIP_BY_CHILDREN</span>; this means that child windows
        are not affected by drawing on parent windows. This
        preserves the illusion that child windows are "on top" of
        parents, and child windows are opaque. <span class= 
        "STRUCTNAME">GDK_INCLUDE_INFERIORS</span> will draw right
        over the top of any child windows, overwriting any graphics
        the child windows may contain; normally this mode is not
        used. If you do use <span class="STRUCTNAME">
        GDK_INCLUDE_INFERIORS</span>, you will probably use <span
        class="STRUCTNAME">GDK_XOR</span> as your drawing function,
        since it allows you to restore the child windows' previous
        contents.
      </p>
      <p>
        <span class="STRUCTNAME">graphics_exposures</span> is a
        boolean value which defaults to <span class="STRUCTNAME">
        TRUE</span>; it determines whether <tt class="FUNCTION">
        gdk_window_copy_area()</tt> sometimes generates expose
        events. <a href="sec-gdkevent.html#SEC-EXPOSEEVENTS">the
        section called <i>Expose Events</i></a> explained this in
        more detail.
      </p>
      <p>
        The final four GC values determine how lines are drawn.
        These values are used for drawing lines, including the
        borders of unfilled polygons and arcs. The <span class= 
        "STRUCTNAME">line_width</span> field specifies the width of
        a line, in pixels. A line width of 0 specifies a "thin
        line"; thin lines are one-pixel lines that can be drawn
        very quickly (usually with hardware acceleration), but the
        exact pixels drawn depend on the X server in use. For
        consistent results, use a width of 1 instead.
      </p>
      <p>
        The <span class="STRUCTNAME">line_style</span> field can
        have one of three values:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_LINE_SOLID</span> is the
            default; a solid line.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_LINE_ON_OFF_DASH</span>
            draws a dashed line with the foreground color, leaving
            the "off" parts of the dash blank.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_LINE_DOUBLE_DASH</span>
            draws a dashed line in the foreground color, but the
            "off" parts of the dash are drawn in the background
            color.
          </p>
        </li>
      </ul>
      <p>
        Dashes are specified with <tt class="FUNCTION">
        gdk_gc_set_dashes()</tt>; <span class="STRUCTNAME">
        GdkGCValues</span> does not include a field for this. <tt
        class="FUNCTION">gdk_gc_set_dashes()</tt> accepts three
        arguments:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">dash_list</span> is an array
            of dash lengths. Even-indexed lengths are "on" dashes;
            these are drawn in the foreground color. Odd-indexed
            lengths are "off" dashes; they are not drawn or drawn
            in the background color, depending on <span class= 
            "STRUCTNAME">line_style</span>. 0 is not a permitted
            value; all lengths must be positive.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">dash_offset</span> is the
            index of the first pixel to use in the dash list. That
            is, if the dash list specifies 5 pixels "on" and 5
            "off", and the offset is 3, the line will begin in the
            middle of the "on" dash.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">n</span> is simply the number
            of elements in <span class="STRUCTNAME">
            dash_list</span>.
          </p>
        </li>
      </ul>
      <p>
        You might set a whimsical dash pattern this way, for
        example:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
  gchar dash_list[] = { 5, 5, 3, 3, 1, 1, 3, 3 };
  gdk_gc_set_dashes(gc, 0, dash_list, sizeof(dash_list));
</pre>
          </td>
        </tr>
      </table>
      <p>
        The default dash list is <span class="STRUCTNAME">{4,
        4}</span> with an offset of 0.
      </p>
      <p>
        <a href="sec-gc.html#FIG-DASHRAW">Figure 13</a> shows some
        dashed lines drawn with <span class="STRUCTNAME">
        GDK_LINE_DOUBLE_DASH</span>. The graphics context's
        foreground color is black, and its background color is a
        light gray. The first five lines are the default <span
        class="STRUCTNAME">{4, 4}</span> dash pattern with offsets
        of 0, 1, 2, 3, and 4. Remember that 0 is the default. <a
        href="sec-gc.html#FIG-DASHES-DEFAULT">Figure 14</a> shows a
        magnified view of these five lines. The last line is the
        whimsical dash pattern mentioned above; it's shown
        magnified in <a href="sec-gc.html#FIG-DASHES-WHIMSICAL">
        Figure 15</a>.
      </p>
      <div class="FIGURE">
        <a name="FIG-DASHRAW"></a>
        <p>
          <img src="figures/dashes-raw.png">
        </p>
        <p>
          <b>Figure 13. Five dashed lines, with <span class= 
          "STRUCTNAME">GDK_LINE_DOUBLE_DASH</span></b>
        </p>
      </div>
      <div class="FIGURE">
        <a name="FIG-DASHES-DEFAULT"></a>
        <p>
          <img src="figures/dashes-default.png">
        </p>
        <p>
          <b>Figure 14. Default dash pattern, with varied
          offsets</b>
        </p>
      </div>
      <div class="FIGURE">
        <a name="FIG-DASHES-WHIMSICAL"></a>
        <p>
          <img src="figures/dashes-whimsical.png">
        </p>
        <p>
          <b>Figure 15. A complex dash pattern</b>
        </p>
      </div>
      <p>
        <span class="STRUCTNAME">cap_style</span> determines how X
        draws line endpoints (or dash endpoints, if a line is
        dashed). It has four possible values:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_CAP_BUTT</span> is the
            default; it means that lines have square ends (as you
            might expect). 
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_CAP_NOT_LAST</span>
            specifies that the last pixel is skipped for one-pixel
            lines. It is otherwise the same as <span class= 
            "STRUCTNAME">GDK_CAP_BUTT</span>.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_CAP_ROUND</span> draws a
            small arc on the end of the line, extending beyond the
            line's endpoint. The center of the arc is on the
            endpoint, and the radius of the arc is one-half the
            width of the line. For one-pixel lines, it has no
            effect (since there is no way to draw a one-pixel-wide
            arc).
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_CAP_PROJECTING</span>
            extends the line past its endpoint by one-half its
            width. It has no effect on one-pixel lines.
          </p>
        </li>
      </ul>
      <p>
        The <span class="STRUCTNAME">join_style</span> parameter
        affects how lines are connected to one another, when
        drawing a polygon or drawing multiple lines in one function
        call. If you think of lines as long, thin rectangles, it is
        clear that they do not connect smoothly; there is a "notch"
        where the two endpoints come together. The three join
        styles fill in this notch:
      </p>
      <ul>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_JOIN_MITER</span> is the
            default; it draws a sharp angle where the lines
            intersect.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_JOIN_ROUND</span> creates
            rounded corners by drawing an arc in the notch.
          </p>
        </li>
        <li>
          <p>
            <span class="STRUCTNAME">GDK_JOIN_BEVEL</span> creates
            a flat corner, filling the notch with the smallest
            possible shape.
          </p>
        </li>
      </ul>
      <div class="FIGURE">
        <a name="FL-GDKGC"></a>
        <div class="FUNCSYNOPSIS">
          <a name="FL-GDKGC.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gdk/gdk.h&gt;
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF">GdkGC* <tt class=
            "FUNCTION">gdk_gc_new</tt></code>(GdkWindow* <tt class= 
            "PARAMETER"><i>window</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">GdkGC* <tt class=
            "FUNCTION">
            gdk_gc_new_with_values</tt></code>(GdkWindow* <tt
            class="PARAMETER"><i>window</i></tt>, GdkGCValues* <tt
            class="PARAMETER"><i>values</i></tt>, GdkGCValuesMask
            <tt class="PARAMETER"><i>values_mask</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gdk_gc_set_dashes</tt></code>(GdkGC* <tt class= 
            "PARAMETER"><i>gc</i></tt>, gint <tt class="PARAMETER">
            <i>dash_offset</i></tt>, gchar <tt class="PARAMETER">
            <i>dash_list</i></tt>, gint <tt class="PARAMETER"><i>
            n</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gdk_gc_unref</tt></code>(GdkGC* <tt class="PARAMETER">
            <i>gc</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 16. <span class="STRUCTNAME">GdkGC</span></b>
        </p>
      </div>
      <div class="TABLE">
        <a name="TAB-GDKGC"></a>
        <p>
          <b>Table 6. GC Attributes</b>
        </p>
        <table border="1" bgcolor="#E0E0E0" cellspacing="0"
        cellpadding="4" class="CALSTABLE">
          <tr>
            <th align="LEFT" valign="TOP">
              Attribute
            </th>
            <th align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GdkGCValuesMask</span>
            </th>
            <th align="LEFT" valign="TOP">
              Modifying Function
            </th>
            <th align="LEFT" valign="TOP">
              Default Value
            </th>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkColor foreground
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_FOREGROUND</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_foreground()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              black
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkColor background
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_BACKGROUND</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_background()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              white
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkFont *font
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_FONT</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_font</tt>
            </td>
            <td align="LEFT" valign="TOP">
              depends on X server
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkFunction function
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_FUNCTION</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_function()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_COPY</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkFill fill
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_FILL</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_fill()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              GDK_SOLID
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkPixmap *tile
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_TILE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_tile()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              pixmap filled with foreground color (i.e. effectively
              none)
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkPixmap *stipple
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_STIPPLE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_stipple()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              all-bits-on bitmap (i.e. effectively none)
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkPixmap *clip_mask
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_CLIP_MASK</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_clip_mask()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              none
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkSubwindowMode subwindow_mode
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_SUBWINDOW</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_subwindow()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              GDK_CLIP_BY_CHILDREN
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint ts_x_origin
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_TS_X_ORIGIN</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_ts_origin()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint ts_y_origin
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_TS_Y_ORIGIN</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_ts_origin()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint clip_x_origin
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_CLIP_X_ORIGIN</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_clip_origin()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint clip_y_origin
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_CLIP_Y_ORIGIN</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_clip_origin()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint graphics_exposures
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_EXPOSURES</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_exposures()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">TRUE</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint line_width
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_LINE_WIDTH</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">
              gdk_gc_set_line_attributes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkLineStyle line_style
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_LINE_STYLE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">
              gdk_gc_set_line_attributes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_LINE_SOLID</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkCapStyle cap_style
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_CAP_STYLE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">
              gdk_gc_set_line_attributes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_CAP_BUTT</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              GdkJoinStyle join_style
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_GC_JOIN_STYLE</span>
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">
              gdk_gc_set_line_attributes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">GDK_JOIN_MITER</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gchar dash_list[]
            </td>
            <td align="LEFT" valign="TOP">
              none
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_dashes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">{4, 4}</span>
            </td>
          </tr>
          <tr>
            <td align="LEFT" valign="TOP">
              gint dash_offset
            </td>
            <td align="LEFT" valign="TOP">
              none
            </td>
            <td align="LEFT" valign="TOP">
              <tt class="FUNCTION">gdk_gc_set_dashes()</tt>
            </td>
            <td align="LEFT" valign="TOP">
              <span class="STRUCTNAME">0</span>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkfont.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z132.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Fonts</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Drawing</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

