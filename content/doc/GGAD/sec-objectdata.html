<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Attaching Data to Objects
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="The GTK+ Object and Type System" href= 
    "cha-objects.html">
    <link rel="PREVIOUS" title="Object Finalization" href= 
    "sec-finalization.html">
    <link rel="NEXT" title="GDK Basics" href="cha-gdk.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-finalization.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="cha-gdk.html"><font color="#0000ff" size="2">
            <b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-OBJECTDATA">Attaching Data to Objects</a>
      </h1>
      <p>
        You can "attach" arbitrary string-pointer pairs to a <span
        class="STRUCTNAME">GtkObject</span> instance, in effect
        adding a new data member. GTK+ uses this some internally,
        but it can also be a convenient way to pass data around in
        your application. In particular, it's a nice way to pass
        information to callbacks.
      </p>
      <p>
        Here's a simple example:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
    GtkWidget* button = gtk_button_new();
    GtkWidget* label  = gtk_label_new(_("Foo"));
  
    gtk_object_set_data(GTK_OBJECT(button), "my_label_key", label); 
  
</pre>
          </td>
        </tr>
      </table>
      <p>
        Later, when you have a pointer to the button but not the
        label (perhaps in a callback connected to the button's
        <span class="SYMBOL">"clicked"</span> signal), you can do
        this:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
    GtkWidget* label = gtk_object_get_data(GTK_OBJECT(button),
                                           "my_label_key");

    /* If no data is found for the key, NULL is returned. */

    if (label == NULL)
      {
        g_warning("No data was associated with 'my_label_key'!");
      }
  
</pre>
          </td>
        </tr>
      </table>
      <p>
        A pair of convenience functions use a predetermined key and
        thus save typing (and remembering) the object data key.
        These are <tt class="FUNCTION">
        gtk_object_set_user_data()</tt> and <tt class="FUNCTION">
        gtk_object_get_user_data()</tt>. You can also register a
        function to free the data when the data is removed or
        replaced, or the <span class="STRUCTNAME">GtkObject</span>
        is destroyed; This function should be of type <span class= 
        "STRUCTNAME">GtkDestroyNotify</span>:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
  typedef void (*GtkDestroyNotify) (gpointer data);
</pre>
          </td>
        </tr>
      </table>
      <p>
        Conveniently, <tt class="FUNCTION">g_free()</tt> and <tt
        class="FUNCTION">gtk_object_unref()</tt> will work here.
        You register a "destroy notification" function when you set
        the data, using <tt class="FUNCTION">
        gtk_object_set_data_full()</tt>. You can remove data before
        the object is destroyed with <tt class="FUNCTION">
        gtk_object_remove_data()</tt>, or remove it without calling
        the destroy function with <tt class="FUNCTION">
        gtk_object_remove_no_notify()</tt>. Setting the data to
        <span class="STRUCTNAME">NULL</span> is equivalent to
        removing it with <tt class="FUNCTION">
        gtk_object_remove_data()</tt>, and will also call the
        destroy function if you registered one. <a href= 
        "sec-objectdata.html#FL-OBJECTDATA">Figure 6</a> summarizes
        the object data functions.
      </p>
      <p>
        It's worth pointing out that the object data system is a
        thin wrapper around the <span class="STRUCTNAME">
        GData</span> facility in glib, which can be used
        standalone.
      </p>
      <div class="FIGURE">
        <a name="FL-OBJECTDATA"></a>
        <div class="FUNCSYNOPSIS">
          <a name="FL-OBJECTDATA.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gtk/gtkobject.h&gt;
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gtk_object_set_data</tt></code>(GtkObject* <tt class= 
            "PARAMETER"><i>object</i></tt>, const gchar* <tt class= 
            "PARAMETER"><i>key</i></tt>, gpointer <tt class= 
            "PARAMETER"><i>data</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gtk_object_set_data_full</tt></code>(GtkObject* <tt
            class="PARAMETER"><i>object</i></tt>, const gchar* <tt
            class="PARAMETER"><i>key</i></tt>, gpointer <tt class= 
            "PARAMETER"><i>data</i></tt>, GtkDestroyNotify <tt
            class="PARAMETER"><i>destroy</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gtk_object_remove_data</tt></code>(GtkObject* <tt
            class="PARAMETER"><i>object</i></tt>, const gchar* <tt
            class="PARAMETER"><i>key</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">gpointer <tt class=
            "FUNCTION">gtk_object_get_data</tt></code>(GtkObject*
            <tt class="PARAMETER"><i>object</i></tt>, const gchar*
            <tt class="PARAMETER"><i>key</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gtk_object_remove_no_notify</tt></code>(GtkObject* <tt
            class="PARAMETER"><i>object</i></tt>, const gchar* <tt
            class="PARAMETER"><i>key</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gtk_object_set_user_data</tt></code>(GtkObject* <tt
            class="PARAMETER"><i>object</i></tt>, gpointer <tt
            class="PARAMETER"><i>data</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">gpointer <tt class=
            "FUNCTION">
            gtk_object_get_user_data</tt></code>(GtkObject* <tt
            class="PARAMETER"><i>object</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 6. Attaching key-value pairs to a <span class= 
          "STRUCTNAME">GtkObject</span></b>
        </p>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-finalization.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="cha-gdk.html"><font color="#0000ff" size="2">
            <b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Object
            Finalization</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>GDK Basics</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

