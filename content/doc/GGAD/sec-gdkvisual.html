<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Visuals and Colormaps
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GDK Basics" href="cha-gdk.html">
    <link rel="PREVIOUS" title="GdkWindow" href=
    "sec-gdkwindow.html">
    <link rel="NEXT" title="Drawables and Pixmaps" href= 
    "sec-gdkdrawable.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkwindow.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkdrawable.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-GDKVISUAL">Visuals and Colormaps</a>
      </h1>
      <p>
        Unfortunately, not all hardware is created equal. The most
        primitive X servers support only two colors; each pixel is
        either on or off. This is referred to as a "one bit per
        pixel (bpp)" display. A display with one bit per pixel is
        said to have a <i class="FIRSTTERM">depth</i> of one. More
        advanced X servers support 24 or 32 bits per pixel, and
        allow you to specify a different depth on a
        window-by-window basis. 24 bits per pixel allows 2^24
        different pixels, which includes more colors than the human
        eye can differentiate.
      </p>
      <p>
        Conceptually, a bitmap display consists of a rectangular
        grid of pixels. Each pixel consists of some fixed number of
        bits; pixels are mapped to visible colors in a
        hardware-dependent way. One way to think about this is to
        imagine a two-dimensional array of integers, where the
        integer size is chosen to hold the required number of bits.
        Alternatively, you can think of a display like this as a
        stack of <i class="FIRSTTERM">bit planes</i>, or
        two-dimensional arrays of bits. If all the planes are
        parallel to one another, a pixel is a perpendicular line
        passing through the same coordinates on each plane, taking
        a single bit from each one. This is the origin of the term
        <i class="FIRSTTERM">depth</i>, since the number of bits
        per pixel is equal to the depth of the stack of bit planes.
      </p>
      <p>
        In the X Window System, pixels represent entries in a color
        lookup table. A <i class="FIRSTTERM">color</i> is a red,
        green, blue (RGB) value---monitors mix red, green, and blue
        light in some ratio to display each pixel. Take an eight
        bit display, for example: eight bits are not enough to
        encode a color in-place; only a few arbitrary RGB values
        would be possible. Instead, the bits are interpreted as an
        integer and used to index an array of RGB color values.
        This table of colors is called the <i class="FIRSTTERM">
        colormap</i>; it can sometimes be modified to contain the
        colors you plan to use, though this is
        hardware-dependent---some colormaps are read-only.
      </p>
      <p>
        A <i class="FIRSTTERM">visual</i> is required to determine
        how a pixel's bit pattern is converted into a visible
        color. Thus, a visual also defines how colormaps work. On
        an 8-bit display, the X server might interpret each pixel
        as an index into a single colormap containing the 256
        possible colors. 24-bit visuals typically have three
        colormaps: one for shades of red, one for shades of green,
        and one for shades of blue. Each colormap is indexed with
        an eight-bit value; the three eight-bit values are packed
        into a 24-bit pixel. The visual defines the meaning of the
        pixel contents. Visuals also define whether the colormap is
        read-only or modifiable.
      </p>
      <p>
        In short, a visual is a description of the color
        capabilities of a particular X server. In Xlib, you have to
        do a lot of fooling around with visuals; GDK and GTK+
        shield you from most of the mess.
      </p>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z114"><span class="STRUCTNAME">
          GdkVisual</span></a>
        </h2>
        <p>
          Xlib can report a list of all available visuals and
          information about each; GDK keeps a client-side copy of
          this information in a struct called <span class= 
          "STRUCTNAME">GdkVisual</span>. GDK can report the
          available visuals, and rank them in different ways. Most
          of the time you will only use <tt class="FUNCTION">
          gdk_visual_get_system()</tt>, which returns a pointer to
          the default visual (<a href= 
          "sec-gdkvisual.html#FL-GDKVISUAL">Figure 2</a>). (If
          you're writing a <tt class="CLASSNAME">GtkWidget</tt>,
          <tt class="FUNCTION">gtk_widget_get_visual()</tt> returns
          the visual you should use; more on this in <a href= 
          "cha-widget.html">the chapter called <i>Writing a <tt
          class="CLASSNAME">GtkWidget</tt></i></a>.) The returned
          visual is not a copy, so there is no need to free it; GDK
          keeps visuals around permanently.
        </p>
        <div class="FIGURE">
          <a name="FL-GDKVISUAL"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-GDKVISUAL.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gdk/gdk.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">GdkVisual* <tt class= 
              "FUNCTION">
              gdk_visual_get_system</tt></code>(void);</code>
            </p>
          </div>
          <p>
            <b>Figure 2. Default Visual</b>
          </p>
        </div>
        <p>
          For reference, here are the contents of <span class= 
          "STRUCTNAME">GdkVisual</span>; most of the members are
          used to calculate pixel values from colors. Since this is
          fairly involved and rarely used, this book glosses over
          the topic. The <span class="STRUCTNAME">depth</span>
          member is convenient sometimes. <a href= 
          "sec-gdkvisual.html#SEC-TYPESOFVISUAL">the section called
          <i>Types of Visual</i></a> has more to say about the
          <span class="STRUCTNAME">type</span> member.
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GdkVisual GdkVisual;

struct _GdkVisual
{
  GdkVisualType type;
  gint depth;
  GdkByteOrder byte_order;
  gint colormap_size;
  gint bits_per_rgb;

  guint32 red_mask;
  gint red_shift;
  gint red_prec;

  guint32 green_mask;
  gint green_shift;
  gint green_prec;

  guint32 blue_mask;
  gint blue_shift;
  gint blue_prec;
};
</pre>
            </td>
          </tr>
        </table>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-TYPESOFVISUAL">Types of Visual</a>
          </h3>
          <p>
            Visuals differ along several dimensions. They can be
            grayscale or RGB, colormaps can be modifiable or fixed,
            and the pixel value can either index a single colormap
            or contain packed red, green, and blue indexes. Here
            are the possible values for <span class="STRUCTNAME">
            GdkVisualType</span>:
          </p>
          <ul>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_STATIC_GRAY</span> means the display is
                either monochrome or gray scale, and the colormap
                cannot be modified. A pixel value is simply a level
                of gray; each pixel is "hard coded" to represent a
                certain on-screen color.
              </p>
            </li>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_GRAYSCALE</span> means the display has a
                modifiable colormap, but only levels of gray are
                possible. The pixel represents an entry in the
                colormap, so a given pixel can represent a
                different level of gray at different times.
              </p>
            </li>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_STATIC_COLOR</span> represents a color
                display which uses a single read-only colormap
                rather than a separate colormap for each of red,
                green, and blue. The display is almost certainly
                12-bit or less (a 24-bit display using a single
                colormap would need a colormap with 2^24 entries,
                occupying close to half a gigabyte---not very
                practical!). This is an annoying visual, because
                relatively few colors are available and you can't
                change which colors they are.
              </p>
            </li>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_PSEUDO_COLOR</span> is the most common
                visual on low-end PC hardware from several years
                ago. If you have a one-megabyte 256-color video
                card, this is most likely your X server's visual.
                It represents a color display with a read/write
                colormap. Pixels index a single colormap.
              </p>
            </li>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_TRUE_COLOR</span> is a color display
                with three read-only colormaps, one for each of
                red, green, and blue. A pixel contains three
                indexes, one per colormap. There is a fixed
                mathematical relationship between pixels and RGB
                triplets; you can get a pixel from red, green, and
                blue values in [0, 255] using the formula: <span
                class="STRUCTNAME">gulong pixel =
                (gulong)(red*65536 + green*256 + blue)</span>.
                
              </p>
            </li>
            <li>
              <p>
                <span class="STRUCTNAME">
                GDK_VISUAL_DIRECT_COLOR</span> is a color display
                with three read-write colormaps. If you use the GDK
                color handling routines, they simply fill up all
                three colormaps to emulate a true color display,
                then pretend the direct color display is true
                color.
              </p>
            </li>
          </ul>
        </div>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z115">Color and <span class="STRUCTNAME">
          GdkColormap</span></a>
        </h2>
        <p>
          A <span class="STRUCTNAME">GdkColor</span> stores an RGB
          value and a pixel. Red, green, and blue are given as
          16-bit unsigned integers; so they are in the range [0,
          65535]. The contents of the pixel depend on the visual.
          Here is <span class="STRUCTNAME">GdkColor</span>:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GdkColor GdkColor;

struct _GdkColor
{
  gulong  pixel;
  gushort red;
  gushort green;
  gushort blue;
};
</pre>
            </td>
          </tr>
        </table>
        <p>
          Before you can use a color to draw, you must:
        </p>
        <ul>
          <li>
            <p>
              Ensure that the pixel value contains an appropriate
              value.
            </p>
          </li>
          <li>
            <p>
              Ensure that the color exists in the colormap of the
              drawable you intend to draw to. (A <i class=
              "FIRSTTERM">drawable</i> is a window or pixmap you
              can draw to; see <a href="sec-gdkdrawable.html">the
              section called <i>Drawables and
              Pixmaps</i></a>.)
            </p>
          </li>
        </ul>
        <p>
          In Xlib, this is an enormously complicated process,
          because it has to be done differently for every kind of
          visual. GDK conceals things fairly well. You simply call
          <tt class="FUNCTION">gdk_colormap_alloc_color()</tt> to
          fill in the pixel value and add the color to the colormap
          (<a href="sec-gdkvisual.html#FL-COLORALLOC">Figure
          3</a>). Here is an example; it assumes a preexisting
          <span class="STRUCTNAME">GdkColormap* colormap</span>,
          which should be the colormap of the drawable you are
          targetting:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
  GdkColor color;
  
  /* Describe a pure red */
  color.red   = 65535;
  color.green = 0;
  color.blue  = 0;

  if (gdk_colormap_alloc_color(colormap, &amp;color, FALSE, TRUE))
    {
      /* Success! */
    }
</pre>
            </td>
          </tr>
        </table>
        <p>
          If <tt class="FUNCTION">gdk_colormap_alloc_color()</tt>
          returns <span class="STRUCTNAME">TRUE</span>, then the
          color was allocated and <span class="STRUCTNAME">
          color.pixel</span> contains a valid value. The color can
          then be used to draw. The two boolean arguments to <tt
          class="FUNCTION">gdk_colormap_alloc_color()</tt> specify
          whether the color should be <i class="FIRSTTERM">
          writeable</i>, and whether to try to find a "best match"
          if the color can't be allocated. If a best match is used
          instead of allocating a new color, the color's RGB values
          will be changed to the best match. If you request a best
          match for a non-writeable entry, allocation really should
          not fail, since even on a black and white display either
          black or white will be the best match; only an empty
          colormap could cause failure. The only way to get an
          empty colormap is to create a custom colormap yourself.
          If you don't ask for the best match, failure is quite
          possible on displays with a limited number of colors.
          Failure is always possible with writeable colormap
          entries (where best match makes no sense, because the
          entry can be modified).
        </p>
        <p>
          A <i class="FIRSTTERM">writeable</i> colormap entry is
          one that you can change at any time; some visuals support
          this, and some don't. The purpose of a writeable colormap
          entry is to change an on-screen color without redrawing
          the graphics. Some hardware stores pixels as indices into
          a color lookup table, so changing the lookup table
          changes how the pixels are displayed. The disadvantages
          of writeable colormap entries are numerous. Most notably:
          not all visuals support them, and writeable colormap
          entries can't be used by other applications (read-only
          entries can be shared, since other applications know the
          color will remain constant). Thus, it is a good idea to
          avoid allocating writeable colors. On modern hardware,
          they are more trouble than they're worth; the speed gain
          compared to simply redrawing your graphics will not be
          noticeable.
        </p>
        <p>
          When you're finished with a color, you can remove it from
          the colormap with <tt class="FUNCTION">
          gdk_colormap_free_colors()</tt>. This is only really
          important for pseudo color and grayscale visuals, where
          colors are in short supply and the colormap can be
          modified by clients. GDK will automatically do the right
          thing for each visual type, so always call this function.
        </p>
        <p>
          A convenient way to obtain RGB values is the <tt class= 
          "FUNCTION">gdk_color_parse()</tt> function. This takes an
          X color specification, and fills in the <span class= 
          "STRUCTNAME">red</span>, <span class="STRUCTNAME">
          green</span>, and <span class="STRUCTNAME">blue</span>
          fields of a <span class="STRUCTNAME">GdkColor</span>. An
          X color specification can have many forms; one
          possibility is an RGB string:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
 RGB:FF/FF/FF
</pre>
            </td>
          </tr>
        </table>
        <p>
          This specifies white (red, green, and blue are all at
          full intensity). The <span class="STRUCTNAME">RGB:</span>
          specifies a "color space," and determines the meaning of
          the numbers after it. X also understands several more
          obscure color spaces. If the color specification string
          doesn't begin with a recognized color space, X assumes
          it's a color name and looks it up in a database of names.
          So you can write code like this:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
  GdkColor color;

  if (gdk_color_parse("orange", &amp;color))
    {
      if (gdk_colormap_alloc_color(colormap, &amp;color, FALSE, TRUE))
        {
          /* We have orange! */
        }
    } 
</pre>
            </td>
          </tr>
        </table>
        <p>
          As you can see, <tt class="FUNCTION">
          gdk_color_parse()</tt> returns <span class="STRUCTNAME">
          TRUE</span> if it figures out the string you pass it.
          There is no way to know exactly what will be in the color
          database, so always check this return value.
        </p>
        <div class="FIGURE">
          <a name="FL-COLORALLOC"></a>
          <div class="FUNCSYNOPSIS">
            <a name="FL-COLORALLOC.SYNOPSIS"></a>
            <table border="0" bgcolor="#E0E0E0" width="100%">
              <tr>
                <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gdk/gdk.h&gt;
</pre>
                </td>
              </tr>
            </table>
            <p>
              <code><code class="FUNCDEF">gboolean <tt class= 
              "FUNCTION">
              gdk_colormap_alloc_color</tt></code>(GdkColormap* <tt
              class="PARAMETER"><i>colormap</i></tt>, GdkColor* <tt
              class="PARAMETER"><i>color</i></tt>, gboolean <tt
              class="PARAMETER"><i>writeable</i></tt>, gboolean <tt
              class="PARAMETER"><i>best_match</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">void <tt class=
              "FUNCTION">
              gdk_colormap_free_colors</tt></code>(GdkColormap* <tt
              class="PARAMETER"><i>colormap</i></tt>, GdkColor* <tt
              class="PARAMETER"><i>colors</i></tt>, gint <tt class= 
              "PARAMETER"><i>ncolors</i></tt>);</code>
            </p>
            <p>
              <code><code class="FUNCDEF">gint <tt class=
              "FUNCTION">gdk_color_parse</tt></code>(gchar* <tt
              class="PARAMETER"><i>spec</i></tt>, GdkColor* <tt
              class="PARAMETER"><i>color</i></tt>);</code>
            </p>
          </div>
          <p>
            <b>Figure 3. Color Allocation</b>
          </p>
        </div>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="Z116">Obtaining a Colormap</a>
          </h3>
          <p>
            If you're writing a <tt class="CLASSNAME">
            GtkWidget</tt> subclass, the correct way to obtain a
            colormap is with <tt class="FUNCTION">
            gtk_widget_get_colormap()</tt> (see <a href= 
            "cha-widget.html">the chapter called <i>Writing a <tt
            class="CLASSNAME">GtkWidget</tt></i></a>). Otherwise,
            the system (default) colormap is usually what you want;
            call <tt class="FUNCTION">
            gdk_colormap_get_system()</tt>, which takes no
            arguments and returns the default colormap.
          </p>
          <p>
            The GdkRGB module (see <a href="z132.html#SEC-GDKRGB">
            the section called <i>RGB Buffers</i></a>) is another
            way to deal with colors; among other capabilities, it
            can set the foreground and background colors of a
            graphics context from an RGB value. The relevant
            functions are <tt class="FUNCTION">
            gdk_rgb_gc_set_foreground()</tt> and <tt class=
            "FUNCTION">gdk_rgb_gc_set_background()</tt>. GdkRGB has
            a pre-allocated colormap that it uses to pick a
            best-match color; using it means that your application
            can share limited colormap resources with other
            applications using GdkRGB (such as the Gimp). You can
            also obtain GdkRGB's colormap and use it directly (see
            <a href="z132.html#SEC-GDKRGB">the section called <i>
            RGB Buffers</i></a>).
          </p>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkwindow.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkdrawable.html"><font color="#0000ff"
            size="2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b><span class= 
            "STRUCTNAME">GdkWindow</span></b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Drawables and
            Pixmaps</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

