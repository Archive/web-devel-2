<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      The GtkWidget Base Class
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="Writing a GtkWidget" href=
    "cha-widget.html">
    <link rel="PREVIOUS" title="Writing a GtkWidget" href= 
    "cha-widget.html">
    <link rel="NEXT" title="An Example: The GtkEv Widget" href= 
    "z147.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-widget.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z147.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="Z144">The <tt class="CLASSNAME">GtkWidget</tt>
        Base Class</a>
      </h1>
      <p>
        Obviously, to subclass <tt class="CLASSNAME">
        GtkWidget</tt>, you will have to be familiar with the base
        class. This section offers a brief tour of the <tt class= 
        "CLASSNAME">GtkWidget</tt> class and instance structs, and
        some important GTK+ routines that aren't very common in
        everyday programming.
      </p>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z145">The <tt class="CLASSNAME">GtkWidget</tt>
          Instance Struct</a>
        </h2>
        <p>
          A <tt class="CLASSNAME">GtkWidget</tt> instance looks
          like this:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GtkWidget GtkWidget;

struct _GtkWidget
{
  GtkObject object;

  guint16 private_flags;
  
  guint8 state;

  guint8 saved_state;
  
  gchar *name;

  GtkStyle *style;
  
  GtkRequisition requisition;

  GtkAllocation allocation;
  
  GdkWindow *window;

  GtkWidget *parent;
};
      
</pre>
            </td>
          </tr>
        </table>
        <p>
          The <span class="STRUCTNAME">private_flags</span>, <span
          class="STRUCTNAME">state</span>, and <span class= 
          "STRUCTNAME">saved_state</span> fields should all be
          accessed with macros, if at all. Some of these macros
          will come up as we discuss widget implementations. The
          <span class="STRUCTNAME">state</span> field stores the
          widget's state as described in <a href= 
          "z57.html#SEC-WIDGETSTATES">the section called <i>Widget
          States</i> in the chapter called <i>GTK+ Basics</i></a>.
          <span class="STRUCTNAME">saved_state</span> is used to
          save the widget's previous state when the current state
          is <span class="STRUCTNAME">GTK_STATE_INSENSITIVE</span>;
          when the widget is re-sensitized, its original state is
          restored. As <a href="z57.html#SEC-WIDGETSTATES">the
          section called <i>Widget States</i> in the chapter called
          <i>GTK+ Basics</i></a> explains, the current state can be
          accessed with the <tt class="FUNCTION">
          GTK_WIDGET_STATE()</tt> macro.
        </p>
        <p>
          The <span class="STRUCTNAME">name</span> of a widget is
          used in a <tt class="FILENAME">gtkrc</tt> file to group
          widgets together for customization purposes. By default,
          the name of a widget is the type name registered with the
          object system (in GTK+, this type name is always the name
          of the instance struct, such as <span class="STRUCTNAME">
          "GtkLabel"</span>). Particular widgets can be given a
          different name with <tt class="FUNCTION">
          gtk_widget_set_name()</tt>; for example, if you want a
          particular label to appear in a different font, you can
          give it a name like "FunkyFontLabel" and then specify a
          different font for that name in a <tt class="FILENAME">
          gtkrc</tt> shipped with your application.
        </p>
        <p>
          The <span class="STRUCTNAME">requisition</span> and <span
          class="STRUCTNAME">allocation</span> fields store the
          last requested and allocated size of the widget,
          respectively. <a href=
          "sec-widgetindetail.html#SEC-SIZENEG">the section called
          <i>Size Negotiation</i></a> will have more to say about
          this.
        </p>
        <p>
          The <span class="STRUCTNAME">window</span> field stores
          the widget's <span class="STRUCTNAME">GdkWindow</span>,
          or the widget's parent's <span class="STRUCTNAME">
          GdkWindow</span> if the widget has none. The <span class= 
          "STRUCTNAME">parent</span> field is a pointer to the
          widget's parent container; it will be <span class= 
          "STRUCTNAME">NULL</span> if the widget is not inside a
          container.
        </p>
      </div>
      <div class="SECT2">
        <h2 class="SECT2">
          <a name="Z146">The <tt class="CLASSNAME">GtkWidget</tt>
          Class Struct</a>
        </h2>
        <p>
          There are a truly huge number of class functions in <span
          class="STRUCTNAME">GtkWidgetClass</span>. Thankfully, in
          most cases you only have to override a few of them. Here
          is the code:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GtkWidgetClass GtkWidgetClass;

struct _GtkWidgetClass
{
  GtkObjectClass parent_class;
  
  guint activate_signal;

  guint set_scroll_adjustments_signal;
  
  /* Basics */
  void (* show)                (GtkWidget      *widget);
  void (* show_all)            (GtkWidget      *widget);
  void (* hide)                (GtkWidget      *widget);
  void (* hide_all)            (GtkWidget      *widget);
  void (* map)                 (GtkWidget      *widget);
  void (* unmap)               (GtkWidget      *widget);
  void (* realize)             (GtkWidget      *widget);
  void (* unrealize)           (GtkWidget      *widget);
  void (* draw)                (GtkWidget      *widget,
                                GdkRectangle   *area);
  void (* draw_focus)          (GtkWidget      *widget);
  void (* draw_default)        (GtkWidget      *widget);
  void (* size_request)        (GtkWidget      *widget,
                                GtkRequisition *requisition);
  void (* size_allocate)       (GtkWidget      *widget,
                                GtkAllocation  *allocation);
  void (* state_changed)       (GtkWidget      *widget,
                                GtkStateType    previous_state);
  void (* parent_set)          (GtkWidget      *widget,
                                GtkWidget      *previous_parent);
  void (* style_set)           (GtkWidget      *widget,
                                GtkStyle       *previous_style);
  
  /* Accelerators */
  gint (* add_accelerator)     (GtkWidget      *widget,
                                guint           accel_signal_id,
                                GtkAccelGroup  *accel_group,
                                guint           accel_key,
                                GdkModifierType accel_mods,
                                GtkAccelFlags   accel_flags);
  void (* remove_accelerator)  (GtkWidget      *widget,
                                GtkAccelGroup  *accel_group,
                                guint           accel_key,
                                GdkModifierType accel_mods);

  /* Explicit focus */
  void (* grab_focus)          (GtkWidget      *widget);
  
  /* Events */
  gint (* event)                   (GtkWidget          *widget,
                                    GdkEvent           *event);
  gint (* button_press_event)      (GtkWidget          *widget,
                                    GdkEventButton     *event);
  gint (* button_release_event)    (GtkWidget          *widget,
                                    GdkEventButton     *event);
  gint (* motion_notify_event)     (GtkWidget          *widget,
                                    GdkEventMotion     *event);
  gint (* delete_event)            (GtkWidget          *widget,
                                    GdkEventAny        *event);
  gint (* destroy_event)           (GtkWidget          *widget,
                                    GdkEventAny        *event);
  gint (* expose_event)            (GtkWidget          *widget,
                                    GdkEventExpose     *event);
  gint (* key_press_event)         (GtkWidget          *widget,
                                    GdkEventKey        *event);
  gint (* key_release_event)       (GtkWidget          *widget,
                                    GdkEventKey        *event);
  gint (* enter_notify_event)      (GtkWidget          *widget,
                                    GdkEventCrossing   *event);
  gint (* leave_notify_event)      (GtkWidget          *widget,
                                    GdkEventCrossing   *event);
  gint (* configure_event)         (GtkWidget          *widget,
                                    GdkEventConfigure  *event);
  gint (* focus_in_event)          (GtkWidget          *widget,
                                    GdkEventFocus      *event);
  gint (* focus_out_event)         (GtkWidget          *widget,
                                    GdkEventFocus      *event);
  gint (* map_event)               (GtkWidget          *widget,
                                    GdkEventAny        *event);
  gint (* unmap_event)             (GtkWidget          *widget,
                                    GdkEventAny        *event);
  gint (* property_notify_event)   (GtkWidget          *widget,
                                    GdkEventProperty   *event);
  gint (* selection_clear_event)   (GtkWidget          *widget,
                                    GdkEventSelection  *event);
  gint (* selection_request_event) (GtkWidget          *widget,
                                    GdkEventSelection  *event);
  gint (* selection_notify_event)  (GtkWidget          *widget,
                                    GdkEventSelection  *event);
  gint (* proximity_in_event)      (GtkWidget          *widget,
                                    GdkEventProximity  *event);
  gint (* proximity_out_event)     (GtkWidget          *widget,
                                    GdkEventProximity  *event);
  gint (* visibility_notify_event)  (GtkWidget         *widget,
                                     GdkEventVisibility *event);
  gint (* client_event)            (GtkWidget          *widget,
                                    GdkEventClient     *event);
  gint (* no_expose_event)         (GtkWidget          *widget,
                                    GdkEventAny        *event);

  /* Selection */
  void (* selection_get)           (GtkWidget          *widget,
                                    GtkSelectionData   *selection_data,
                                    guint               info,
                                    guint               time);
  void (* selection_received)      (GtkWidget          *widget,
                                    GtkSelectionData   *selection_data,
                                    guint               time);

  /* Source side drag signals */
  void (* drag_begin)              (GtkWidget          *widget,
                                    GdkDragContext     *context);
  void (* drag_end)                (GtkWidget          *widget,
                                    GdkDragContext     *context);
  void (* drag_data_get)           (GtkWidget          *widget,
                                    GdkDragContext     *context,
                                    GtkSelectionData   *selection_data,
                                    guint               info,
                                    guint               time);
  void (* drag_data_delete)        (GtkWidget          *widget,
                                    GdkDragContext     *context);

  /* Target side drag signals */
  void (* drag_leave)              (GtkWidget          *widget,
                                    GdkDragContext     *context,
                                    guint               time);
  gboolean (* drag_motion)         (GtkWidget          *widget,
                                    GdkDragContext     *context,
                                    gint                x,
                                    gint                y,
                                    guint               time);
  gboolean (* drag_drop)           (GtkWidget          *widget,
                                    GdkDragContext     *context,
                                    gint                x,
                                    gint                y,
                                    guint               time);
  void (* drag_data_received)      (GtkWidget          *widget,
                                    GdkDragContext     *context,
                                    gint                x,
                                    gint                y,
                                    GtkSelectionData   *selection_data,
                                    guint               info,
                                    guint               time);
  
  /* Action signals */
  void (* debug_msg)               (GtkWidget          *widget,
                                    const gchar        *string);

  /* Padding for future expansion */
  GtkFunction pad1;
  GtkFunction pad2;
  GtkFunction pad3;
  GtkFunction pad4;
};
      
</pre>
            </td>
          </tr>
        </table>
        <p>
          Most of the functions in <span class="STRUCTNAME">
          GtkWidgetClass</span> are registered as default handlers
          for signals. The exceptions are <span class="STRUCTNAME">
          show_all</span> and <span class="STRUCTNAME">
          hide_all</span>, which are class functions only. Of
          course, <span class="STRUCTNAME">GtkWidgetClass</span>
          inherits the five class functions and single signal
          (<span class="SYMBOL">"destroy"</span>) from <span class= 
          "STRUCTNAME">GtkObjectClass</span>. This chapter will
          describe the important methods in more detail; also, <a
          href="sec-gdkevent.html">the section called <i>Events</i>
          in the chapter called <i>GDK Basics</i></a> is important
          for understanding the event methods. <a href= 
          "sec-widgetindetail.html">the section called <i><tt
          class="CLASSNAME">GtkWidget</tt> In Detail</i></a>
          describes the default implementation of each method in
          some detail.
        </p>
        <div class="SECT3">
          <h3 class="SECT3">
            <a name="SEC-OVERRIDESIGNALS">Overridable Signals</a>
          </h3>
          <p>
            You may notice that <span class="STRUCTNAME">
            GtkWidgetClass</span> contains two signal identifiers
            in addition to function pointers. These are 0 by
            default; otherwise, they indicate the signal to emit to
            "activate" the widget, or to set its scroll
            adjustments.
          </p>
          <p>
            The <span class="STRUCTNAME">activate_signal</span> is
            emitted when the user presses the space bar or Enter
            key while the widget is focused; for buttons, it will
            be the <span class="SYMBOL">"clicked"</span> signal,
            for menu items, the <span class="SYMBOL">
            "activate"</span> signal.
          </p>
          <p>
            The <span class="STRUCTNAME">
            set_scroll_adjustments_signal</span> is used by <tt
            class="CLASSNAME">GtkScrolledWindow</tt> to set the
            scroll adjustments used by the widget. <tt class= 
            "CLASSNAME">GtkLayout</tt>, <tt class="CLASSNAME">
            GtkCList</tt>, and others have a signal to set the
            scroll adjustments.
          </p>
          <p>
            These two hacks are necessary because GTK+ 1.2 does not
            support interfaces or multiple inheritance. (A future
            version of GTK+ may support interfaces similar in
            spirit to Java's interfaces.) Ideally, there would be
            "GtkActivatable" and "GtkScrollable" base classes or
            interfaces, and all widgets supporting these actions
            would derive from them. Including the two signal IDs in
            <span class="STRUCTNAME">GtkWidgetClass</span> is a
            short-term workaround.
          </p>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-widget.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z147.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Writing a <tt class= 
            "CLASSNAME">GtkWidget</tt></b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>An Example: The <tt
            class="CLASSNAME">GtkEv</tt> Widget</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

