<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Writing a GtkWidget
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="Advanced GTK+/Gnome Techniques" href= 
    "advanced.html">
    <link rel="PREVIOUS" title="GtkStyle and Themes" href= 
    "sec-style.html">
    <link rel="NEXT" title="The GtkWidget Base Class" href= 
    "z144.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-style.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z144.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="CHAPTER">
      <h1>
        <a name="CHA-WIDGET">Writing a <tt class="CLASSNAME">
        GtkWidget</tt></a>
      </h1>
      <div class="TOC">
        <dl>
          <dt>
            <b>Table of Contents</b>
          </dt>
          <dt>
            <a href="cha-widget.html#Z141">Overview</a>
          </dt>
          <dt>
            <a href="z144.html">The <tt class="CLASSNAME">
            GtkWidget</tt> Base Class</a>
          </dt>
          <dt>
            <a href="z147.html">An Example: The <tt class=
            "CLASSNAME">GtkEv</tt> Widget</a>
          </dt>
          <dt>
            <a href="sec-widgetindetail.html"><tt class=
            "CLASSNAME">GtkWidget</tt> In Detail</a>
          </dt>
          <dt>
            <a href="z166.html"><tt class="CLASSNAME">GtkVBox</tt>:
            A Windowless Container</a>
          </dt>
          <dt>
            <a href="z170.html"><tt class="CLASSNAME">
            GnomeAppBar</tt>: A Trivial Composite Widget</a>
          </dt>
          <dt>
            <a href="z171.html">Other Examples</a>
          </dt>
        </dl>
      </div>
      <p>
        This chapter describes how to write a new <tt class= 
        "CLASSNAME">GtkWidget</tt>. A widget is any <span class= 
        "STRUCTNAME">GtkObject</span> that derives from <tt class= 
        "CLASSNAME">GtkWidget</tt>; before reading this chapter,
        you should be familiar with <a href="cha-objects.html">the
        chapter called <i>The GTK+ Object and Type System</i></a>.
        This chapter will discuss the details of <tt class= 
        "CLASSNAME">GtkWidget</tt>, but will not re-explain <span
        class="STRUCTNAME">GtkObject</span> in general. You will
        also need to know something about GDK to write a widget; be
        sure to skim <a href="cha-gdk.html">the chapter called <i>
        GDK Basics</i></a> if you haven't.
      </p>
      <p>
        Widgets are easy to create; you only need to cut-and-paste
        the usual <span class="STRUCTNAME">GtkObject</span>
        boilerplate (instance and class initializers, a <span
        class="STRUCTNAME">get_type()</span> function, and so on),
        and then implement your widget's functionality. Writing new
        widgets is an important application development technique.
      </p>
      <p>
        After a brief overview, this chapter jumps straight to the
        implementation of a very simple widget called <tt class= 
        "CLASSNAME">GtkEv</tt>. Then it takes a step back,
        describing widget implementation more systematically. It
        ends with more examples, taken from GTK+ itself. (It pays
        to become familiar with the GTK+ source code: often the
        easiest way to implement a widget is to subclass or
        slightly modify the most similar stock GTK+ widget. Of
        course, you must comply with the terms of GTK+'s license if
        you cut-and-paste code from the library.)
      </p>
      <div class="SECT1">
        <h1 class="SECT1">
          <a name="Z141">Overview</a>
        </h1>
        <p>
          This section gives a brief overview, including the
          different kinds of widget you might encounter, and the
          general functionality a <tt class="CLASSNAME">
          GtkWidget</tt> is required to have.
        </p>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="Z142">Kinds of Widget</a>
          </h2>
          <p>
            The term "widget" is really very broad, since it
            encompasses any object that implements the <tt class= 
            "CLASSNAME">GtkWidget</tt> interface. There are many
            ways to classify widgets:
          </p>
          <ul>
            <li>
              <p>
                <i class="FIRSTTERM">Containers</i> are widgets
                that store other widgets inside, such as the boxes
                and tables described in <a href="cha-gtk.html">the
                chapter called <i>GTK+ Basics</i></a>. As <a href= 
                "cha-gtk.html">the chapter called <i>GTK+
                Basics</i></a> discussed, containers can be
                subdivided into those that add functionality to a
                single child (<tt class="CLASSNAME">GtkButton</tt>,
                <tt class="CLASSNAME">GtkFrame</tt>, <tt class= 
                "CLASSNAME">GtkEventBox</tt>, etc.), and those that
                manage layout for multiple children (<tt class= 
                "CLASSNAME">GtkBox</tt>, <tt class="CLASSNAME">
                GtkTable</tt>, etc.). Container widgets are harder
                to implement than "plain" widgets because the <tt
                class="CLASSNAME">GtkContainer</tt> interface must
                be implemented in addition to the <tt class= 
                "CLASSNAME">GtkWidget</tt> interface.
              </p>
            </li>
            <li>
              <p>
                <i class="FIRSTTERM">Composite</i> widgets are
                containers that already contain a useful collection
                of child widgets in a nice package. For example,
                the <span class="STRUCTNAME">
                GtkFileSelection</span> widget is a subclass of
                <span class="STRUCTNAME">GtkWindow</span> that
                already contains a list widget to show files,
                dialog buttons, and so on. Widgets like this are
                easy to write, and are a convenient way to code
                applications. You could write a "MainWindow" widget
                for your main application window, for example, and
                then create a new instance of the widget whenever
                the user opens a new document. <tt class=
                "CLASSNAME">GnomeApp</tt> and <tt class=
                "CLASSNAME">GnomeDialog</tt> are two important
                composite widgets in Gnome.
              </p>
            </li>
            <li>
              <p>
                Non-container widgets can be actual controls
                (buttons, scroll bars, etc.), information displays
                (<tt class="CLASSNAME">GtkLabel</tt>), or
                decorative flourishes (<tt class=
                "CLASSNAME">GtkSeparator</tt>, for example). As <a
                href="cha-gtk.html">the chapter called <i>GTK+
                Basics</i></a> briefly mentioned, there are two
                major ways to implement widgets: most widgets
                (those that need to receive events or draw their
                own background) have an associated <span class= 
                "STRUCTNAME">GdkWindow</span>; "no window" widgets
                draw on their parent container. Widgets without
                windows are implemented slightly differently. All
                containers have a <span class="STRUCTNAME">
                GdkWindow</span> (since widgets without one might
                need to draw on it, among other reasons).
              </p>
            </li>
          </ul>
          <p>
            This chapter presents several widgets as examples,
            including a <tt class="CLASSNAME">GtkEv</tt> widget
            written especially for this book, <tt class=
            "CLASSNAME">GtkVBox</tt> from GTK+, and <tt class= 
            "CLASSNAME">GnomeAppBar</tt> from <tt class=
            "APPLICATION">libgnomeui</tt>.
          </p>
        </div>
        <div class="SECT2">
          <h2 class="SECT2">
            <a name="Z143">What a Widget Does</a>
          </h2>
          <p>
            A minimal widget implements the following (though <tt
            class="CLASSNAME">GtkWidget</tt>'s default
            implementation may be sufficient in many cases):
          </p>
          <ul>
            <li>
              <p>
                Creation and destruction; this means the usual
                <span class="STRUCTNAME">GtkObject</span>
                boilerplate (instance and class initializers,
                shutdown, destroy and finalize methods). See <a
                href="cha-objects.html">the chapter called <i>The
                GTK+ Object and Type System</i></a>, especially <a
                href="cha-objects.html#SEC-OBJECTSTRUCTS">the
                section called <i>Object and Class Structures</i>
                in the chapter called <i>The GTK+ Object and Type
                System</i></a> and <a href="sec-finalization.html">
                the section called <i>Object Finalization</i> in
                the chapter called <i>The GTK+ Object and Type
                System</i></a>. Also see <a href= 
                "z57.html#WIDGETLIFECYCLE">the section called <i>
                Widget Life Cycle</i> in the chapter called <i>GTK+
                Basics</i></a>.
              </p>
            </li>
            <li>
              <p>
                The realize/map/unmap/unrealize cycle discussed in
                <a href="z57.html#SEC-REALIZINGSHOWING">the section
                called <i>Realizing, Mapping, and Showing</i> in
                the chapter called <i>GTK+ Basics</i></a>. Widgets
                must be able to create and uncreate their
                associated X resources any number of times, and
                they must be able to show and hide themselves any
                number of times.
              </p>
            </li>
            <li>
              <p>
                Geometry negotiation, discussed in <a href= 
                "sec-containers.html#SEC-SIZENEGOTIATION">the
                section called <i>Size Allocation</i> in the
                chapter called <i>GTK+ Basics</i></a>. Your widget
                must respond to size requests, and honor size
                allocations.
              </p>
            </li>
            <li>
              <p>
                Drawing; widgets must be able to draw themselves on
                the screen. For container widgets, the widget
                itself may be invisible but it must ensure child
                widgets are drawn.
              </p>
            </li>
            <li>
              <p>
                The widget's unique functionality. Typically this
                means implementing handlers for some of the
                widget's event signals.
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-style.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z144.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b><span class= 
            "STRUCTNAME">GtkStyle</span> and Themes</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>The <tt class= 
            "CLASSNAME">GtkWidget</tt> Base Class</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

