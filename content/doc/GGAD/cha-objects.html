<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      The GTK+ Object and Type System
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="Advanced GTK+/Gnome Techniques" href= 
    "advanced.html">
    <link rel="PREVIOUS" title="Advanced GTK+/Gnome Techniques"
    href="advanced.html">
    <link rel="NEXT" title="Type Checking and New Types" href= 
    "z105.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="advanced.html"><font color="#0000ff" size="2">
            <b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z105.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="CHAPTER">
      <h1>
        <a name="CHA-OBJECTS">The GTK+ Object and Type System</a>
      </h1>
      <div class="TOC">
        <dl>
          <dt>
            <b>Table of Contents</b>
          </dt>
          <dt>
            <a href="cha-objects.html#SEC-OBJECTSTRUCTS">Object and
            Class Structures</a>
          </dt>
          <dt>
            <a href="z105.html">Type Checking and New Types</a>
          </dt>
          <dt>
            <a href="sec-classinit.html">Initializing a New
            Class</a>
          </dt>
          <dt>
            <a href="sec-gtkarg.html"><span class="STRUCTNAME">
            GtkArg</span> and the Type System</a>
          </dt>
          <dt>
            <a href="hc-objectargs.html">Object Arguments</a>
          </dt>
          <dt>
            <a href="z109.html">Signals</a>
          </dt>
          <dt>
            <a href="sec-finalization.html">Object Finalization</a>
          </dt>
          <dt>
            <a href="sec-objectdata.html">Attaching Data to
            Objects</a>
          </dt>
        </dl>
      </div>
      <p>
        People often ask why GTK+ was written in C rather than an
        object-oriented language. The answer is that C is more
        portable and standardly available than any other language.
        However, although C lacks syntactic sugar for
        object-oriented programming, it in no way prohibits an
        object-oriented approach.
      </p>
      <p>
        GTK+ implements its own custom object system, which offers
        standard object-oriented features such as inheritance and
        virtual functions. In the tradition of languages such as
        Lisp, Smalltalk, and Java, the GTK+ object system is more
        runtime-centric than that of C++, allowing interpreted
        language bindings and GUI builders to interact with it in
        powerful ways.
      </p>
      <p>
        You may recall from <a href="cha-gtk.html">the chapter
        called <i>GTK+ Basics</i></a> that widgets are a special
        type of <span class="STRUCTNAME">GtkObject</span>; any
        object with <tt class="CLASSNAME">GtkWidget</tt> in its
        ancestry is a widget. Widgets represent a region on the
        screen---most of them are user interface elements, such as
        buttons or menus. There's nothing GUI-specific about <span
        class="STRUCTNAME">GtkObject</span>; the object system can
        be used in non-graphical programs.
      </p>
      <p>
        This chapter dives right into the details of GTK+'s object
        system, giving you an idea what's happening "behind the
        scenes" in any GTK+ program. Sooner or later you'll need
        this information: to write your own objects, debug existing
        objects, or just understand GTK+ code on a conceptual
        level.
      </p>
      <div class="SECT1">
        <h1 class="SECT1">
          <a name="SEC-OBJECTSTRUCTS">Object and Class
          Structures</a>
        </h1>
        <p>
          Each <span class="STRUCTNAME">GtkObject</span> has two
          essential components: a struct representing an <i class= 
          "EMPHASIS">instance</i> of the object, and a struct
          representing the <i class="EMPHASIS">class</i>. In
          general, the instance struct contains the data members
          for each instance, and the class struct contains class
          function pointers (which can be overridden by
          subclasses). The class struct can also contain class data
          members---however, it's more typical to use static
          variables in the <span class="STRUCTNAME">.c</span> file
          implementing the object. If you're familiar with C++, the
          class struct is equivalent to a vtable, only the class
          struct is written by hand. It stores virtual functions
          for an object type.
        </p>
        <p>
          Here are the structs used in <tt class="CLASSNAME">
          GtkButton</tt>:
        </p>
        <table border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
<pre class="PROGRAMLISTING">
typedef struct _GtkButton       GtkButton;
typedef struct _GtkButtonClass  GtkButtonClass;

struct _GtkButton
{
  GtkBin bin;

  GtkWidget *child;

  guint in_button : 1;
  guint button_down : 1;
  guint relief : 2;
};

struct _GtkButtonClass
{
  GtkBinClass        parent_class;
  
  void (* pressed)  (GtkButton *button);
  void (* released) (GtkButton *button);
  void (* clicked)  (GtkButton *button);
  void (* enter)    (GtkButton *button);
  void (* leave)    (GtkButton *button);
};

</pre>
            </td>
          </tr>
        </table>
        <p>
          Notice that the first member of <span class="STRUCTNAME">
          struct _GtkButton</span> is <tt class="CLASSNAME">
          GtkBin</tt>---that's because <tt class="CLASSNAME">
          GtkButton</tt> is a subclass of <tt class="CLASSNAME">
          GtkBin</tt>. (<tt class="CLASSNAME">GtkBin</tt> is a <tt
          class="CLASSNAME">GtkContainer</tt> that can hold one
          child.) Since <tt class="CLASSNAME">GtkBin</tt> is the
          first member, we can safely cast a <tt class="CLASSNAME">
          GtkButton</tt> to <tt class="CLASSNAME">GtkBin</tt>. In
          <span class="STRUCTNAME">struct _GtkButtonClass</span>,
          the same principle applies, and <span class="STRUCTNAME">
          GtkBinClass</span> is the first member.
        </p>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="advanced.html"><font color="#0000ff" size="2">
            <b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z105.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Advanced GTK+/Gnome
            Techniques</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Type Checking and New
            Types</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

