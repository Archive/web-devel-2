<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Modal Dialogs
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="User Communication: Dialogs" href= 
    "cha-dialogs.html">
    <link rel="PREVIOUS" title="User Communication: Dialogs" href= 
    "cha-dialogs.html">
    <link rel="NEXT" title="A Dialog Example" href="z101.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-dialogs.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z101.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-MODALDIALOGS">Modal Dialogs</a>
      </h1>
      <p>
        Sometimes you need to prevent interaction with the rest of
        your application while the user manipulates a dialog.
        Dialogs that freeze the rest of the application in this way
        are called <i class="FIRSTTERM">modal</i> dialogs.
      </p>
      <p>
        There is a lot of debate about when to use modal dialogs;
        some users hate them passionately, but there are times when
        they are necessary. Unfortunately, it is a little bit
        easier to write code using modal dialogs, because you can
        stop in the middle of a function, wait for a user response,
        then continue. With nonmodal dialogs, you have to return
        the flow of control to the main application, and arrange
        callbacks to pick up where you left off when the user
        finally deals with the dialog. With a complex sequence of
        dialogs, the result is ugly spaghetti code. This tempts
        many programmers to use modal dialogs all the time, or at
        least a little too often. Avoid the temptation, and your
        users will thank you.
      </p>
      <p>
        Avoid modal dialogs if users might want to refer back to
        information in the main application as they use the dialog,
        or if they might want to cut-and-paste between the
        application and the dialog. "Properties" dialogs should
        usually be nonmodal; because users will want to experiment
        with the effects of the changes they make, without having
        to close the dialog. And there's no reason to make trivial
        message boxes modal, since clicking on them has no effect
        on the rest of the application.
      </p>
      <p>
        Do not be afraid to use a modal dialog if it makes sense,
        however. For example, I wrote a frontend for the Debian
        package manager, called <tt class="APPLICATION">
        gnome-apt</tt>. The main application allows the user to
        select software packages for installation and removal; then
        there are a series of dialogs which ask for confirmation
        and perform the requested changes. These dialogs are modal,
        because it would make no sense to change a request in the
        middle of performing it. Changing the request should imply
        restarting the request-fulfillment process and asking for
        confirmation a second time. Another example: the "File
        Properties" dialog for the Gnome file manager is modal,
        because otherwise the user could delete the file while its
        properties were being edited---a strange situation. There
        are no hard and fast rules; you'll have to use your
        judgment on a dialog-by-dialog basis.
      </p>
      <p>
        All that said, it is very easy to create a modal dialog. In
        GTK+, any window can be made modal with <tt class=
        "FUNCTION">gtk_window_set_modal()</tt> (<a href= 
        "sec-modaldialogs.html#FL-SETMODAL">Figure 5</a>).
      </p>
      <div class="FIGURE">
        <a name="FL-SETMODAL"></a>
        <div class="FUNCSYNOPSIS">
          <a name="FL-SETMODAL.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
     #include &lt;gtk/gtkwindow.h&gt;
    
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF"><tt class="FUNCTION">
            gtk_window_set_modal</tt></code>(GtkWindow* <tt class= 
            "PARAMETER"><i>window</i></tt>, gboolean <tt class= 
            "PARAMETER"><i>modality</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 5. Modal Windows</b>
        </p>
      </div>
      <p>
        Since <tt class="CLASSNAME">GnomeDialog</tt> is a <tt
        class="CLASSNAME">GtkWindow</tt> subclass, this function
        works fine. It simply blocks all interaction with windows
        other than the modal one.
      </p>
      <p>
        Typically you want to go a step further, and wait for the
        user to click one of the dialog buttons without setting up
        a lot of callbacks. In GTK+ this is done by running a
        second instance of <tt class="FUNCTION">gtk_main()</tt>,
        entering another, nested event loop. When the second loop
        exits, the flow of control returns to just after your <tt
        class="FUNCTION">gtk_main()</tt> call. However there are a
        host of complications and race conditions, due to the large
        number of ways to close a dialog; the resulting code is
        somewhat unpleasant and error-prone. The two functions in
        <a href="sec-modaldialogs.html#FL-DIALOGRUN">Figure 6</a>
        are provided to save your from the mess.
      </p>
      <div class="FIGURE">
        <a name="FL-DIALOGRUN"></a>
        <div class="FUNCSYNOPSIS">
          <a name="FL-DIALOGRUN.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
     #include &lt;libgnomeui/gnome-dialog.h&gt;
    
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF">gint <tt class="FUNCTION">
            gnome_dialog_run</tt></code>(GnomeDialog* <tt class= 
            "PARAMETER"><i>dialog</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">gint <tt class="FUNCTION">
            gnome_dialog_run_and_close</tt></code>(GnomeDialog* <tt
            class="PARAMETER"><i>dialog</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 6. "Running" a Dialog</b>
        </p>
      </div>
      <p>
        These two functions block until the user clicks a dialog
        button, clicks the window manager's close decoration, or
        does the equivalent with a key shortcut. If a button was
        clicked, they return that button's number; recall that <tt
        class="CLASSNAME">GnomeDialog</tt> buttons are numbered
        from left to right starting with <span class="STRUCTNAME">
        0</span>. If no button was clicked (the dialog was closed
        via window manager), they return <span class="STRUCTNAME">
        -1</span> instead.
      </p>
      <p>
        The dialog is automatically made modal for the duration of
        the call; otherwise chaos would reign. (For example,
        calling <tt class="FUNCTION">gtk_main_quit()</tt> from your
        main application code would quit the nested <tt class= 
        "FUNCTION">gtk_main()</tt> rather than the primary one.)
        However, if you plan to leave the dialog open after <tt
        class="FUNCTION">gnome_dialog_run()</tt> returns, and you
        want it to be modal, you should manually make it modal; <tt
        class="FUNCTION">gnome_dialog_run()</tt> will only change
        the dialog's modality temporarily.
      </p>
      <p>
        It is your responsibility to figure out how the dialog will
        be closed or destroyed before you call <tt class=
        "FUNCTION">gnome_dialog_run()</tt>. You can set the dialog
        up so that no user actions destroy it, then destroy it
        yourself after <tt class="FUNCTION">gnome_dialog_run()</tt>
        returns. Or you can set the dialog up so that all user
        actions destroy it, then forget about it after <tt class= 
        "FUNCTION">gnome_dialog_run()</tt> returns. You could also
        write a loop, calling <tt class="FUNCTION">
        gnome_dialog_run()</tt> repeatedly until the user gives
        valid input, and closing the dialog only after the loop
        ends. If you write a loop, be careful to manually make the
        dialog modal; otherwise there will be short intervals where
        it is not.
      </p>
      <p>
        <tt class="FUNCTION">gnome_dialog_run_and_close()</tt>
        monitors the dialog's <span class="SYMBOL">"close"</span>
        and <span class="SYMBOL">"destroy"</span> signals, and
        closes the dialog if and only if it does not close
        "naturally" in response to user clicks or keystrokes. Using
        this function guarantees that <tt class="FUNCTION">
        gnome_dialog_close()</tt> will be called exactly once
        before it returns, unless you connect truly malicious
        callbacks to sabotage the process. <tt class="FUNCTION">
        gnome_dialog_run_and_close()</tt> is not very useful in my
        opinion; it is little more than a way to avoid thinking
        about how the dialog will be closed.
      </p>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="cha-dialogs.html"><font color="#0000ff" size=
            "2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="z101.html"><font color="#0000ff" size="2"><b>
            Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>User Communication:
            Dialogs</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>A Dialog
            Example</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

