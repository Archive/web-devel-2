<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>
      Drawables and Pixmaps
    </title>
    <meta name="GENERATOR" content=
    "Modular DocBook HTML Stylesheet Version 1.45">
    <link rel="HOME" title="GTK+ / Gnome Application Development"
    href="ggad.html">
    <link rel="UP" title="GDK Basics" href="cha-gdk.html">
    <link rel="PREVIOUS" title="Visuals and Colormaps" href= 
    "sec-gdkvisual.html">
    <link rel="NEXT" title="Events" href="sec-gdkevent.html">
  </head>
  <body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink= 
  "#840084" alink="#0000FF">
    <div class="NAVHEADER">
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <th colspan="4" align="center">
            <font color="#000000" size="2">GTK+ / Gnome Application
            Development</font>
          </th>
        </tr>
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkvisual.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkevent.html"><font color="#0000ff" size= 
            "2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
      </table>
    </div>
    <div class="SECT1">
      <h1 class="SECT1">
        <a name="SEC-GDKDRAWABLE">Drawables and Pixmaps</a>
      </h1>
      <p>
        A <i class="FIRSTTERM">pixmap</i> is an off-screen buffer
        you can draw graphics into. After drawing into a pixmap,
        you can copy it to a window, causing it to appear on the
        screen (when the window is visible). (You can also draw
        into a window directly, of course. Using a pixmap as a
        buffer allows you to rapidly update the screen without
        repeating a series of primitive drawing operations.)
        Pixmaps are also good to store image data loaded from disk,
        such as icons and logos. You can then copy the image to a
        window. In GDK, the pixmap type is called <span class= 
        "STRUCTNAME">GdkPixmap</span>. A pixmap with a single bit
        representing each pixel is called a <span class=
        "STRUCTNAME">bitmap</span>; GDK's bitmap type is <span
        class="STRUCTNAME">GdkBitmap</span>. "Bitmap" is not really
        a separate type; from X's point of view, it is simply a
        pixmap with a depth of 1. Like windows, pixmaps are
        server-side resources.
      </p>
      <p>
        In X terminology, a <i class="FIRSTTERM">drawable</i> is
        anything you can draw graphics on. GDK has a corresponding
        type, called <span class="STRUCTNAME">GdkDrawable</span>.
        Drawables include windows, pixmaps, and bitmaps. Here is
        how the types are defined in GDK:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
typedef struct _GdkWindow GdkWindow;
typedef struct _GdkWindow GdkPixmap;
typedef struct _GdkWindow GdkBitmap;
typedef struct _GdkWindow GdkDrawable;
</pre>
          </td>
        </tr>
      </table>
      <p>
        On the client side, pixmaps and bitmaps are just <span
        class="STRUCTNAME">GdkWindow</span>s with type <span class= 
        "STRUCTNAME">GDK_WINDOW_PIXMAP</span>. <span class= 
        "STRUCTNAME">GdkDrawable</span> is used in function
        declarations when either a window or a pixmap is an
        acceptable argument. Functions that draw graphics take
        either type; functions that move windows around or set
        window manager hints accept only windows. Only windows can
        receive events. <span class="STRUCTNAME">
        GDK_INPUT_ONLY</span> windows are a special case; they are
        not drawables and you can't draw on them.
      </p>
      <p>
        Three of the four logical combinations of "window features"
        and drawability actually exist:
      </p>
      <table border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
<pre class="PROGRAMLISTING">
                       Drawable             Not Drawable
Window Features      Normal Window         Input Only Window
No Window Features   Pixmap/Bitmap            ---
</pre>
          </td>
        </tr>
      </table>
      <p>
        Unfortunately, all three of these logically distinct cases
        appear the same from a type-checking point of view. So be
        careful not to use the wrong one. Also keep in mind that a
        normal window is not drawable until it actually appears on
        the screen; you should wait until you receive an expose
        event before you draw. Expose events are covered in <a
        href="sec-gdkevent.html#SEC-EXPOSEEVENTS">the section
        called <i>Expose Events</i></a>.
      </p>
      <p>
        Like <span class="STRUCTNAME">GdkWindow</span>, a <span
        class="STRUCTNAME">GdkPixmap</span> is merely a client-side
        handle for an object located on the X server. Because of
        this, some things are entirely infeasible from a
        performance point of view; notably, if you are doing
        anything which requires significant manipulation of
        individual pixels, drawables will be far too slow. On the
        other hand, copying a pixmap to a window is not as slow as
        you might think, because both objects are on the same
        machine.
      </p>
      <p>
        Creating a pixmap is much easier than creating a window,
        because most of the window attributes are not relevant to
        pixmaps. The function is <tt class="FUNCTION">
        gdk_pixmap_new()</tt> (<a href= 
        "sec-gdkdrawable.html#FL-PIXNEW">Figure 4</a>). It accepts
        an initial size, and a bit depth. If a depth of <span
        class="STRUCTNAME">-1</span> is given, the depth is copied
        from its <span class="STRUCTNAME">GdkWindow</span>
        argument. You can't choose an arbitrary number for the
        depth---the server will not support all depths, and the
        pixmap's depth must match the depth of any windows you plan
        to copy it to. To destroy a pixmap, call <tt class=
        "FUNCTION">gdk_pixmap_unref()</tt>.
      </p>
      <p>
        The <span class="STRUCTNAME">GdkWindow</span> argument to
        <tt class="FUNCTION">gdk_pixmap_new()</tt> may not seem
        strictly necessary. However, the function wraps <tt class= 
        "FUNCTION">XCreatePixmap()</tt>, which takes an X window as
        an argument. It uses this argument to determine which <i
        class="FIRSTTERM">screen</i> to create the window on; some
        X servers have multiple displays. Screens are an Xlib
        concept totally concealed by GDK; GDK supports only one
        screen at a time. Thus the <span class="STRUCTNAME">
        window</span> argument to <tt class="FUNCTION">
        gdk_pixmap_new()</tt> seems mysterious from a GDK point of
        view.
      </p>
      <div class="FIGURE">
        <a name="FL-PIXNEW"></a>
        <div class="FUNCSYNOPSIS">
          <a name="FL-PIXNEW.SYNOPSIS"></a>
          <table border="0" bgcolor="#E0E0E0" width="100%">
            <tr>
              <td>
<pre class="FUNCSYNOPSISINFO">
#include &lt;gdk/gdk.h&gt;
</pre>
              </td>
            </tr>
          </table>
          <p>
            <code><code class="FUNCDEF">GdkPixmap* <tt class= 
            "FUNCTION">gdk_pixmap_new</tt></code>(GdkWindow* <tt
            class="PARAMETER"><i>window</i></tt>, gint <tt class= 
            "PARAMETER"><i>width</i></tt>, gint <tt class=
            "PARAMETER"><i>height</i></tt>, gint <tt class= 
            "PARAMETER"><i>depth</i></tt>);</code>
          </p>
          <p>
            <code><code class="FUNCDEF">void <tt class="FUNCTION">
            gdk_pixmap_unref</tt></code>(GdkPixmap* <tt class= 
            "PARAMETER"><i>pixmap</i></tt>);</code>
          </p>
        </div>
        <p>
          <b>Figure 4. <span class="STRUCTNAME">GdkPixmap</span>
          Constructor</b>
        </p>
      </div>
    </div>
    <div class="NAVFOOTER">
      <br>
      <br>
      <table width="100%" border="0" bgcolor="#ffffff" cellpadding= 
      "1" cellspacing="0">
        <tr>
          <td width="25%" bgcolor="#ffffff" align="left">
            <a href="sec-gdkvisual.html"><font color="#0000ff"
            size="2"><b>&lt;&lt;&lt; Previous</b></font></a>
          </td>
          <td width="25%" colspan="2" bgcolor="#ffffff" align= 
          "center">
            <font color="#0000ff" size="2"><b><a href="ggad.html">
            <font color="#0000ff" size="2"><b>
            Home</b></font></a></b></font>
          </td>
          <td width="25%" bgcolor="#ffffff" align="right">
            <a href="sec-gdkevent.html"><font color="#0000ff" size= 
            "2"><b>Next &gt;&gt;&gt;</b></font></a>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="left">
            <font color="#000000" size="2"><b>Visuals and
            Colormaps</b></font>
          </td>
          <td colspan="2" align="right">
            <font color="#000000" size="2"><b>Events</b></font>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>

