<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><title>What does porting mean?</title><meta name="generator" content="DocBook XSL Stylesheets V1.45"><link rel="home" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="up" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="previous" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="next" href="ar01s03.html" title="The platform libraries"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">What does <span class="emphasis"><i>porting</i></span> mean?</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="index.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="ar01s03.html">Next</a></td></tr></table><hr></div><div class="sect1"><div class="titlepage"><div><h2 class="title" style="clear: both"><a name="porting-description"></a>What does <span class="emphasis"><i>porting</i></span> mean?</h2></div></div><p>This document is about porting applications to GNOME 2; you already
   know that. However, that can mean many things to different people, so in
   this section I have tried to give an idea of what porting might involve in
   general and what the different levels are. </p><p>It is not unreasonable to think of porting as a series of steps
   through the following list (not necessarily in order): </p><div class="orderedlist"><ol type="1"><li><p>Do the minimal conversions required to make the application
         build and run with the GNOME 2 libraries (and any auxiliary
         libraries). </p></li><li><p>Build without needing to define
         <tt>GTK_ENABLE_BROKEN</tt> (see <a href="ar01s09.html#gtk-deprecation">Deprecation</a> in the
         GTK+ section for more information). </p></li><li><p>Build with <tt>GTK_DISABLE_DEPRECATED</tt> and
         <tt>GNOME_DISABLE_DEPRECATED</tt> constants defined (see
         <a href="ar01s07.html#libgnome-deprecation">Deprecation management in libgnome
      and libgnomeui</a> for a description of these
         constants). </p></li><li><p>Modernize the documentation. This doesn't just mean port the
         documentation across to GNOME 2 standards. There is more work involved
         if you want to be really conscientious about it.
            <div class="itemizedlist"><ul><li><p>If you don't have user documentation and you are
                  writing an application that is not just a library, then you
                  need to write some (or find a volunteer who would like to
                  write the documentation). </p></li><li><p>If your application is to be used by other applications
                  (that is, it is a library or component), then you need to
                  have some API documentation. This can be as simple as putting
                  the necessary comments in the code and running
                  gtk-doc as part of the build
                  process. You may wish to go further, but at least provide
                  <span class="emphasis"><i>some</i></span> assistance to developers wanting to
                  use your product. </p></li><li><p>Clean up any old documentation that is lying around. A
                  large number of modules in the GNOME CVS repository have
                  files sitting around that were essentially &quot;brain dumps&quot; from
                  the developers two years ago and they don't really relate to
                  anything now. Or maybe you have a document describing how to
                  move from version 2 to version 3 of your application, but it
                  was last edited eight months ago and is now hopelessly out of
                  date. </p><p>If you don't want to just remove some of these
                  historical documents (which is entirely understandable), you
                  could at least move them into a new directory called
                  something like <tt>old-docs</tt> to make their
                  relevance to the project clear. </p></li><li><p>Do a pass through the <tt>README</tt>,
                  <tt>TODO</tt>, <tt>NEWS</tt>,
                  <tt>AUTHORS</tt> and <tt>HACKING</tt>
                  (or <tt>README.cvs-commits</tt>) files in your
                  project. Are they up to date? Are contact email addresses
                  still valid and have you clearly indicated what the right
                  contact paths are (some authors may have moved on and
                  should not be contacted any more when reporting bugs, for
                  example)? All of these files tend to atrophy as maintainers
                  forget to update them and so people stop trusting them. Your
                  GNOME 2 release is a perfect time to remedy this situation,
                  if it exists! </p></li></ul></div>
         </p></li><li><p>Make your application accessible. Have a read of the <a href="ar01s13.html">Accessibility</a>
         section in this document and look the Accessibility Project's <a href="http://developer.gnome.org/projects/gap/" target="_top">website</a>. </p></li><li><p>Use <a href="ar01s21.html" title="GConf">gconf</a> instead of
         <tt>gnome-config.h</tt> for as much of your configuration
         as you can. Also, utilize the platform supplied
         gconf settings for things like HTTP
         proxies. This will mean your users can set things up in a consistent
         fashion and need not have to do it for each and every application.
         </p></li><li><p>Design your application to work in an environment where the user
         may be logged in multiple times simultaneously. In other words, you
         should be prepared to manage some of the application state based on
         the particular session that is calling it, if necessary. For
         information about this, have a look at the <a href="ar01s23.html">Session management</a>
         section in this document. </p></li><li><p>Implement the recommendations from the <a href="http://developer.gnome.org/projects/gup/hig/" target="_top"><i>Human
         Interface Guidelines</i></a>, as mentioned in the
         introduction. </p></li></ol></div><p>How many of these items you might like to be able to complete for your
   own application is a decision for you and any other developers. However, it
   is not unreasonable to hope that core applications in the GNOME 2 release
   might meet all of these criteria and other popular applications would try to
   fulfill as many as possible. </p><p>As a general guide, working well with gconf
   and session management are new additions for GNOME 2, but should be
   considered very important as part of &quot;playing nicely&quot; with the rest of the
   platform. Similarly, accessibility and usability are two facets that are
   being given a lot of attention for GNOME 2 and applications which do not
   make efforts in these areas are likely to stand out from the crowd (and not
   in a good way). </p><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="general-considerations"></a>General considerations</h3></div></div><p>These items are not really related to the technical aspects of
      porting, but they are often overlooked and you could do worse than
      consider them during the &quot;cleaning up&quot; phase of your porting process.
      </p><div class="itemizedlist"><ul><li><p>Make sure that the <b>make distcheck</b> target
            works for your application. Preferably, keep it working all the
            time. This will ensure that your application has all of the
            necessary files distributed when you create a tarball and will,
            amongst other things, ensure that it can be built when the build
            directory is not the same as the source directory (which is an
            often overlooked case). </p></li><li><p>Before releasing the GNOME 2 compatible version of your
            application, try to build it from scratch on a &quot;clean&quot; system. Too
            often a developer's main system will have little pieces from
            previous library versions or installations hanging around and being
            accidentally used by the new package. Or you will discover that
            something you assumed was available on a standard installation
            actually needs to be downloaded separately. Extra attention to
            those sorts of details before release will spare you and your users
            a lot of annoyance at a later date. </p></li></ul></div></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="index.html">Prev</a> </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right"> <a accesskey="n" href="ar01s03.html">Next</a></td></tr><tr><td width="40%" align="left">Porting applications to the GNOME 2.0 platform </td><td width="20%" align="center"><a accesskey="u" href="index.html">Up</a></td><td width="40%" align="right"> The platform libraries</td></tr></table></div></body></html>
