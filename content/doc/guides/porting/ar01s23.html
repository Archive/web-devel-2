<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><title>Session management</title><meta name="generator" content="DocBook XSL Stylesheets V1.45"><link rel="home" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="up" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="previous" href="ar01s22.html" title="Other libraries"><link rel="next" href="apa.html" title="A. Module dependencies"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Session management</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ar01s22.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="apa.html">Next</a></td></tr></table><hr></div><div class="sect1"><div class="titlepage"><div><h2 class="title" style="clear: both"><a name="session-management"></a><a name="session-management-title"></a>Session management</h2></div><div><h3 class="author">Havoc Pennington</h3><div class="affiliation"><div class="address"><p><tt>&lt;<a href="mailto:hp@redhat.com">hp@redhat.com</a>&gt;</tt></p></div></div></div></div><p> The concept of session management hasn't changed much in the
   move from GNOME 1 to GNOME 2. Applications should still use the
   <tt>&lt;libgnomeui/gnome-client.h&gt;</tt> interface to
   make their applications session aware. It's important to be clear
   about what parts of application state should be saved per-session, 
   and which should be stored as preferences. This document tries to 
   clarify that issue.
   </p><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="session-management-overview"></a>Preferences and Session Management Overview</h3></div></div><p>
        Applications typically have <i>preferences</i> 
        which are global and apply to all instances of the
        application. Ideally, changing a preference in one instance 
        of the application will automatically change the same preference in all
        other instances. GConf makes this happen automatically.
      </p><p>
         A <i>session</i> is a collection of
         application instances, possibly defined under some
         user-provided name. For example, the user might have &quot;Home&quot;
         and &quot;Work&quot; sessions. The set of application instances can be
         saved to disk, and then later restored. Sessions are saved
         and restored by a special program called the
         <i>session manager</i>. Each application
         instance provides the session manager with enough information to
         restart that application instance in its current state.
      </p><p>
         Sessions are saved by taking &quot;snapshots&quot; of a group of
         applications connected to the session manager. For each
         snapshot, the session manager sends a request called a
         &quot;SaveYourself&quot; to all application instances. Each instance
         then provides the session manager with a command line that,
         when executed, will result in an application instance with
         the same state as the currently-open instance,
         <span class="emphasis"><i>and</i></span> the same <i>session client
         ID</i>.
                  <sup>[<a name="id2807889" href="#ftn.id2807889">12</a>]</sup>
         For example, 
         a terminal program with three windows open might give 
         the session manager the command line <tt>terminal
         --with-window-count=3 --session-client-id=2341341534</tt>, or
         something along those lines.
      </p><p>
        Note that each &quot;SaveYourself&quot; is independent of the others. 
        The same application instance (with the same session ID) may
        be asked to save multiple times in the same session. Each save
        must continue to work — apps do not know which one of the
        saves will be used by the session manager. As a result, if 
        applications save persistent state, they should namespace that 
        persistent state with a per-save unique key. Unfortunately, 
        gnome-libs has no way to generate such a key; one simple
        method might be:
<pre class="programlisting">
 key = g_strdup_printf (&quot;%d-%d-%u&quot;,
                        (int) getpid (),
                        (int) time (),
                        g_random_int ());                          
</pre>
        The application would then use this key as part of the configuration
        key used to store per-save data. For example, in GConf you might store
        per-save data under
        <tt>/apps/<i><tt>application_name</tt></i>/sessions/<i><tt>save_key</tt></i></tt>
        where <i><tt>save_key</tt></i> is the per-save key. </p><p>
       Note that saving per-save data in a persistent location
       requires you to provide a &quot;discard command&quot; to the session
       manager, so it can clean up your persistent data 
       when it throws out the saved snapshot in question. 
       This can all be kind of a pain, so apps are encouraged to 
       save all their state as part of the command they pass to the
       session manager, and avoid saving persistent files.  That is, it's
       easier and more robust to have a command line option
       <i><tt>--with-window-count=3</tt></i> than it is to store the
       window count in GConf under
       <tt>/apps/terminal/sessions/4543-3252345-6745/window_count</tt>
       and provide a discard command to clear that key.  </p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p> Sometimes GNOME has a piece of state that should be
         per-session, but belongs to the entire desktop.  For example, say you
         wanted per-session background images.  We thought we had a plan here
         with <tt>gnome_client_get_desktop_id()</tt>, but we were
         wrong. We don't have a plan yet. We are trying to come up with one.
         </p></div></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="session-where-to-put-data"></a>Where to store data?</h3></div></div><p> By following the rules below it should be relatively easy to
      determine how to give your application the correct session support.
      </p><p>
         <div class="itemizedlist"><ul><li><p>Only settings related to application <span class="emphasis"><i> instance
               </i></span> state should be stored when the session manager asks
               you to save. Examples are the currently-open windows, and what
               documents are opened in the windows. Most settings are global
               preferences and not session state. Session state is ideally
               simple enough to encode in the command line used to launch your
               application. </p></li><li><p>The window manager stores window positions. Application
               developers need not bother to do so. </p></li><li><p>Important or critical data, including preferences that
               take effort to set up, should never be stored per-session. If a
               user spends a lot of time setting up their panel or terminal,
               they do not want to lose that data when changing sessions, or if
               something goes wrong with the session system.  Data such as
               documents should <span class="emphasis"><i>NEVER</i></span> go in the
               instance-specific location. </p></li><li><p>User preferences should almost always apply globally to
               all application instances in all sessions. With
               gconf this can happen without even
               restarting the application instance. </p></li><li><p><span class="emphasis"><i>(FIXME: We have to figure out how to handle
               desktop-wide per-session info.)</i></span>
               </p></li><li><p>In no case should the same setting be stored both
               per-instance and globally. Let's repeat that, to avoid some
               disasters that happened in GNOME 1: <span class="emphasis"><i>DO NOT STORE THE
               SAME SETTING BOTH PER-INSTANCE AND GLOBALLY.</i></span>
               </p></li><li><p>Per-session settings should not appear in any preferences
               dialogs, because it's confusing. Preferences dialogs should
               affect the global state of the application. Anything
               per-instance should be in a menu or something, not in the
               preferences dialog. </p></li></ul></div>
      </p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="session-implementation-notes"></a>Implementation Notes</h3></div></div><p> Often you want to allow per-session setups, but setting up an
      application involves noticeable user effort. One suggested solution to
      this problem is to have a &quot;named profile&quot; feature in the application. Make
      the list of profiles, and their contents, a global preference making it
      easy to restore the activate profile when necessary. The active profile
      can be a per-session or per-instance setting. Using profiles is
      complicated so most applications should not need to use them.
         <sup>[<a name="id2808783" href="#ftn.id2808783">13</a>]</sup>
      </p><p> Applications <span class="emphasis"><i>must always</i></span> open the same number
      of windows they had open at session save time. If you have not implemented
      this, then please <span class="emphasis"><i>do not</i></span> connect to the session
      manager because you will confuse the window manager when it tries to
      position missing windows. Use
      <tt>gnome_disable_master_client()</tt> until you have
      implemented this.
     </p><p> When setting the <tt>RestartCommand</tt> for your
      application, use the standard
      <tt>--sm-client-id</tt> option to pass the current
      session ID to the restarted application. </p><p> If you store session state in gconf, the
      <tt>DiscardCommand</tt> can use the
      <tt>--recursive-unset</tt> option to
      gconftool. </p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="session-reference"></a>References</h3></div></div><p>
         <div class="itemizedlist"><ul><li><p> <a href="http://mail.gnome.org/archives/gnome-hackers/2001-November/msg00003.html" target="_top">Session
               Management for GNOME 2.0</a> - Havoc Pennington
               2001-11-01 (Note one important mistake in the original
               post, which is that session state should be
               per-session-save, not per-session-ID - in other words, do
               <span class="emphasis"><i>not</i></span> use the session ID to key your saved
               session data.) </p></li></ul></div>
      </p></div><div class="footnotes"><br><hr width="100" align="left"><div class="footnote"><p><sup>[<a name="ftn.id2807889" href="#id2807889">12</a>] </sup> The application session ID is a globally unique
                     Latin-1 encoded string identifying an application
                     instance.  Applications receive their ID from the session
                     manager by calling
                     <tt>gnome_client_get_id()</tt>. This ID may be
                     <tt>NULL</tt> if not connected to the session
                     manager.</p></div><div class="footnote"><p><sup>[<a name="ftn.id2808783" href="#id2808783">13</a>] </sup>A profile is a named set of application settings. e.g. the
            'terminal class' of
            gnome-terminal. This makes
            sense when an application can be used in multiple contexts
            by the same user, and that user may want different
            settings in each context. </p></div></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ar01s22.html">Prev</a> </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right"> <a accesskey="n" href="apa.html">Next</a></td></tr><tr><td width="40%" align="left">Other libraries </td><td width="20%" align="center"><a accesskey="u" href="index.html">Up</a></td><td width="40%" align="right"> A. Module dependencies</td></tr></table></div></body></html>
