<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><title>Text Handling in GTK+ 2.0</title><meta name="generator" content="DocBook XSL Stylesheets V1.45"><link rel="home" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="up" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="previous" href="ar01s09.html" title="Glib, GTK+ and Pango"><link rel="next" href="ar01s11.html" title="GObjects"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Text Handling in GTK+ 2.0</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ar01s09.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="ar01s11.html">Next</a></td></tr></table><hr></div><div class="sect1"><div class="titlepage"><div><h2 class="title" style="clear: both"><a name="gtk-text"></a>Text Handling in GTK+ 2.0</h2></div></div><p> The move to Unicode, along with proper internationalized text
   handling, is the most pervasive change in GTK+ 2.0. However, most user
   code that simply uses existing GTK widgets won't require modification.
   Some string manipulation code will need modification to work with UTF-8,
   and code that draws text directly to the screen with functions such as
   <tt>gdk_draw_string()</tt> will compile but will not display
   international text correctly.  Similarly, code using
   <tt>GdkFont</tt> will compile but will not internationalize
   properly.  </p><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="gtk-utf8"></a>UTF-8 Manipulation</h3></div></div><p> Most existing GTK+ code probably assumes that natural language
      strings are in an 8-bit fixed-length encoding with ASCII as a subset.
      In addition to ASCII itself, the most common such encoding is Latin-1.
      &quot;Fixed length&quot; means that all characters are represented by the same
      number of bits; so each byte corresponds to one character.  This
      wasn't true even in GTK+ 1.2; user-visible strings in GTK+ 1.2 were
      normally in the current locale's encoding. Essentially this meant that
      there was no way to know the encoding of a string unless you knew it
      was ASCII. You could not perform any operations on a per-character
      basis, because characters could have been 32 bits long, or a variable
      number of bits long. The bit pattern corresponding to an ASCII
      character such as <tt>'a'</tt> could actually be part of
      encoded Japanese text, not representing the English letter &quot;a&quot; at
      all.  In practice, most GTK+ 1.2 programs are somewhat broken from an
      i18n standpoint.  </p><p> In GTK+ 2.0, all user-visible strings are encoded in UTF-8
      format.  UTF-8 is an encoding of the Unicode character set. Unicode
      characters are 32-bit integers (represented in GLib and GTK+ by the
      typedef '<tt>gunichar</tt>'). In UTF-8, the 128 ASCII
      characters are encoded as themselves, as a single byte. In addition,
      the ASCII character 0 (nul) has its usual role as a string terminator.
      Because ASCII is a UTF-8 subset, much existing code will still work
      fine; scanning for an ASCII character works fine. C string literals
      containing only ASCII continue to work fine. The type of strings
      remains '<tt>char*</tt>', rather than
      '<tt>wchar_t*</tt>' or some other wide string type.  </p><p> All ASCII characters have the eighth bit set to 0; UTF-8 uses
      sequences of bytes with this bit set to 1 to encode the rest of
      Unicode. This variable-length strategy has a couple of important
      implications:
         <div class="itemizedlist"><ul><li><p> Characters do not correspond to bytes. The return
               value of strlen() is the number of <span class="emphasis"><i>bytes</i></span>
               in a string. With UTF-8, the number of characters could be
               much smaller than the number of bytes.  </p></li><li><p> Strings can be <span class="emphasis"><i>invalid</i></span>. All
               possible strings of bytes are valid Latin-1 text, since 0 to
               255 are all valid Latin-1 characters. All possible strings of
               bytes are not valid UTF-8, because some sequences of bytes
               encode a character, and some do not. In particular, if you
               &quot;split&quot; a sequence in the middle, the resulting halves will
               be invalid; they never form a valid sequence by themselves.
               </p></li></ul></div>
      </p><p> When a GTK+ 2.0 function takes a human-readable string as an
      argument, that string <span class="emphasis"><i>must</i></span> be valid UTF-8. If you
      receive text from a file or from the network, always validate it with
      <tt>g_utf8_validate()</tt> before passing it to GTK+.
      Invalid UTF-8 will crash your program if you pass it to GTK+.  </p><p> When iterating over strings interpreting
      <span class="emphasis"><i>characters</i></span>, you must use
      <tt>g_utf8_next()</tt> instead of simply incrementing
      pointers. &quot;<tt>++p</tt>&quot; does not advance by one character;
      it may advance into the middle of a character.  (Caveat: code using
      the &quot;<tt>++p</tt>&quot; idiom is OK if it's just scanning for
      ASCII characters, since they are encoded as a single byte. e.g., the
      call &quot;<tt>strchr (s, 'a')</tt>&quot; remains valid. But if you
      replace <tt>'a'</tt> with a non-ASCII character such as an
      accented &quot;a&quot;, the code will not work. You have to replace such code
      with something like:
         <pre class="programlisting">
while (*p)
{
gunichar ch = g_utf8_get_char (p);

if (ch == 0xE1)
   /* Found small a with acute accent, Unicode character 0xE1 */;

p = g_utf8_next (p);
}
         </pre>
      Conveniently, the Unicode characters <tt>0xA0</tt> through
      <tt>0xFF</tt> are the same as the Latin-1 characters.
      However, these are encoded as multiple bytes in UTF-8, so to compare
      to a Latin-1 character code you first have to decode the UTF-8 using
      <tt>g_utf8_get_char()</tt>.  </p><p> GLib 2.0 contains many useful functions for handling UTF-8
      text; including a <tt>g_utf8_strchr()</tt> that would handle
      the just-mentioned example. In addition, GLib 2.0 contains functions
      for converting Unicode text to and from the current locale's
      encoding.  You might want to look over <tt>gunicode.h</tt>
      or the GLib documentation on this topic. (Nearly 100% of the
      newly-added stuff in GTK+ 2.0 has complete reference documentation.)
      </p><p> Reading a file in locale encoding and converting it to UTF-8
      on-the-fly is pretty difficult. The simplest solution is to read the
      whole file at once with <tt>g_file_get_contents()</tt> and
      then call <tt>g_utf8_validate()</tt> on it. However, this
      blocks during the entire read operation, etc.
      <tt>GIOChannel</tt> now supports encoding conversion on
      streams, which is a bit nicer way to handle it.  </p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="gtk-fonts"></a>Fonts and Drawing Text</h3></div></div><p> If you're using any kind of non-default fonts, you'll want to
      convert your code to use the new
      <tt>PangoFontDescription</tt> rather than
      <tt>GdkFont</tt>. <tt>PangoFontDescription</tt> is
      much, much easier to use; to load a <tt>GdkFont</tt> you had
      to specify an awkward X font description string, and the resulting
      <tt>GdkFont</tt> was specific to a certain locale; it
      wouldn't work for all languages. Moreover, there were no fonts you
      could choose that everyone using your application was guaranteed to have,
      so a call to <tt>gdk_font_load()</tt> could always fail.
      </p><p> <tt>PangoFontDescription</tt> is much higher-level.
      The exact font technology used by GTK+ varies according to GDK target
      and how GTK+ was compiled; underneath Pango, the real implementation
      could be Win32 fonts, XRender fonts, traditional X fonts, TrueType
      fonts, or even a mixture. Pango can even assemble a collection of
      locale-specific fonts and use those to emulate a single Unicode font.
      Programmers don't have to worry about this; they specify high-level
      font details such as &quot;Sans Italic 12&quot;.  </p><p> A <tt>PangoFontDescription</tt> has a stringified
      form, described in the Pango documentation. &quot;Sans Italic 12&quot; is an
      example of such a string. The easiest way to obtain a
      <tt>PangoFontDescription</tt> is to call
      <tt>pango_font_description_from_string()</tt>. The font
      families &quot;Sans&quot;, &quot;Monospace&quot;, and &quot;Serif&quot; are guaranteed to exist on
      all GTK+ installations; these will never fail to load.  </p><p> If you were using the <tt>font</tt> field in
      <tt>GtkStyle</tt> to change the font for a widget, you'll
      need to use the new <tt>font_desc</tt> field instead.
      However, when converting your code from <tt>GdkFont</tt>,
      ask yourself if setting <tt>style-&gt;font</tt> remains the
      easiest thing to do; <tt>gtk_label_new_from_markup()</tt> is
      a handy alternative provided for <tt>GtkLabel</tt>. You can
      pass in a string marked up with HTML-like tags affecting text
      attributes such as color, text size, font family, and so on. The Pango
      documentation describes all the tags you can use. If you do still want
      to modify the style, probably you can use the new
      <tt>gtk_widget_modify_font()</tt> function instead of
      fooling with styles manually.
      <tt>gtk_widget_modify_font()</tt> works properly when the
      theme changes, and is easier to use.  </p><p> If you were using <tt>gdk_draw_string()</tt>,
      <tt>gdk_draw_text()</tt>,
      <tt>gtk_paint_string()</tt>, or variants thereof, you'll
      want to rework your code to use <tt>PangoLayout</tt>. A
      <tt>PangoLayout</tt> represents a paragraph of text to be
      rendered to the screen. With English or European text, to render a
      paragraph, you just break on spaces and draw each line one font height
      below the previous line. When you start to handle languages such as
      Arabic, Hindi, Japanese, Hebrew, Thai, or Korean, however, this is
      wrong; it simply won't work properly. Pango handles the details with
      <tt>PangoLayout</tt>. To render a paragraph of text, create
      a <tt>PangoLayout</tt> for that text, then call
      <tt>gdk_draw_layout</tt> or
      <tt>gtk_paint_layout()</tt>. To create your layout, call
      <tt>gtk_widget_create_pango_layout()</tt> on the widget you
      intend to draw on.  </p><p> While you'll usually just create a layout and draw it, for more
      complex cases <tt>PangoLayout</tt> has a lot of useful
      features; it has text attributes, and you can use the HTML-like Pango
      markup language mentioned earlier as a convenient way to set those
      attributes. (The less-convenient way uses
      <tt>PangoAttrList</tt> directly.)
      <tt>PangoLayout</tt> can also handle cursors, conversion
      from pixel locations to text positions, and that kind of thing.
      </p><p> When asking for text metrics, such as the size of a
      <tt>PangoLayout</tt>, you'll notice that all Pango objects
      have a &quot;logical size&quot; and an &quot;ink size.&quot; The logical size corresponds
      to the font metrics (ascent and descent), and should be used to
      position text. The logical size for a line of text will change if you
      add a new character that's larger than the existing characters. The
      ink size of some text describes the actual pixels covered by the font
      glyphs, so for example capital letters have a larger ink size than
      lowercase, and letters with &quot;descenders&quot; such as lowercase &quot;y&quot;, &quot;g&quot;,
      &quot;p&quot; have a larger ink size than letters without. Usually you don't
      need the ink size. Logical size should be used in widget size
      requests, and in any other case where you're positioning text.  </p><p> Pango sizes are specified in &quot;Pango units,&quot; which are converted
      to and from device units (e.g. pixels) using the
      <tt>PANGO_SCALE</tt> constant.  There are
      <tt>PANGO_SCALE</tt> units per pixel.  </p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="gtk-complexities"></a>General Text Handling</h3></div></div><p> If your application does anything complicated with text, such
      as text editing, you'll want to investigate Pango in more depth. Some
      considerations and definitions to keep in mind:
         <div class="itemizedlist"><ul><li><p> Logical and visual direction do not correspond.
               Logical direction is the sequence of characters in a string;
               iterating over a string moves through it logically. Visual
               direction is the sequence of font glyphs on the user's
               monitor. Overall text direction can be right-to-left instead
               of left-to-right; also, individual characters can be
               &quot;reordered&quot; on a smaller scale, so that glyphs representing
               characters are not in the same sequence as the characters
               themselves.  </p></li><li><p> Glyphs do not correspond to characters. That is,
               multiple Unicode characters can be displayed as a single font
               glyph on the screen. A <i>cluster</i> is a
               set of characters represented by a single glyph. (Glyphs are
               the graphical characters making up a font.) </p></li><li><p> The process of converting a string of characters into
               a string of glyphs is called <i>shaping</i>.
               The low-level Pango function <tt>pango_shape()</tt>
               performs this operation -- <tt>PangoLayout</tt>
               does shaping for you, so typically you don't need to think
               about it.  </p></li><li><p> Word, line, sentence, and paragraph breaks do not
               necessarily follow English rules. Some languages are written
               with no spaces, for example, so you can't use spaces to split
               up words. Line break rules depend on the language. Paragraphs
               can be broken by the newline/linefeed
               (<tt>'\n'</tt>) character, by carriage return
               (<tt>'\r'</tt>), by a sequence of carriage return
               and line feed, or by a special Unicode paragraph separator
               character. You can obtain text boundary information for a
               paragraph of text with the functions
               <tt>pango_break()</tt>,
               <tt>pango_get_log_attrs()</tt>, and
               <tt>pango_find_paragraph_boundary()</tt>.  </p></li><li><p> It's not possible to put the cursor (caret, insertion
               mark) at all character positions. Some characters &quot;combine&quot;
               into a single logical unit, and the cursor can't go between
               the combined characters. A group of combined characters is
               called a <i>grapheme</i>, that is, a logical
               unit of writing. <tt>pango_break()</tt> and friends
               report valid cursor positions along with other text boundary
               information.  </p></li></ul></div>
      </p><p> There are plenty of other considerations when rendering
      internationalized text; see the Pango documentation and the Unicode
      specification for a lot of useful information. Whenever possible, let
      <tt>PangoLayout</tt> handle the details, so your application
      will work properly around the world. If in doubt, please ask someone!
      We're happy to answer internationalization questions on
      gtk-list@gnome.org, and if you don't ask you might end up introducing
      bugs that are hard to fix.  </p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ar01s09.html">Prev</a> </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right"> <a accesskey="n" href="ar01s11.html">Next</a></td></tr><tr><td width="40%" align="left">Glib, GTK+ and Pango </td><td width="20%" align="center"><a accesskey="u" href="index.html">Up</a></td><td width="40%" align="right"> GObjects</td></tr></table></div></body></html>
