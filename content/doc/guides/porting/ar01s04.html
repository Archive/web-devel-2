<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><title>Preparing to port</title><meta name="generator" content="DocBook XSL Stylesheets V1.45"><link rel="home" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="up" href="index.html" title="Porting applications to the GNOME 2.0 platform"><link rel="previous" href="ar01s03.html" title="The platform libraries"><link rel="next" href="ar01s05.html" title="Changes to the build environment"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Preparing to port</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ar01s03.html">Prev</a> </td><th width="60%" align="center"> </th><td width="20%" align="right"> <a accesskey="n" href="ar01s05.html">Next</a></td></tr></table><hr></div><div class="sect1"><div class="titlepage"><div><h2 class="title" style="clear: both"><a name="setting-up"></a>Preparing to port</h2></div></div><p>Naturally, before any porting can begin, you need to have all of the
   correct tools and libraries available to build against. In this section, we
   cover collecting the software and setting up your system to build
   applications and run with the GNOME 2 environment.</p><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="build-requirements"></a><a name="build-requirements-title"></a>Requirements for building the
      modules</h3></div></div><p>Here is a list of the tools you will need to be able to build the
      GNOME 2 platform libraries and to build your applications against these
      libraries. </p><div class="table"><p><b>Table 2. Packages required to build the GNOME 2 platform</b></p><table summary="Packages required to build the GNOME 2 platform" border="1"><colgroup><col><col align="center"></colgroup><thead><tr><th>Package</th><th>Minimum Required Version</th></tr></thead><tbody><tr><td>autoconf</td><td>2.52</td></tr><tr><td>automake</td><td>1.4-p4</td></tr><tr><td>libtool</td><td>1.4</td></tr><tr><td>pkgconfig</td><td>0.8.0</td></tr><tr><td>gettext</td><td>0.10.40</td></tr></tbody></table></div><p> Some general notes on these packages:</p><div class="itemizedlist"><ul><li><p>The autoconf,
            automake and
            libtool packages are only needed if you
            are building the libraries from CVS.</p></li><li><p>The required version of automake
            is not the absolutely latest released version (that is version
            1.5). If you are using version 1.5 of
            automake, then the following tips
            (<a href="http://mail.gnome.org/archives/gnome-2-0-list/2001-August/msg00212.html" target="_top">originally
            posted</a> to the gnome-2-0 list by James Henstridge) will be
            of use:</p><div class="itemizedlist"><ul><li><p>If a compilation fails when trying to build some files
                  containing assembly code, you will need to add the line
                  <pre class="programlisting">AM_PROG_AS</pre> to your
                  <tt>configure.in</tt> file. This is necessary
                  currently when building GTK+ with
                  version 1.5, for example.</p></li><li><p>Some packages will fail when running <b>make
                  distcheck</b>. There are two main reasons for this:
                  Firstly, the source directory for the test build is made
                  read-only during the build. You can get around this problem
                  by not putting any generated files in the source directory
                  during the build (this is a problem for documentation) or by
                  putting an explicit
                     <pre class="programlisting">
chmod u+w $(sourcedir)
                     </pre>
                  in your <tt>Makefile</tt> rules.</p><p>The other reason a distcheck might fail is if
                  <b>make uninstall</b> does not uninstall all of
                  the files installed by <b>make install</b>.
                  Similarly, if <b>make distclean</b> leaves
                  generated files in the build directory, automake
                  1.5 will fail.</p></li></ul></div></li><li><p>It is possible to use a version of
            gettext that is earlier than 0.10.40.
            Versions 0.10.38 and upwards are generally fine. However, 0.10.38
            would occasionally hit problems when combined with
            autoconf 2.52, which is fixed in version
            0.10.39 and 0.10.40.</p></li></ul></div><p><a name="pkg-config-desc"></a>The
      pkg-config application is an attempt to get
      around the need to for every package and library to have its own
      configuration script to pass the correct flags for the C compiler and
      linker to the <b>configure</b> script. Now each package just
      installs a <tt>package.pc</tt> file which contains
      information about where the package is installed, what other packages it
      depends upon and what linker and compiler flags it requires. Porting your
      application to use pkg-config is covered in
      the section called <a href="ar01s05.html">Changes to the build environment</a>.</p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="package-downloading"></a><a name="package-downloading-title"></a>Downloading the packages</h3></div></div><p>There are a few alternatives for retrieving all of the needed
      libraries — from CVS, from pre-packaged tarballs or in a packaging
      format suitable for your distribution.</p><p>The tarballs can be downloaded from <a href="ftp://ftp.gnome.org/pub/gnome/pre-gnome2/latest/sources/" target="_top">ftp://ftp.gnome.org/pub/gnome/pre-gnome2/latest/sources/</a>.
      However, due to regularly slow performance from ftp.gnome.org, it is
      recommended that you choose one of the mirrors from <a href="http://www.gnome.org/mirrors/ftpmirrors.php3" target="_top">http://www.gnome.org/mirrors/ftpmirrors.php3</a> and then go to the
      equivalent directory. </p><p>The install paths for these packages has been set up so that it is
      possible to install all of the GNOME 2 libraries alongside your current
      GNOME 1 packages. For example, whereas the GNOME 1 header files for
      libgnome are installed (typically) in
      <tt>/usr/include/libgnome</tt>, the GNOME 2 versions will be
      in <tt>/usr/include/libgnome-2.0/libgnome</tt>. </p><p>If you consistently use pkg-config to
      work out the required include paths and library flags, all of these
      subtleties will be taken care of for you. It is sufficient to realize
      that the two platforms can be installed simultaneously.</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>Some of the separate packages listed in the <a href="ar01s03.html">The platform libraries</a> section may be merged
            into a single tarball, so do not worry if there does not seem to be
            exactly the right number of tarballs.</p></div><p>The distribution specific packages can come from a number of
      sources.
         <div class="itemizedlist"><ul><li><p>Some Debian packages for Sid are available from the normal
               mirrors. At the moment, you can execute <b>apt-cache
               search gtk+ 1.3</b> to have
               gtk+,
               glib, atk
               and pango downloaded and installed.
               As more packages become available, you will be able to download
               them in a similar fashion (and this note will be update to
               reflect the most recent status). </p></li><li><p>Packages for Red Hat 7.2, collectively called
               <span class="emphasis"><i>gnomehide</i></span>, have been compiled by Havoc
               Pennington and are available from <a href="ftp://people.redhat.com/hp/gnomehide/" target="_top">ftp://people.redhat.com/hp/gnomehide/</a>. There is also a
               mailing list for these packages (compilation and installation
               problems only) at <a href="http://mail.gnome.org/mailman/listinfo/gnome-redhat-list" target="_top">http://mail.gnome.org/mailman/listinfo/gnome-redhat-list</a>.
               </p></li><li><p>Jacob Berkman at <a href="http://www.ximian.com" target="_top">Ximian</a> is doing snapshot
               builds of all the packages and making the results available
               through Ximian's <a href="http://www.ximian.com/products/ximian_red_carpet/" target="_top">Red
               Carpet</a> developers' channel. Before using these packages,
               you are recommended to read <a href="http://primates.ximian.com/~jacob/gnome-2-snapshots/" target="_top">http://primates.ximian.com/~jacob/gnome-2-snapshots/</a>.
               </p></li><li><p>There was a rumor floating around that Sun were doing
               nightly builds and going to make the results available for their
               platform. More details will be posted here when that is
               confirmed or denied.</p></li></ul></div>
      </p><p>For installing and building everything from CVS, you will need to
      check out all of the modules mentioned in <a href="ar01s03.html">The platform libraries</a>and build them in the order they are listed
      there.</p><p>One of the easiest ways to get all the right modules from CVS and
      build them is to check out the module called
      vicious-build-scripts from the GNOME CVS
      repository. Read the <tt>README</tt> file in this module
      carefully and follow the installation instructions. Then you can
      (optionally) automatically update your tree and compile most of the
      needed modules in the correct order.</p><p>At the present time
      vicious-build-scripts does not include
      gail,
      libgnomeprint, or
      libgnomeprintui. However, after building all
      of the earlier modules, building the last few by hand if they are
      required is a straightforward process.</p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="other-requirements"></a>Other packages required to run GNOME 2</h3></div></div><p>The GNOME 2 platform will also depend on a few extra packages that
      are not maintained by the GNOME developers, so users are expected to
      source them independtly and your application should check for their
      presence, if necessary. </p><p>Most of the extra packages will just be part of any standard Linux
      or Unix distribution. However, some are not yet that common, so here are
      the ones you may need to get especially. </p><div class="table"><p><b>Table 3. Extra packages required to run GNOME 2</b></p><table summary="Extra packages required to run GNOME 2" border="1"><colgroup><col><col align="center"><col></colgroup><thead><tr><th>Package</th><th>Minimum Version</th><th>Download from ...</th></tr></thead><tbody><tr><td>scrollkeeper</td><td>0.2</td><td><a href="http://scrollkeeper.sourceforge.net/" target="_top">http://scrollkeeper.sourceforge.net/</a></td></tr><tr><td>Norm Walsh's DocBook stylesheets
                     <sup>[<a name="id2798226" href="#ftn.id2798226">a</a>]</sup></td><td><span class="emphasis"><i>(unknown)</i></span></td><td><a href="http://sourceforge.net/projects/docbook/" target="_top">http://sourceforge.net/projects/docbook/</a></td></tr></tbody><tbody class="footnotes"><tr><td colspan="3"><div class="footnote"><p><sup>[<a name="ftn.id2798226" href="#id2798226">a</a>] </sup>These are used as the basis for some customized
                        GNOME stylesheets in converting help documents.</p></div></td></tr></tbody></table></div><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>The download links above are just pointers to the home page for
         each of these projects. You may be able to get packages for your
         particular distribution from the regular sources also.</p></div></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="misc-setup"></a>Some miscellaneous setup tips</h3></div></div><p><span class="emphasis"><i>(With thanks to Alan Cox.)</i></span></p><div class="sect3"><div class="titlepage"><div><h4 class="title"><a name="pango-tip"></a>Pango</h4></div></div><p>In order to set up Pango for testing, create a file in your home
         directory called <tt>.pangorc</tt> and put these lines in
         it:
            <pre class="programlisting">
[PangoFT2]
FontPath = /usr/share/fonts/default/Type1:/usr/share/fonts/default/TrueType
            </pre>
         Also, copy the file <tt>examples/pangoft2.aliases</tt>
         from the pango source directory to the file
         <tt>.pangoft2_aliases</tt> in your home directory. </p></div><div class="sect3"><div class="titlepage"><div><h4 class="title"><a name="rh7.1-tip"></a><a name="rh7.1-tip-title"></a>Building on Red Hat 7.1</h4></div></div><p>Building the entire platform from scratch requires at least
         version 2.0 of Python. This is due to a
         conversion script required by libglade (see
         the section about <a href="ar01s22.html#libglade">Libglade</a>
         for more information). </p><p>If you have a standard Red Hat 7.1 installation, you will need
         to get the python2 rpm from the Rawhide
         packages, since the version that come with the 7.1 Power Tools does
         not include the required XML support. Once place to download the
         required rpm is from <a href="http://www.rpmfind.net/linux/rpm2html/search.php?query=python" target="_top">www.rpmfind.net</a>.
         </p></div></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="disk-space"></a>Disk space requirements</h3></div></div><p>GNOME does not take up zero amounts of disk space. In fact, it
      requires quite a bit of space, although in these days of multi-gigabyte
      drives, it's nothing too severe. However, if space on your hard-drive is
      of concern, here are some facts. These are very rough numbers —
      there may be some variation just due to packaging options, for example
      — and are based solely on my observations on a couple of machines.
         <div class="itemizedlist"><ul><li><p>The sources for the platform libraries require about 170MB
               before being built (this is the size of the distributed source
               code -- it is slightly less in CVS because some of the files are
               generated as part of the <tt>autogen.sh</tt> script
               and not cleaned up as part of <b>make
               distclean</b>). </p></li><li><p>The installed platform base (without sources and without
               stripping any libraries) is about 200MB. </p></li><li><p>If you are building the libraries using the
               vicious-build-scripts and using the
               <tt>LEAN=yes</tt> option (which runs <b>make
               clean distclean</b> after installing each module), you
               will need approximately another 140MB of disk space on top of
               that required for the sources. The really big module here is
               gtk — if you have enough space
               to build that, you will be fine for the rest of the modules as
               well. </p></li><li><p>Building all of the platform libraries requires
               approximately 800MB, including the source code and excluding
               the space required to install the libraries in their final
               destination. </p></li></ul></div>
      </p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ar01s03.html">Prev</a> </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right"> <a accesskey="n" href="ar01s05.html">Next</a></td></tr><tr><td width="40%" align="left">The platform libraries </td><td width="20%" align="center"><a accesskey="u" href="index.html">Up</a></td><td width="40%" align="right"> Changes to the build environment</td></tr></table></div></body></html>
