<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st September 2003), see www.w3.org" />

  <title>Modifying Other People's Code</title>
  <meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.48" />
  <link rel="HOME" title="GNOME Programming Guidelines" href="index.html" />
  <link rel="PREVIOUS" title="Binary Compatibility in Libraries" href="binary.html" />
  <link rel="NEXT" title="Maintaining a Package" href="maintain.html" />
  <style type="text/css">
  /*<![CDATA[*/
  body {
  background-color: #FFFFFF;
  color: #000000;
  }
  :link { color: #0000FF }
  :visited { color: #840084 }
  :active { color: #0000FF }
  /*]]>*/
  </style>
</head>

<body class="SECT1">
  <div class="NAVHEADER">
    <table summary="navheader" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <th colspan="4" align="center">GNOME Programming Guidelines</th>
      </tr>

      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="binary.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="maintain.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>
    </table>
  </div>

  <div class="SECT1">
    <h1 class="SECT1"><a name="MODIFY" id="MODIFY">Modifying Other People's Code</a></h1>

    <p>GNOME is a team project, so contributions of code to other people's programs are always appreciated. Please follow these guidelines when modifying other people's code.</p>

    <div class="SECT2">
      <h2 class="SECT2"><a name="ETIQUETTE" id="ETIQUETTE">General Etiquette</a></h2>

      <p>Please follow the same indentation style that the original code uses. The original code will be around for a longer time than you may want to dedicate to it, so keeping your contributions consistent with respect to indentation is more important than forcing your indentation style on the code.</p>

      <p>Although your patches may implement very cool functionality, it is very annoying for the author to have to re-indent your code before applying the patch to the code base. So if the original code looks like</p>

      <table summary="Example of good indention" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
int
sum_plus_square_of_indices (int *values, int nvalues)
{
  int i, total;

  total = 0;

  for (i = 0; i &lt; nvalues; i++)
    total += values[i] + i * i;

  return total;
}
  
</pre>
          </td>
        </tr>
      </table>

      <p>then do not add a function that looks like</p>

      <table summary="Example of poor indention" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
int sum_plus_cube_of_indices(int *values, int nvalues) {
  int i,total;

  total=0;

  for (i=0;i&lt;nvalues;i++)
    total+=values[i]+i*i*i;

  return total;
}
  
</pre>
          </td>
        </tr>
      </table>

      <p>In this second example, indentation and brace placement do not match the original code, there are no spaces around operators, and the code will just not look like the original one. Please follow the original author's coding style and your patches are more likely to be accepted.</p>

      <p>Please do not fix bugs with quick hacks or work-arounds; do the correct fix instead. Also, do not add features as hacks or with code that is not extensible; it is better to rework the original code to be extensible, and then to add your new feature using that code as a framework.</p>

      <p>Code cleanups are always welcome; if you find dirty pieces of code in GNOME, we will greatly appreciate it if you submit patches to make the code pretty and easy to maintain.</p>

      <p>As usual, make sure your contributed code compiles without warnings, has the correct prototypes, and does things following the guidelines in this document.</p>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="DOCUMENT" id="DOCUMENT">Documenting Your Changes</a></h2>

      <p>GNOME uses the standard GNU <tt class="FILENAME">ChangeLog</tt> files to document changes to the code. Every change you make to a program <i class="EMPHASIS">must</i> be accompanied by a ChangeLog entry. This lets people read the history of changes to the program in an easy way.</p>

      <p>If you use Emacs, you can add ChangeLog entries by pressing "C-x 4 a". If you know the proper incantation for other popular editors, please tell us so that we can expand this document.</p>

      <p>ChangeLog entries have the following general format:</p>

      <table summary="Example of Changelog format" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
1999-04-19  J. Random Hacker  &lt;jrandom@foo.org&gt;

  * foo.c (some_function): Changed the way MetaThingies are
  frobnicated.  We now use a hash table instead of a linked list to
  look MetaThingies up.
  (some_other_function): Support the MetaThingies hash table by
  feeding it when necessary

  * bar.c (another_function): Fixed bug where it would print "Take
  me to your leader" instead of "Hello, World".

1999-04-18  Johnny Grep  &lt;grep@foobar.com&gt;

  * baz.c (ugly_function): Beautified by using a helper function.
  
</pre>
          </td>
        </tr>
      </table>

      <p>If you add a new function to a library, please write the necessary reference and programming documentation for it. You can write inline reference documentation by using the comment format described in <tt class="FILENAME">gnome-libs/devel-docs/api-comment-style.txt</tt>. If you use Emacs, <tt class="FILENAME">gnome-libs/tools/gnome-doc/gnome-doc.el</tt> provides a keyboard shortcut you can use to automatically add documentation templates to your code.</p>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="CHANGE-CVS" id="CHANGE-CVS">Changing Code on CVS</a></h2>

      <p>If you have write access to the GNOME CVS repository, there are additional policies that must be followed. Since you are working on the master copy of the sources, you have to be especially careful.</p>

      <p>If you fix something in a program that is on CVS or if you add functionality to it, and if you have not been working on that program for a long time, please ask the original author before committing your patches. Generally it is OK to ask these questions on the <tt class="FILENAME">gnome-devel-list</tt> mailing list.</p>

      <p>Once the author of the program has acknowledged you as a 'frequent contributor', you may begin to commit your patches without prior consent. If you want to do a major re-organization of the code, though, you should ask first.</p>

      <p>Some modules on CVS have stable branches and development branches. Usually development should go on in the HEAD branch on CVS, and the stable branch should be kept separately. Generally no new features are to be put in stable branches; only bug fixes. Make the bug fix to the stable branch and ask the main author about the policy on merging patches to the development branch; some authors prefer to do it in batches, while others prefer to merge them immediately.</p>

      <p>If you will be working on experimental features that could break a lot of code, please create a branch on CVS and do your changes there. Do not merge them onto the main development branch until you are reasonably confident that they work correctly and are nicely integrated with the rest of the code. Using a branch to work on experimental features lets you avoid disrupting the work of other developers. Please ask the main author before merging your branch back into the main part of the tree.</p>

      <p>As usual, add a ChangeLog entry when you make a change. Some authors have the policy of rejecting changes that do not have such an entry, and rightly so.</p>

      <p>Sometimes different commiting policies exist for the modules, check if the module contains a <tt class="FILENAME">README.CVS</tt> file, and if so, please check that file before making changes.</p>

      <div class="SECT3">
        <h3 class="SECT3"><a name="BRANCHES" id="BRANCHES">Branches and Tags</a></h3>

        <p>We have a naming convention for branches and tags or branch points in the GNOME CVS repository. Tags should be set after each release and should be of the form "MODULE_MAJOR_MINOR_MICRO", for example, "GNOME_LIBS_1_0_53". Branch points should be of the form "MODULE_BRANCH_ANCHOR", as in "GNOME_LIBS_1_0_ANCHOR". Finally, a branch rooted at this point would be of the form "module-branch", i.e. "gnome-libs-1-0". Please use this naming convention when creating tags, branch points, and branches.</p>
      </div>

      <div class="SECT3">
        <h3 class="SECT3"><a name="CVS-POLICIES" id="CVS-POLICIES">Additional CVS Policies</a></h3>

        <p>CVS does not provide a built-in way to rename files or move them across directories. You should plan your source tree layout carefully so that you do not have to move or rename files.</p>

        <p>In case you have to move or rename a file in CVS, <i class="EMPHASIS">DO NOT</i> do a "cvs remove" and a "cvs add". If you do this, the "new" files will have lost the ability to track their history of changes, since from the viewpoint of CVS they will truly be new files in their initial revision. A goal of the GNOME CVS repository is to be able to track the history of the sources accurately since the beginning.</p>

        <p><i class="EMPHASIS">Please</i> ask a CVS maintainer, that is, a knowledgeable person with shell access to the CVS machine, to do the necessary surgery to move the files for you. This requires knowledge of how CVS works and must be done very carefully. Recovering from a "cvs add/remove" mistake is a very unpleasant task; if you move files in this erroneous way a CVS maintainer who has to go through this job will summon a flaming bat of death from hell to bite your head off. So please ask for someone to move the files for you.</p>
      </div>
    </div>
  </div>

  <div class="NAVFOOTER">
    <table summary="navfooter" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="binary.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="maintain.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>

      <tr>
        <td colspan="2" align="left"><b>Binary Compatibility in Libraries</b></td>

        <td colspan="2" align="right"><b>Maintaining a Package</b></td>
      </tr>
    </table>
  </div>
</body>
</html>
