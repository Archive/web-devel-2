<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st September 2003), see www.w3.org" />

  <title>Correctness and Robustness</title>
  <meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.43" />
  <link rel="HOME" title="GNOME Programming Guidelines" href="index.html" />
  <link rel="PREVIOUS" title="Coding Style" href="x53.html" />
  <link rel="NEXT" title="Security Considerations" href="x219.html" />
  <style type="text/css">
  /*<![CDATA[*/
  body {
  background-color: #FFFFFF;
  color: #000000;
  }
  :link { color: #0000FF }
  :visited { color: #840084 }
  :active { color: #0000FF }
  /*]]>*/
  </style>
</head>

<body>
  <div class="NAVHEADER">
    <table summary="navheader" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <th colspan="4" align="center">GNOME Programming Guidelines</th>
      </tr>

      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="x53.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="x219.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>
    </table>
  </div>

  <div class="SECT1">
    <h1 class="SECT1"><a name="AEN137" id="AEN137">Correctness and Robustness</a></h1>

    <p>It is extremely important that GNOME code be correct and robust. This means that the code should do what is expected of it, and it should handle exceptional conditions gracefully. While this may seem obvious, this section will give you some tips about how to ensure correctness in your GNOME code. This is very important, since users expect and deserve reliable software that does not crash and performs correctly.</p>

    <div class="SECT2">
      <h2 class="SECT2"><a name="AEN140" id="AEN140">Ensuring Consistency</a></h2>

      <p>Use the Glib assertion macros to ensure that your program's state is consistent. These macros help locate bugs very quickly, and you'll spend much less time in the debugger if you use them liberally and consistently.</p>

      <p>Insert sanity checks in your code at important spots like the beginning of public functions, at the end of code that does a search that must always succeed, and any place where the range of computed values is important.</p>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="AEN144" id="AEN144">Assertions and Preconditions</a></h2>

      <p>Assertions and preconditions help you ensure that your program's state is consistent. Glib provides macros to put assertions and preconditions in your code. You should use them liberally; in return they will help you locate bugs very quickly, and you'll spend much less time in the debugger tracking bugs down.</p>

      <p>These are the Glib macros for preconditions; they will emit a message when their conditions fail, and they will return the function from which they are called. They should be used at the beginning of functions.</p>

      <dl>
        <dt><span class="SYMBOL">g_return_if_fail (condition)</span></dt>

        <dd>
          <p>Returns from the current function if <span class="SYMBOL">condition</span> is false.</p>
        </dd>

        <dt><span class="SYMBOL">g_return_val_if_fail (condition, value)</span></dt>

        <dd>
          <p>Returns the specified <span class="SYMBOL">value</span> from the current function if <span class="SYMBOL">condition</span> is false.</p>
        </dd>
      </dl>

      <p>These are the macros for assertions. They will emit a message when their conditions fail and will call <tt class="FUNCTION">abort(3)</tt> to terminate the program. They should be used to ensure consistency for the code's internals.</p>

      <dl>
        <dt><span class="SYMBOL">g_assert (condition)</span></dt>

        <dd>
          <p>Aborts the program if <span class="SYMBOL">condition</span> is false.</p>
        </dd>

        <dt><span class="SYMBOL">g_assert_not_reached ()</span></dt>

        <dd>
          <p>Aborts the program if the macro is ever called.</p>
        </dd>
      </dl>

      <p>These functions should be used to impose preconditions on the code and check its correctness - think of them as sanity checks for your program. You should use them liberally to assist you in catching bugs quickly; once your program or is fully debugged, you can compile it with these macros disabled so that they will not add any runtime overhead.</p>

      <p>The <span class="SYMBOL">g_return_*()</span> macros should be used at the beginning of public functions in libraries to ensure that the arguments passed to them are correct and within the valid range. If a function returns nothing (i.e. it is <span class="SYMBOL">void</span>), you should use <span class="SYMBOL">g_return_if_fail()</span>. Otherwise, you should use <span class="SYMBOL">g_return_val_if_fail()</span> to return a 'safe' value. When a library function that uses these macros is invoked with incorrect argument values, it will issue an error message, and continue running. The client program may just take a hiccup, do nothing, or just crash, but at least you will know <i class="EMPHASIS">where</i> you have passed an incorrect value to a function.</p>

      <p>The <span class="SYMBOL">g_assert()</span> macro should be use to ensure the internal consistency of a library or program. Instead of returning from the current function and continuing execution if the condition fails, <span class="SYMBOL">g_assert()</span> issues an error message and immediately aborts the program. This is to avoid the program from running in an inconsistent state. You should use this macro when you want to ensure that the program or library is using sane internal values.</p>

      <p>The <span class="SYMBOL">g_assert_not_reached()</span> macro is used to mark a place in the code that should never be reached. For example, if you have a <span class="SYMBOL">switch</span> statement and you think you are handling all the possible values in the <span class="SYMBOL">case</span> labels, you should put a <span class="SYMBOL">g_assert_not_reached()</span> in the <span class="SYMBOL">default</span> label to ensure that the code never gets there (this would mean you missed handling a value, or your program is incorrect).</p>

      <p>These macros help you find bugs faster by warning you as soon as the program reaches an inconsistent state. Use them frequently, and you will find a lot of bugs very easily.</p>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="AEN193" id="AEN193">GTK+-related Issues</a></h2>

      <p>Be careful when writing event handlers - make sure they mark events as handled in the proper situations. Make sure your signal handlers have the correct prototypes. This is very important! Remember that not all signal handler prototypes look like this:</p>

      <table summary="example event handlers" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
static void my_handler (GtkWidget *widget, gpointer data);
  
</pre>
          </td>
        </tr>
      </table>

      <p>For example, event handlers take an extra event parameter and return a <span class="SYMBOL">gint</span> Check the GTK+ header files if you need to double-check this.</p>

      <p>Make sure your program deals with all user-generated actions in the proper way. Remember that the user can close windows at any time with the window manager; take this into account and write the necessary code to handle this.</p>

      <p>If you are testing for modifier keys in an event's state mask, do this:</p>

      <table summary="example of good event status mask" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
 if ((event-&gt;status &amp; (GDK_CONTROL_MASK | GDK_SHIFT_MASK))
      == (GDK_CONTROL_MASK | GDK_SHIFT_MASK))
    do_some_action ();
  
</pre>
          </td>
        </tr>
      </table>

      <p>This is necessary; if you do this instead</p>

      <table summary="example of poor event status mask" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
 if (event-&gt;state == (GDK_CONTROL_MASK | GDK_SHIFT_MASK))
    do_some_action ();
  
</pre>
          </td>
        </tr>
      </table>

      <p>then your program will not work correctly if the user has the NumLock key on, for example - NumLock is also a modifier, and if it is turned on, then the event state mask will not be what the second example expects.</p>

      <div class="SECT3">
        <h3 class="SECT3"><a name="AEN202" id="AEN202">Visuals and Colormaps</a></h3>

        <p>Some users have high-end video cards (SGIs and Suns, for example) that support multiple simultaneous visuals. Roughly, a visual defines the memory representation that a piece of hardware uses to store the contents of an image. Most PC video cards support a single visual at a time, but high-end hardware can have different windows and pixmaps in different visuals at the same time.</p>

        <p>It is important that you understand visuals and colormaps if you will be writing code that creates windows or pixmaps on its own, instead of using high-level wrappers like GnomeCanvas and GnomePixmap. Please read an Xlib programming manual for more information.</p>

        <p>In general, you just need to remember that the visual and colormap for a drawable must match the ones of another drawable if you want to copy an area from the first drawable into the second one. If they are not the same, you will get a BadMatch error from X and you application will most likely abort.</p>

        <p>If you create a graphics context (GC) and share it to paint onto several drawables, make sure all of them have the same visual and colormap for which the GC was defined. The same applies if you want to copy an area from a pixmap to a window; both must have the same visual and colormap</p>

        <p>If you are not sure that your code is doing this correctly, please ask politely on one of the GNOME development mailing lists for someone to test it with a video card that supports this feature. That person will likely know how to fix the problem and will tell you about it.</p>
      </div>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="AEN209" id="AEN209">Unix-related Issues</a></h2>

      <p>Check the return values of <i class="EMPHASIS">all</i> system calls that your program makes. Remember that a lot of system calls can be interrupted (i.e. the call will return -1 and errno will be set to EINTR) and must be restarted.</p>

      <p>Do not assume, for example, that <tt class="FUNCTION">write(2)</tt> will write the whole buffer at a time; you have to check the return value, which indicates the number of bytes written, and try again until it is zero. If the return value is -1, remember to check errno and handle the error appropriately.</p>

      <p>If your application calls <tt class="FUNCTION">fork(2)</tt> without calling <tt class="FUNCTION">execve(2)</tt>, remember that the child process cannot make any X calls. You can usually diagnose this problem because you get an obscure Xlib error message.</p>

      <p>Please read a book like "Advanced programming in the Unix environment", by Stevens, to learn about all these issues and to make sure that your programs use the Unix API correctly. If you are not sure you are using an Unix call correctly, please ask on the mailing lists.</p>
    </div>
  </div>

  <div class="NAVFOOTER">
    <table summary="navfooter" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="x53.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="x219.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>

      <tr>
        <td colspan="2" align="left"><b>Coding Style</b></td>

        <td colspan="2" align="right"><b>Security Considerations</b></td>
      </tr>
    </table>
  </div>
</body>
</html>
