<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st September 2003), see www.w3.org" />

  <title>Coding Style</title>
  <meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.48" />
  <link rel="HOME" title="GNOME Programming Guidelines" href="index.html" />
  <link rel="PREVIOUS" title="The Importance of Writing Good Code" href="good-code.html" />
  <link rel="NEXT" title="Correctness and Robustness" href="robust.html" />
  <style type="text/css">
  /*<![CDATA[*/
  body {
  background-color: #FFFFFF;
  color: #000000;
  }
  :link { color: #0000FF }
  :visited { color: #840084 }
  :active { color: #0000FF }
  /*]]>*/
  </style>
</head>

<body class="SECT1">
  <div class="NAVHEADER">
    <table summary="navheader" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <th colspan="4" align="center">GNOME Programming Guidelines</th>
      </tr>

      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="good-code.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="robust.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>
    </table>
  </div>

  <div class="SECT1">
    <h1 class="SECT1"><a name="CODE-STYLE" id="CODE-STYLE">Coding Style</a></h1>

    <p>'Coding style' refers to the way source code is formatted. For C, this involves things like brace placement, indentation, and the way parentheses are used. GNOME has a mix of coding styles, and we do not enforce any one of them. The most important thing is for the code to be consistent within a program or library - code with sloppy formatting is not acceptable, since it is hard to read.</p>

    <p>When writing a new program or library, please follow a consistent style of brace placement and indentation. If you do not have any personal preference for a style, we recommend the Linux kernel coding style, or the GNU coding style.</p>

    <p>Read the <tt class="FILENAME">(Standards)Writing C</tt> info node in the GNU documentation. Then, get the Linux kernel sources and read the file <tt class="FILENAME">linux/Documentation/CodingStyle</tt>, and ignore Linus's jokes. These two documents will give you a good idea of what we recommend for GNOME code.</p>

    <div class="SECT2">
      <h2 class="SECT2"><a name="INDENT" id="INDENT">Indentation Style</a></h2>

      <p>For core GNOME code we prefer the Linux kernel indentation style. Use 8-space tabs for indentation.</p>

      <p>Using 8-space tabs for indentation provides a number of benefits. It makes the code easier to read, since the indentation is clearly marked. It also helps you keep your code honest by forcing you to split functions into more modular and well-defined chunks - if your indentation goes too far to the right, then it means your function is designed badly and you should split it to make it more modular or re-think it.</p>

      <p>8-space tabs for indentation also helps you to design functions that fit nicely in a single screen, which means that people can understand the code without having to scroll back and forth in order to understand it.</p>

      <p>If you use Emacs, then you can select the Linux kernel indentation style by including this in your <tt class="FILENAME">.emacs</tt> file:</p>

      <table summary="emacs style" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
(add-hook 'c-mode-common-hook
          (lambda ()
            (c-set-style "k&amp;r")
            (setq c-basic-offset 8)))
  
</pre>
          </td>
        </tr>
      </table>

      <p>On newer Emacsen or with a newer cc-mode, you may be able to simply do this instead:</p>

      <table summary="new emacs style" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
(add-hook 'c-mode-common-hook
          (lambda ()
            (c-set-style "linux")))
  
</pre>
          </td>
        </tr>
      </table>

      <p>If you use vim, then you can select the GNOME kernel indentation style by including this fragment in your <tt class="FILENAME">~/.vimrc</tt> file:</p>

      <table summary="vim style" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
set ts=8
if !exists("autocommands_loaded")
  let autocommands_loaded = 1
  augroup C
      autocmd BufRead *.c set cindent
  augroup END
endif
  
</pre>
          </td>
        </tr>
      </table>

      <p>The GNU indentation style is the default for Emacs, so you do not need to put anything in your <tt class="FILENAME">.emacs</tt> to enable it. If you wish to select it explicitly, substitute "gnu" for "linux" in the example above.</p>

      <p>If you know how to customize indentation styles in other popular editors, please tell us about it so that we can expand this document.</p>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="NAMING" id="NAMING">Naming Conventions</a></h2>

      <p>It is important to follow a good naming convention for the symbols in your programs. This is especially important for libraries, since they should not pollute the global namespace - it is very annoying when a library has sloppily-named symbols that clash with names you may want to use in your programs.</p>

      <p>Function names should be of the form <tt class="FUNCTION">module_submodule_operation</tt>, for example, <tt class="FUNCTION">gnome_canvas_set_scroll_region</tt> or <tt class="FUNCTION">gnome_mime_get_keys</tt>. This naming convention eliminates inter-module clashes of symbol names. This is very important for libraries.</p>

      <p>Symbols should have descriptive names. As Linus says, do not use <tt class="FUNCTION">cntusr()</tt>, use <tt class="FUNCTION">count_active_users()</tt> instead. This makes code very easy to read and almost self-documenting.</p>

      <p>Try to use the same naming conventions as in GTK+ and the GNOME libraries:</p>

      <ul>
        <li>
          <p>Function names are lowercase, with underscores to separate words, like this: <tt class="FUNCTION">gnome_canvas_set_scroll_region()</tt>, <tt class="FUNCTION">gnome_mime_get_keys()</tt>.</p>
        </li>

        <li>
          <p>Macros and enumerations are uppercase, with underscores to separate words, like this: <span class="SYMBOL">GNOMEUIINFO_SUBTREE()</span> for a macro, and <span class="SYMBOL">GNOME_INTERACT_NONE</span> for an enumeration value.</p>
        </li>

        <li>
          <p>Types and structure names are mixed upper and lowercase, like this: <span class="SYMBOL">GnomeCanvasItem</span>, <span class="SYMBOL">GnomeIconList</span>.</p>
        </li>
      </ul>

      <p>Using underscores to separate words makes the code less cramped and easier to edit, since you can use your editor's word commands to navigate quickly.</p>

      <p>If you are writing a library, then you may need to have exported symbols that are to be used only within the library. For example, two of the object files that compose the library <tt class="FILENAME">libfoo.so</tt> may need to access symbols from each other, but this symbols are not meant to be used from user programs. In that case, put an underscore before the function name and make the first words follow the standard module/submodule convention. For example, you could have a function called <tt class="FUNCTION">_foo_internal_frobnicate()</tt>.</p>

      <div class="SECT3">
        <h3 class="SECT3"><a name="CONSIST" id="CONSIST">Consistency in Naming</a></h3>

        <p>It is important that your variables be consistently named. For example, a module that does a list manipulation may choose to name the variables that hold a list pointer "<span class="SYMBOL">l</span>", for terseness and simplicity. However, it is important that a module that manipulates widgets and sizes does not use variables called "<span class="SYMBOL">w</span>" for both widgets and widths (as in width/height values); this would make the code inconsistent and harder to read.</p>

        <p>Of course, these very short and terse names should only be used for the local variables of functions. Never call a global variable "<span class="SYMBOL">x</span>"; use a longer name that tells what it does.</p>
      </div>
    </div>

    <div class="SECT2">
      <h2 class="SECT2"><a name="CLEAN" id="CLEAN">Cleanliness</a></h2>

      <p>GNOME code should be as clean as possible. This implies using a consistent indentation style and good naming conventions, as described above. It also implies the following.</p>

      <p>Learn the correct use of the <span class="SYMBOL">static</span> keyword. Do <i class="EMPHASIS">not</i> make all your symbols global. This has the advantage that you can use shorter names for internal functions within a single source file, since they are not globally visible and thus you do not need the module/submodule prefix.</p>

      <p>Learn the correct use of the <span class="SYMBOL">const</span> keyword. Use it consistently, as it can make the compiler catch a lot of stupid bugs for you.</p>

      <p>If you have a function that returns a pointer to internal data which the user is not supposed to free, you should use a const modifier. This will warn the user if he tries to do something incorrect, for example:</p>

      <table summary="const example" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
const char *gnome_mime_get_info (const char *info);
  
</pre>
          </td>
        </tr>
      </table>

      <p>The compiler will warn the user if he tries to free the returned string. This can catch a lot of bugs.</p>

      <p>If you have random 'magic values' in your program or library, use macros to define them instead of hardcoding them where they are used:</p>

      <table summary="constants example" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
/* Amount of padding for GUI elements */
#define GNOME_PAD          8
#define GNOME_PAD_SMALL    4
#define GNOME_PAD_BIG      12
  
</pre>
          </td>
        </tr>
      </table>

      <p>If you have a list of possible values for a variable, do not use macros for them; use an enum instead and give it a type name - this lets you have symbolic names for those values in a debugger. Also, do not use an 'int' to store an enumeration value; use the enum type instead. This lets the compiler catch errors for you, allows the debugger to show proper values for these values and makes it obvious what values a variable can take. An example follows:</p>

      <table summary="enum example" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
/* Shadow types */
typedef enum {
  GTK_SHADOW_NONE,
  GTK_SHADOW_IN,
  GTK_SHADOW_OUT,
  GTK_SHADOW_ETCHED_IN,
  GTK_SHADOW_ETCHED_OUT
} GtkShadowType;

void gtk_frame_set_shadow_type (GtkFrame *frame, GtkShadowType type);
  
</pre>
          </td>
        </tr>
      </table>

      <p>If you define a set of values for a bit field, do it like this:</p>

      <table summary="enum bit field example" border="0" bgcolor="#E0E0E0" width="100%">
        <tr>
          <td>
            <pre class="PROGRAMLISTING">
/* Update flags for items */
enum {
  GNOME_CANVAS_UPDATE_REQUESTED  = 1 &lt;&lt; 0,
  GNOME_CANVAS_UPDATE_AFFINE     = 1 &lt;&lt; 1,
  GNOME_CANVAS_UPDATE_CLIP       = 1 &lt;&lt; 2,
  GNOME_CANVAS_UPDATE_VISIBILITY = 1 &lt;&lt; 3,
  GNOME_CANVAS_UPDATE_IS_VISIBLE = 1 &lt;&lt; 4
};
  
</pre>
          </td>
        </tr>
      </table>

      <p>This makes it easier to modify the list of values, and is less error-prone than specifying the values by hand. It also lets you use those values as symbols in a debugger.</p>

      <p>Do not write obfuscated code, but also try to be spartan. Do not use more parentheses than are necessary to clarify an expression. Use spaces before parentheses and after commas, and also around binary operators.</p>

      <p>Please do not put hacks in the code. Instead of writing an ugly hack, re-work the code so that it is clean, extensible and maintainable.</p>

      <p>Make sure your code compiles with absolutely no warnings from the compiler. These help you catch stupid bugs. Use function prototypes in header files consistently.</p>

      <p>Within GNOME you can use the <span class="SYMBOL">GNOME_COMPILE_WARNINGS</span> Autoconf macro in your <tt class="FILENAME">configure.in</tt>. This will take care of turning on a good set of compiler warnings in a portable fashion.</p>

      <p>Comment your code. Please put a comment before each function that says what it does. Do not say how it does it unless it is absolutely necessary; this should be obvious from reading the code. If it is not, you may want to rework it until the code is easy to understand.</p>

      <p>While documenting API functions for a library, please follow the guidelines specified in the file <tt class="FILENAME">gnome-libs/devel-docs/api-comment-style.txt</tt>. This allows your source code to provide inline documentation that is later extracted by the <tt class="FILENAME">gtk-doc</tt> system to create a DocBook manual automatically.</p>

      <div class="SECT3">
        <h3 class="SECT3"><a name="GTK" id="GTK">GTK+-related Issues</a></h3>

        <p>GTK+ lets you do a lot of magic and obfuscation with signal handlers, passed closures, and datasets. If you find yourself doing a lot of <span class="SYMBOL">gtk_object_set_data()</span> all over the place, or passing state around in bizarre ways via signal handlers, please rework the code. If you need to attach a lot of data to a particular object, then it is a good candidate for a new derived class, which will not only make the code cleaner, but more extensible as well.</p>

        <p>A lot of heuristics in complicated event handlers can often be replaced by clean code in the form of a state machine. This is useful when you want to implement tricky things like selection and dragging behavior, and will make the code easier to debug and extend.</p>
      </div>
    </div>
  </div>

  <div class="NAVFOOTER">
    <table summary="navfooter" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="good-code.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="robust.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>

      <tr>
        <td colspan="2" align="left"><b>The Importance of Writing Good Code</b></td>

        <td colspan="2" align="right"><b>Correctness and Robustness</b></td>
      </tr>
    </table>
  </div>
</body>
</html>
