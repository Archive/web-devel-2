<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st September 2003), see www.w3.org" />

  <title>Binary Compatibility in Libraries</title>
  <meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.43" />
  <link rel="HOME" title="GNOME Programming Guidelines" href="index.html" />
  <link rel="PREVIOUS" title="Localization" href="x265.html" />
  <link rel="NEXT" title="Modifying Other People's Code" href="x338.html" />
  <style type="text/css">
  /*<![CDATA[*/
  body {
  background-color: #FFFFFF;
  color: #000000;
  }
  :link { color: #0000FF }
  :visited { color: #840084 }
  :active { color: #0000FF }
  p.c1 {font-weight: bold}
  /*]]>*/
  </style>
</head>

<body>
  <div class="NAVHEADER">
    <table summary="navheader" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <th colspan="4" align="center">GNOME Programming Guidelines</th>
      </tr>

      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="x265.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="x338.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>
    </table>
  </div>

  <div class="SECT1">
    <h1 class="SECT1"><a name="AEN277" id="AEN277">Binary Compatibility in Libraries</a></h1>

    <p>For stable and released libraries, it is very important to try to preserve binary compatibility across major revisions of the code. Library developers should try not to alienate users by making frequent binary incompatible changes on production libraries. Of course, libraries that are being developed or are labeled as not done can change as much as they need.</p>

    <p>A library typically exports a number of interfaces. These include routine names, the signatures or prototypes of these routines, global variables, structures, structure fields (both the types and the meaning of these), the meaning of enumeration values, and the semantics of the files that are created by the library. To keep binary compatibility means that these interfaces will not be changed. You can add new interfaces without breaking binary compatibility, but you cannot change the existing ones, since old programs will no longer run correctly.</p>

    <p>This section includes a number of hints on how you can make your library code binary compatible with old versions of itself.</p>

    <p>Keeping binary compatibility means that programs and object files that were compiled against a previous version of the code will continue to work without recompiling if the library is replaced. A detailed description of this can be found in the <tt class="FILENAME">(libtool)Interfaces</tt> info node in the GNU libtool documentation.</p>

    <div class="SECT2">
      <h2 class="SECT2"><a name="AEN284" id="AEN284">Private Information in Structures</a></h2>

      <p>In the GNOME system, it is very common task to create a new <span class="STRUCTNAME">GtkObject</span> by creating a structure, whose first member is the class this object inherits from, and then a number of instance variables that are added after this first member.</p>

      <p>This design scheme usually leads to code that is hard to update and change: if the internal state that the object needs to maintain is extended, programmers need to resort to various hacks to keep the size of the structures the same. That is, it is hard to add new fields to the public structures without breaking binary compatibility, since the structure fields may change and the structures may change size.</p>

      <p>We therefore suggest a strategy that developers may adopt when creating new objects. This strategy will ensure binary compatibility and at the same time will improve the readability and maintainability of the code.</p>

      <p>The idea is that for any given object, the programmer should create three files: the first one contains the public contract between the library and the user (this is the header file that gets installed in the system); the second one contains the implementation of the object; and the third file contains a structure definition for the internal or private fields that do not need to be in the public structure.</p>

      <p>The public API structure would include a private pointer element that would point to the per-instance private data. The implementation routines would dereference this pointer to access private data; the pointer of course points to a structure that is allocated privately.</p>

      <p>For example, imagine we are creating a GnomeFrob object. The widget implementation will be split in three files:</p>

      <div class="TABLE">
        <p class="c1">Table 1. Files that make up the sample GnomeFrob widget</p>

        <table summary="sample GnomeFrob" border="1" bgcolor="#E0E0E0" cellspacing="0" cellpadding="4" class="CALSTABLE">
          <tr>
            <th align="left" valign="top">Filename</th>

            <th align="left" valign="top">Contents</th>
          </tr>

          <tr>
            <td align="left" valign="top"><tt class="FILENAME">gnome-frob.h</tt></td>

            <td align="left" valign="top">GnomeFrob public API</td>
          </tr>

          <tr>
            <td align="left" valign="top"><tt class="FILENAME">gnome-frob.c</tt></td>

            <td align="left" valign="top">GnomeFrob object implementation</td>
          </tr>

          <tr>
            <td align="left" valign="top"><tt class="FILENAME">gnome-frob-private.h</tt></td>

            <td align="left" valign="top">GnomeFrob object private implementation data. You might want to use this if you plan on sharing the private data across various C files. If you limit the use of this private information to a single file, you do not need this, defining the structure in the implementation file will achieve the same effect.</td>
          </tr>
        </table>
      </div>

      <div class="EXAMPLE">
        <p class="c1">Example 1. Sample <tt class="FILENAME">gnome-frob.h</tt> public API.</p>

        <table summary="example header" border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
              <pre class="PROGRAMLISTING">
typedef struct _GnomeFrob GnomeFrob;
typedef struct _GnomeFrobPrivate GnomeFrobPrivate;

struct _GnomeFrob {
       GtkObject parent_object;

       int  public_value;
       GnomeFrobPrivate *priv;
} GnomeFrob;

GnomeFrob *gnome_frob_new (void);
void       gnome_frob_frobate (GnomeFrob *frob);
    
</pre>
            </td>
          </tr>
        </table>
      </div>

      <div class="EXAMPLE">
        <p class="c1">Example 2. Sample <tt class="FILENAME">gnome-frob-private.h</tt></p>

        <table summary="example private header" border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
              <pre class="PROGRAMLISTING">
typedef struct {
        gboolean frobbed;
};
    
</pre>
            </td>
          </tr>
        </table>
      </div>

      <div class="EXAMPLE">
        <p class="c1">Example 3. Sample<tt class="FILENAME">gnome-frob.c</tt> implementation.</p>

        <table summary="example c source" border="0" bgcolor="#E0E0E0" width="100%">
          <tr>
            <td>
              <pre class="PROGRAMLISTING">
void gnome_frob_frobate (GnomeFrob *frob)
{
        g_return_if_fail (frob != NULL);
  g_return_if_fail (GNOME_IS_FROB (frob));

  frob-&gt;priv-&gt;frobbed = TRUE;
}
    
</pre>
            </td>
          </tr>
        </table>
      </div>

      <p>Notice the use of structure prototypes: this enables the compiler to do more checking for you at compile time about the types being used.</p>

      <p>This scheme is useful in some situations, particularly the case in which you can afford to store an extra pointer per object instance. If this is not possible the programmer might need to resort to other workarounds for this problem.</p>

      <p>The purpose of having an extra private header file for the private structures is to allow for derived classes to use this information. Of course, good programming practice would indicate that interfaces or access methods would be provided precisely to keep the programmer from having to poke into the private structures. You should try to achieve a balance between good practice and pragmatism when creating private structures and public access methods.</p>

      <p>Some problems might arise, for example, when you have shipped a version of your code that is being widely used and you want to extend it. There are two possible solutions for this.</p>

      <p>The first option is to find a pointer field in the public structure that you can make private, and replace it with a pointer to the private part of the structure. This preserves the size of the structure, since for the purposes of GNOME, pointers can be assumed to be all of the same size. This field can be made to point to the private part of the structure that will be allocated by the function that originally created the public structure. It is important that this field is not called <tt class="STRUCTFIELD"><i>private</i></tt>, as this is a C++ keyword and will create problems when your header file is being used from C++ sources - call it <tt class="STRUCTFIELD"><i>priv</i></tt> instead. Of course, this type of change will only work if the old pointer field was really being used for internal purposes only; if users of the library had had to access that field for any purpose, you will have to find another field or resort to a different solution.</p>

      <p>If your original structure was a derivative of <span class="STRUCTNAME">GtkObject</span> and there are no pointer fields that you can replace, you can use the GTK+ object data facility instead. This allows you to associate arbitrary data pointers to objects. You can use can use the <tt class="FUNCTION">gtk_object_set_data()</tt> function to attach values or structures to your object and then you can use the <tt class="FUNCTION">gtk_object_get_data()</tt> to retrieve back those values. This can be used to attach a private structure to a GTK+ object. This is not as efficient as the previous approach, but it might not even matter for your particular application domain. If you will be accessing this data very frequently, you can set and get it using quarks instead of strings by using the <tt class="FUNCTION">gtk_object_set_data_by_id()</tt> and <tt class="FUNCTION">gtk_object_get_data_by_id()</tt> functions, respectively.</p>
    </div>
  </div>

  <div class="NAVFOOTER">
    <table summary="navfooter" width="100%" border="0" bgcolor="#FFFFFF" cellpadding="1" cellspacing="0">
      <tr>
        <td width="25%" bgcolor="#FFFFFF" align="left"><a href="x265.html"><b>&lt;&lt;&lt; Previous</b></a></td>

        <td width="25%" colspan="2" bgcolor="#FFFFFF" align="center"><b><a href="index.html"><b>Home</b></a></b></td>

        <td width="25%" bgcolor="#FFFFFF" align="right"><a href="x338.html"><b>Next &gt;&gt;&gt;</b></a></td>
      </tr>

      <tr>
        <td colspan="2" align="left"><b>Localization</b></td>

        <td colspan="2" align="right"><b>Modifying Other People's Code</b></td>
      </tr>
    </table>
  </div>
</body>
</html>
