@date October 26 - November 9

<hr>

@item Miguel Wins Award

Miguel was recognized along with Linus as one of the "100 most
remarkable innovators under 35" by Technology Review, a respected
magazine from MIT. See the link here:

  http://www.techreview.com/tr100/profile.php3?deIcaza

@item MemProf

Owen released an exciting new development tool that should interest
all developers using the Linux kernel and the GNU C library (2.0 or
2.1). Have a look here:

  http://people.redhat.com/~otaylor/memprof/index.html

MemProf basically does conservative garbage collection to detect
memory leaks.  It even has a nice GUI. For the moment, threads confuse
it a little bit, but it works great otherwise.

Commercial tools to do this cost on the order of $1000 per seat; Owen
wrote this in a couple weeks. Go figure.

I ran an early version of MemProf on GConf and found 6 serious bugs in
an hour or so.  All of them would have been nearly impossible to find
without this tool.

Get MemProf; run it on all your applications, as often as possible. Do
it now.

@item Report from Japan

I went to Tokyo for a GNU Seminar last week, arranged by the Free
Software Foundation. The day after the seminar we held a GNOME BOF as
well. There were around 100 people at the seminar, 50 or so at the
BOF; lots of interest in GNU and GNOME. 

Interesting demonstrations I saw included Masatake Yamato's Display
GhostScript - which does some cool things - and Yutaka Niibe's port of
the Linux kernel to Super 8 (a common embedded chip used in Sega
Dreamcast). I had a chance to meet some members of the Japan GNOME
User's Group, Steve Baur of XEmacs fame, Masayuki Ida (GNU VP for
Japan), Manuel Chakravarty (GTK+-Haskell author), Werner Koch (GPG
author), and many others. Everyone was very nice and very
enthusiastic; I had a great time, a visit to Tokyo is strongly
recommended if you ever get a chance.

This is also the reason I missed the summary last week. :-)

I put the slides for my GNOME presentation on the web, look here:

  http://www106.pair.com/rhp/havoc_slides.tar.gz

You need MagicPoint to view them, but they may be of interest. Feel
free to use/modify/redistribute these slides if you're making a
presentation about GNOME.

@item gnome-db activity

There's a new version of gnome-db (GNOME database
library/applications), and a new mailing list for discussion. If
you're interested in databases check out this module and consider
subscribing to the list. You can read more here:

  http://www.gnome.org/gnome-db/

@item developer.gnome.org Feature Articles

developer.gnome.org is running feature articles now, see:

  http://developer.gnome.org/feature/

The first article is from Federico, on gdk-pixbuf. There are also new
gdk-pixbuf reference docs at:
  
  http://developer.gnome.org/doc/API/gdk-pixbuf/book1.html

@item GStreamer

Erik Walthinsen revealed some interesting streaming media work, have a
look at the announcement here:

  http://news.gnome.org/gnome-news/941434104/index_html

There's lots of information on the home page:

  http://www.cse.ogi.edu/~omega/gnome/gst/

He has an MP3 player in only a few lines of code...

@item GNOME patch for AbiWord

Joaquin Cuenca Abela has a patch for AbiWord to make it use the GNOME
libraries. See his post to the AbiWord mailing list:

  http://www.abisource.com/mailinglists/abiword-dev/99/October/0288.html

No news on whether this has been integrated into the source
tree. GNOME compliance is welcome news; it could give AbiWord
consistent-looking dialogs and session management, among other things.

@item Need New Pixmap Themes Person

Richard Hestilow (hestgray@ionet.net) mailed to ask for help 
on revamping the pixmap theme:

<blockquote>
The pressing needs of school are pretty much requring me to go 
on a "coding vacation". So! I need someone to bring themes back from
the brink of death. I looked over my code, and apparently, the only code
that I'm not embarassed to hand over consists of a header file and
some lame gradient implementations. But I assure you I spent a lot of time
on that header file :-). All of this plus a bit of work on a themes editor
is in cvs module "rcedit". In summary: I want someone to take over this
stuff while I'm gone. Thank you very much. 

PS: Chances are good I will not be replying to email on a regular basis.
</blockquote>

The pixmap theme needs to be rewritten to use gdk-pixbuf; it can also
be made a good bit faster by reducing redraws, I'm told. Mail Richard
if you want to work on this.

@item Bug Buddy

Jacob Berkman hacked up a bug report wizard. This is pretty cute; it
walks the user through the process of submitting a bug report. If
there's a core file, it will even automatically extract a backtrace;
and it adds itself as the MIME handler for core files in the file
manager. (Of course, the backtrace is only useful if the program in
question has debugging symbols, but it will at least sometimes work.)

Bug Buddy is designed to work with multiple bug trackers; right now it
only supports bugs.gnome.org, but should be trivial to get working
with other Debian-based trackers (such as bugs.debian.org) and
probably isn't hard to get working with Bugzilla systems. Members
of projects other than GNOME might want to look into this.

@item Hacking Activity

1,363 commits in two weeks.

Module Score-O-Matic:
 (number of CVS commits per module, during this week)

 107 gdk-pixbuf
  98 entity
  97 gnumeric
  85 gnome-core
  66 gimp
  51 crescendo
  50 gnome-libs
  49 bug-buddy
  45 gxsnmp
  44 gtk--
  35 gnome-debug
  34 gnome-applets
  27 gtkhtml
  26 gphoto
  26 gnome-pilot
  25 web-devel-2
  22 gnomeweb-wml
  22 beast
  20 dr-genius
  19 nethack
  19 glade--
  18 mc

User Score-O-Matic:
 (number of CVS commits per user, during this week)

  86 sopwith
  66 unammx
  65 jirka
  64 jberkman
  60 martin
  60 jody
  59 imain
  39 mwimer
  39 jrb
  34 ole
  34 mmeeks
  32 wlashell
  30 gregm
  28 timj
  24 kmaraas
  24 glaurent
  23 hp
  22 drmike
  22 dcm
  20 kenelson
  20 eskil

The beginnings of a graphics/plot component went in to Gnumeric, and
the evolution module contains code for the much-discussed Evolution
email client International GNOME Support is working on. Lots of work on gdk-pixbuf 
and the next version of gnome-libs, as well as the next gnome-core.

@item New and Updated Software

Something like 40 new/updated applications...

gmt - GUI for kernel module management
Gnonews - news reader
gnomeching - I Ching
Gnomba - Samba browser
Vget - network download tool
gbox_applet - mailbox watcher
sawmill - nice window manager for GNOME
bug-buddy - bug report wizard
Gnetutil - ping, traceroute, etc. GUI
gnome-ttt - Tic Tac Toe
gnome-chess - Chess GUI
elknews - newsreader
GProc - process list
yank - notes and TODO list
irssi - IRC client
GtkExText - enhanced text widget
GMiniCPPEnvironment - sort of a micro-IDE for C++
GPeriodic - periodic table
GMasqDialer - masqdialer client
gnome-ttt-3D - 3D tic-tac-toe
GnOpenGL3ds - OpenGL 3DS file viewer
graphtool - BMP graphs from Gnumeric files
Pygmy - mail client written in Python
GMatH - math environment
Gnome Toaster - CD creation suite
gnome-db - GNOME database access
pasmon - network monitor
Pan - GNOME/GTK newsreader
Gmail - mail client
GStreamer - multimedia library
teatime - tells you about teatime
Gnorponocal - RPN calculator with a really weird name
screem - Web site creator/editor
gEdit - text editor
Gseq - sequencer
Morpheus - 3D model viewer
GCO - GNOME Comics Organizer
LinPopUp - talks to WinPopUp over Samba
GnomeHack - Nethack port
Gnome Network Buddy - network utility program
think - makes outlines
Quest - role-playing game
MemProf - memory profiler
Tapelab - tape labels
PicView - image viewer
ggv - postscript viewer
gimon - ISDN monitor
Eye of GNOME - image viewer
QuickRes applet - switch video resolutions
Tim - web browser (ambitious!)
graham - organizes important documents for you

See the software map on www.gnome.org (or Freshmeat) for more
information about any of these packages.

===========================================================================

Until next week - 

Havoc







