@date January 3-12, 2000

<hr>

@item GUADEC

The big news of the week was the announcement of GUADEC, the GNOME
Users and Developers European Conference in Paris, France this
March. GUADEC will be the first international conference dedicated
entirely to GNOME, and almost all the GNOME core developers will be
there. Part of the time the GNOME developers will discuss the future
of GNOME among themselves; there are also sessions for commercial
software developers to learn about the GNOME libraries, and for users
to learn about the GNOME user environment. 

GUADEC is kindly sponsored by Telecom Paris, Helix Code, Red Hat,
SuSE, LinuxCare, AFUL, ACT Europe, and MandrakeSoft. Students at Ecole
Nationale Supérieure des Télécommunications (ENST) are organizing the
conference. Thanks to all of these organizations for supporting GNOME.

Read more about it here:
  http://www.guadec.enst.fr/

@item Gtk-- Enters Freeze

The Gtk-- C++ wrapper has entered a freeze in preparation for a new
stable release.  The latest version has much, much lower overhead than
the 1.0 release, and is a very thorough wrapper; C data types are
converted to STL-style types, for example. An advantage of Gtk-- is
that it uses standard C++ features such as the STL and the string
class, so there's no need to have toolkit-dependent code in non-GUI
portions of the application. Gtk-- also offers typesafe signals via
Karl Nelson's libsigc++, and allows you to write new widgets via C++
inheritance. Finally Gtk-- is a complete compiler torture suite that has 
resulted in several bug reports to the egcs maintainers. :-)

Read the full announcement here:
  http://news.gnome.org/gnome-news/947026914/index_html

@item GNOME PixPacks

A new site showed up this week that archives some collections of
images to enhance your GNOME desktop. The first collection contains
nice panel backgrounds collected by GNOME documenter and user-support
guru Telsa Gwynne.

  http://dev.nullmodem.de/mawa/gnome-pp/

@item Evolution Updates from Helix Code

Some of the Helix Code hackers sent in progress reports on Evolution
(the mail client and calendar application, similar to Outlook). 

 Bertrand is working on the mail backend (called Camel); they already have 
 an MH backend and are currently working on mbox. Bertrand promises the 
 mbox backend will support Netscape and pine variants of the format, 
 "and be fast as hell too :)". 

 Chris Lahey has been writing the minicard view for the contact list (I guess if 
 you have Outlook you can visualize this better):
   http://primates.helixcode.com/~clahey/minicard-test.png
   
 I know Ettore has been doing work on GtkHTML for the message displayer and composer.

 Michael Zucchi has some cool screenshots and sample printouts on his diary page:
 
   http://zedzone.mmc.com.au/diary.html

 This is printing suport for gnomecal, to be merged into Evolution.

 There are also several impressive widgets you can get working from CVS if you try 
 hard enough: the shortcut panel, a complicated scheduling display, and 
 the ETable table widget. These are all pretty snazzy.

As I understand it the eventual plan is to glue all the components
together with Python.

@item GnomeICU Release

Jeremy Wise announced a new GnomeICU release:

  http://gnomeicu.gdev.net/

This release is "nearly uncrashable" and works great!

@item Mnemonic Release

The long-silent Mnemonic project has made a release, read about it here:

  http://news.gnome.org/gnome-news/947601386/index_html

@item Barnes and Noble talk reminder

Tonight! 7:30. Cary, North Carolina.

  http://news.gnome.org/gnome-news/947622131/index_html

@item Hacking Activity

Module Score-O-Matic:
 (number of CVS commits per module, since the last summary)

  92 gimp
  89 nautilus
  88 gnomeicu
  61 guppi3
  57 gnumeric
  51 gtk--
  49 gnome-libs
  40 gnome-core
  29 libgtop
  27 evolution
  26 gip
  23 gnome-pilot
  22 gphoto
  20 galway
  19 gdk-pixbuf
  19 gconf
  16 mooonsooon
  15 gob
  14 gnome-pim
  14 gb
  14 dr-genius

User Score-O-Matic:
 (number of CVS commits per user, since the last summary)

  71 jwise
  61 trow
  55 kmaraas
  50 rasta
  44 jirka
  42 sopwith
  40 kenelson
  36 hp
  35 unammx
  33 ahyden
  32 martin
  29 darin
  29 arios
  27 neo
  21 sullivan
  20 mstachow
  20 mmeeks
  20 jody
  19 pablo
  19 eskil
  18 sipan
  17 martijn
  16 campd

@item New and Updated Software

See the software map on www.gnome.org (or Freshmeat) for more
information about any of these packages.

===========================================================================

Until next week - 

Havoc
