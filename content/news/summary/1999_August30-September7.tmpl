@date August 30-September 7

<hr>

@item news.gnome.org for this week

This week on our news site we have articles about: libglade 0.5, Gnumeric 0.34, 
gnome-print 0.7, icons status report, Gnome configuration files whitepaper, 
custom shapes in the Dia diagram editor (written in an SVG subset),
Genius 0.4.5, multimedia squeaky rubber gnome tour, new gmc file manager release,
and some other stuff that scrolled off the bottom. :-)

I'm feeling a bit superfluous now that we have the news site, so the
summary will mostly be about a few bits and pieces and statistics that
the news site didn't pick up.

@item Check out the latest Sawmill

The Sawmill window manager is pretty sweet; the latest version has
excellent GNOME integration. Sawmill's GTK theme even changes to match
the current GTK theme; put Sawmill in GTK mode, then change your Gnome
theme from the control center, and Sawmill adjusts its theme to match.
Sawmill also works with gmc, root menu clicks, gnome pager and task
list, and has a graphical configuration tool written in
GTK. Definitely worth checking out.

  http://www.dcs.warwick.ac.uk/~john/sw/sawmill/index.html

I'm using Sawmill now and it's perfectly usable. Given the young age
of this software I think it's very promising.

@item Dr Geo and Genius merge

The Genius scientific calculator is merging with the Dr Geo geometry
exploration application to create "Dr Genius," a complete math
package. Looks pretty cool. Check the 'dr-genius' module out of CVS.

@item Gnomine Bonobo Component

People have been making embeddable components left and right with the
new Bonobo framework, for data such as PNG images, plain text, and PDF
files. But the coolest embeddable component is a wrapper for Gnomine
(minesweeper clone)!

 ftp://ftp.gnome.org/~michael/gnomine.tar.gz

You presumably need bonobo from CVS to get this to work, I imagine
it's complicated, but I'm sure it will be worth it. :-)

@item New RPMs from Dax

Dax put up more RPMs; check out the announce here:

  http://www.gnome.org/mailing-lists/archives/gnome-list/1999-September/0110.shtml

Or go straight to the software:

  ftp://ftp.gurulabs.com/pub/gnome/updates/i386

@item Hack Fest

CVS had over 1000 commits this week; someone was writing a lot of
code. Quite a few new and updated software packages as well.

Module Score-O-Matic:

 126 gimp
  52 gnome-libs
  48 gnome-core
  48 dryad
  45 web-devel-2
  39 gtk--
  39 gtk+
  34 gnome-pim
  32 gnumeric
  27 gnerudite
  24 mc
  24 gnome-vfs
  24 gnome-debug
  23 gnome-applets
  21 gconf
  21 evolution
  20 bonobo
  18 ggv
  17 gnome-print
  16 gnome-utils
  15 ggdb
  15 gdb-guile

User Score-O-Matic:

 101 martin
  69 unammx
  68 chyla
  56 kmaraas
  43 pablo
  34 jirka
  34 hp
  29 dcm
  27 tgil
  27 sopwith
  26 olofk
  24 yosh
  24 neo
  24 kenelson
  21 bertrand

@item New and Updated Software

Of course the big release of the week was a much-improved version of the 
gmc file manager.

GPG Shell: start on a GPG frontend
Loci: distributed data processing
libglade: Load Glade interfaces at runtime
Gnerudite: Scrabble clone
Screem: web site editor
glms: monitor CPU temperature, voltage, etc.
gproc: process list
Pan: newsreader
Sixty Four Bits: fun applet that counts in binary
gnome-print: GNOME printing library
Gnumeric: industrial strength spreadsheet
gxsnmp: SNMP tool
PovFront: POV frontend
gVN: network management tool
genius: scientific calculator
gnome-chess: Chess
glvm: Logical Volume Manager interface
Gone: newsreader
GNews: newsreader
libptb: library for customizable toolbars
URL Collector: stores a list of URLs
gMessaging System: handles message streams
Gip: GNOME install project (installation tool)
Gnome Toaster: Write CDs
gx10: X10 home automation
galway: web editor
gFTP: ftp client
GMail: mail client
GCDE: text editor
DPS-FTP: ftp client
gPhoto: digital camera tool
Pybliographer: bibliography database tool
gdm: xdm replacement for Gnome
Sodipodi: Vector graphics application
FreeSpeech: speech recognition
XChat: IRC client
V/GTK: port of the V toolkit to GTK
alarm applet: reminder applet
Gnomba: Samba shares browser

See the software map on www.gnome.org (or Freshmeat) for more
information about any of these packages.

===========================================================================

Until next week - 

Havoc




